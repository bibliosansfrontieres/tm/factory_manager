from models.core.slack_notification import Slack


class SlackNotification:
    def __init__(self):
        pass

    def send(self, msg=None, channel=None, icon_emoji=None, username=None, text=None):
        s = Slack(msg)
        s.forge_msg(channel=channel, icon_emoji=icon_emoji, username=username, text=text)
        s.send()
