from repository.login.LDAP_DAO import LDAPLoginDAO
from models.core.session import Session


class Login:
    def auth(username, password):
        result = LDAPLoginDAO().auth_user(username, password)

        session = Session(username, password)

        if result['logged']:
            token = session.get_auth_token()
            session.add_session(token)
            headers = {'X-Auth-Token': token}
            return session.to_dict(), 200, headers
        else:
            return session.to_dict(), 401


class Sessions:
    def validate(token):
        session = Session(token=token)

        if session.is_authenticated:
            return session.to_dict(), 200
        else:
            return session.to_dict(), 403
