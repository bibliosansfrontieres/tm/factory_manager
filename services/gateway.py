# -*- coding: utf-8 -*-

import uuid

from main.api import gateway
from models.core.process import Process
from models.core.message import Message
from repository.process.DictProcessDAO import DictProcessDAO
from repository.media_center.DictMediaCenterConfigDAO import DictMediaCenterConfigDAO
from repository.omeka.DictProjectDAO import DictProjectDAO
from repository.deploy.DictDeployDAO import DictDeployDAO
from repository.approval.DictApprovalDAO import DictApprovalDAO
from repository.recipe.FileRecipeHandler import FileRecipeHandler
from repository.user.DictUserDAO import DictUserDAO
from services.process.MongoProcessLogger import MongoProcessLogger


def gateway_start(recipe_id, data):
    process_data = dict()

    try:
        description = data.get('description')
        timestamp = data.get('timestamp')
        project = DictProjectDAO.get_project_base(data)
        user = DictUserDAO.get_user(data)
        config = DictMediaCenterConfigDAO.get_config(data)
        deploy = DictDeployDAO.get_config(data)
        approval = DictApprovalDAO.get_approval(data)
        recipe = FileRecipeHandler.getRecipeById(recipe_id)
        # assemble process
        process = Process(process_id=uuid.uuid4().hex, recipe=recipe, user=user,
                          project=project, config=config, description=description, timestamp=timestamp,
                          approval=approval, deploy=deploy)
    except Exception as e:
        return e.args, 400

    DictProcessDAO.update_process(process_data, process)
    msg = Message(process_data)
    MongoProcessLogger().log(msg)

    return gateway.publish(msg)
