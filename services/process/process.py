from repository.process.MongoProcessDAO import MongoProcessDAO


class ProcessID:
    def get_process_by_id(id):
        return MongoProcessDAO().get_process_by_id(id)

    def get_processes_by_recipe(id):
        return MongoProcessDAO().get_processes_by_recipe(id)
