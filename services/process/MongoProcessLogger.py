from repository.process.MongoProcessHandler import MongoProcessHandler


class MongoProcessLogger():
    def __init__(self):
        self._handler = MongoProcessHandler()

    def log(self, message):
        self._handler.emit(message)
        self._handler.close()

    def update_message(self, message):
        self._handler.update(
            self._handler.formatID(message.process.id),
            self._handler.formatProcess(message)
            )
        self._handler.close()

    def update_approval(self, process_id, message):
        self._handler.update(
            self._handler.formatID(process_id),
            self._handler.formatApprove(message)
            )
        self._handler.close()

    def update_deploy(self, process_id, message):
        from repository.deploy.DeployHelpers import DeployHelpers

        dmsg = DeployHelpers()

        mac_list = self._handler.read_latest(
            {"id": message.process.deployment.process_id},
            {"deployment.devices": 1}
        )['deployment']['devices']

        data = dmsg.get_and_update_mac_list(message, mac_list)

        self._handler.update(self._handler.formatID(process_id), self._handler.formatDevices(data))
        self._handler.update(self._handler.formatID(process_id), self._handler.formatParentProcess(data))
        self._handler.update(self._handler.formatID(process_id), self._handler.formatDeployProcessID(data))

        self._handler.close()
