from repository.log.MongoLogDAO import MongoLogDAO


def get_logs(process_id, level, limit, min_date, max_date):
    return MongoLogDAO().get_logs(process_id, level, limit, min_date, max_date), 200


def post_log(data):
    return MongoLogDAO().post_log(data), 200
