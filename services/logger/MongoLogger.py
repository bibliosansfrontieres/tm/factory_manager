# -*- coding: utf-8 -*-

import logging

from repository.log.MongoLogHandler import MongoHandler

DEFAULT_LOGGER_FORMAT = "%(asctime)s :: %(levelname)s :: %(message)s"


class MongoLogger(logging.Logger):

    def __init__(self, name, host, port, username, password, db='temps-modernes', collection='logs'):
        super().__init__(name)

        try:
            handler = MongoHandler(host, port, username, password, db, collection)
        except Exception as e:
            print("Cannot initialize MongoHandler for logs : {}\n - Using basic StreamHandler instead."
                  .format(e))
            handler = logging.StreamHandler()

        self.setLevel(logging.DEBUG)
        self.addHandler(handler)

    def update_msg(self, msg):
        hdl = self.handlers[0]
        if hdl and isinstance(hdl, MongoHandler):
            hdl.update_msg(msg)
