# -*- coding: utf-8 -*-

import logging

from models.core.rabbit_mq_connection import RabbitMQConnection

DEFAULT_LOGGER_FORMAT = "%(asctime)s :: %(levelname)s :: %(message)s"


class RabbitMQHandler(logging.Handler):

    def __init__(self, host, port, vhost, username, password, log_queue):
        super().__init__()

        if log_queue is None:
            raise Exception("No log_queue given.")
        self._log_queue = log_queue
        self._connection = RabbitMQConnection(host, port, vhost, username, password)

    def emit(self, record):

        try:
            self._connection.publish(
                body=self.format(record),
                exchange=self._log_queue,
                routing_key=self._log_queue
            )
        except Exception:
            self._connection.close()
            self.handleError(record)

    def close(self):
        self._connection.close()
