# -*- coding: utf-8 -*-

import logging

from .RabbitMQHandler import RabbitMQHandler


DEFAULT_LOGGER_FORMAT = "%(asctime)s :: %(levelname)s :: %(message)s"


class RabbitMQLogger(logging.Logger):

    def __init__(self, name, host, port, vhost, username, password, log_queue,
                 formatter=DEFAULT_LOGGER_FORMAT):
        super().__init__(name)

        try:
            handler = RabbitMQHandler(host, port, vhost, username, password, log_queue)
        except Exception as e:
            print("""Cannot initialize RabbitMQHandler for logs : {}\n -
                  Using basic StreamHandler instead.""".format(e))
            handler = logging.StreamHandler()

        handler.setFormatter(logging.Formatter(formatter))

        self.setLevel(logging.DEBUG)
        self.addHandler(handler)
