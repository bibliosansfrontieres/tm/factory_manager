from repository.approval.MongoApprovalDAO import MongoApprovalDAO
"""
from repository.approval.MongoApprovalHandler import MongoApprovalHandler
"""


class ApprovalService:
    def approve(self, approval):
        MongoApprovalDAO.approve(approval)
        return True

    def reject(process, approval):
        MongoApprovalDAO.reject(approval)
        return True
