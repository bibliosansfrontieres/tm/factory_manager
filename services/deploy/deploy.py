from repository.dhcp.DhcpDAO import dhcpDAO
from repository.mongo.MongoDAO import MongoIO


class DeployService:

    @staticmethod
    def get_lease(mac_address):
        return dhcpDAO().get_lease(mac_address), 200

    @staticmethod
    def get_all_leases():
        return dhcpDAO().get_all_leases(), 200

    @staticmethod
    def get_all_leases_with_content():
        return dhcpDAO().get_all_leases_with_content(), 200

    @staticmethod
    def get_device_job(mac_address):
        query = {"deployment.devices.mac_address": mac_address.lower()}
        projection = {'_id': 0}
        cursor = MongoIO(collection="process")
        result = cursor.read_latest(query, projection, sort_field='_id')
        return result, 200
