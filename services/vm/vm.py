from repository.docker.DockerController import DockerController
from repository.mongo.MongoDAO import MongoIO
from models.core.created_container import container_collection


class VMService:
    def __init__(self):
        pass

    def list(self):
        return DockerController().list()

    def mark_deleted(self, process_id):
        """
        Setting the deleted flag to `true` in database.
        """

        hdl = MongoIO(collection=container_collection)

        hdl.update_many({
            "process_id": process_id
        }, {
            "deleted": True
        })

    def delete(self, data):
        """
        Removes all containers having data['process_id'] in their name.
        Setting the deleted flag to `true` in database.
        """
        DockerController().container_cleanup(data['process_id'])

        self.mark_deleted(data['process_id'])
