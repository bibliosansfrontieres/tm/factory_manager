from models.core.mongo_connection import MongoDBConnection
from repository.approval.MongoApprovalHandler import MongoApprovalHandler
from settings import PREPROD_MONGODB_CONNECTION_HOST, PREPROD_MONGODB_CONNECTION_PORT, \
    PREPROD_MONGODB_CONNECTION_USERNAME, PREPROD_MONGODB_CONNECTION_PASSWORD, PREPROD_MONGODB_NAME
from pymongo import DESCENDING

COLLECTION = 'approval'


class MongoApprovalDAO:

    @staticmethod
    def get_approval(process_id):
        """Get the process for given id.

        Parameters
        ----------
        process_id : `int`, optional
            Id of the process to get the detail.

        Returns
        -------
        process : `Object` of `process`
            Object instance of process for given id.
        """
        connection = MongoDBConnection(PREPROD_MONGODB_CONNECTION_HOST,
                                       PREPROD_MONGODB_CONNECTION_PORT,
                                       PREPROD_MONGODB_CONNECTION_USERNAME,
                                       PREPROD_MONGODB_CONNECTION_PASSWORD,
                                       PREPROD_MONGODB_NAME)
        collection = connection.get_collection(COLLECTION)
        if process_id:
            query = {
                '$and': [
                    {'approval.process_id': process_id}
                ]
            }
        else:
            query = {}

        # query, projection
        cursor = collection.find(query, {'_id': 0}).sort('$natural', DESCENDING)

        approval = []
        for document in cursor:
            approval.append(document)

        connection.close()
        # return only first answer, which is last entry.
        return approval[0]

    @staticmethod
    def approve(approval):
        approval.approved = True
        handler = MongoApprovalHandler()
        """
        Suprime les mouvements precedents
        """
        handler.remove(approval)
        handler.emit(approval)
        handler.close()

    @staticmethod
    def reject(approval):
        approval.approved = False
        handler = MongoApprovalHandler()
        """
        Suprime les mouvements precedents
        """
        handler.remove(approval)
        handler.emit(approval)
        handler.close()
