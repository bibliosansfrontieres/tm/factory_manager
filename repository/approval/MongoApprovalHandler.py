# -*- coding: utf-8 -*-
from models.core.mongo_connection import MongoDBConnection
from settings import PREPROD_MONGODB_CONNECTION_HOST, PREPROD_MONGODB_CONNECTION_PORT, \
    PREPROD_MONGODB_CONNECTION_USERNAME, PREPROD_MONGODB_CONNECTION_PASSWORD, PREPROD_MONGODB_NAME

COLLECTION = 'approval'


class MongoApprovalHandler():

    def __init__(self,
                 host=PREPROD_MONGODB_CONNECTION_HOST,
                 port=PREPROD_MONGODB_CONNECTION_PORT,
                 username=PREPROD_MONGODB_CONNECTION_USERNAME,
                 password=PREPROD_MONGODB_CONNECTION_PASSWORD,
                 db=PREPROD_MONGODB_NAME,
                 collection=COLLECTION
                 ):
        self._connection = MongoDBConnection(host=PREPROD_MONGODB_CONNECTION_HOST,
                                             port=PREPROD_MONGODB_CONNECTION_PORT,
                                             username=PREPROD_MONGODB_CONNECTION_USERNAME,
                                             password=PREPROD_MONGODB_CONNECTION_PASSWORD)
        self._collection = self._connection.get_collection(collection)

    def format(self, approval):
        return {
            'approval':
            approval.to_dict()
        }

    def emit(self, approval):
        try:
            self._collection.insert_one(self.format(approval))
        except Exception:
            raise Exception(
                "The data does not contains the correct format for an approval.")

    def remove(self, approval):
        try:
            self._collection.delete_many({'approval.process_id': approval.process_id})
        except Exception:
            raise Exception(
                "The approval could be not removed.")

    def close(self):
        self._connection.close()
