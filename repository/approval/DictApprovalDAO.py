from models.core.approval import Approval
from models.core.approval import KEYWORD_APPROVAL
from datetime import datetime


class DictApprovalDAO:
    """Data Access Object to manage an `Approval` instances in dictionaries.
    """

    @staticmethod
    def get_approval(data):
        """Get an `Approval` instance from given dictionary.

        Parameters
        ----------
        data : `dict`
            Dictionary with the content of the `Approval`. Should have the next fields:


        Returns
        -------
        approval : `Approval`
            The `Approval` result from parsing the ``data`` dictionary.

        Raises
        ------
        Exception
            Raised if the content in ``KEYWORD_APPROVAL`` does not contains the correct format for a `Approval`.

        See Also
        --------
        """

        approval_data = dict() if data.get(KEYWORD_APPROVAL) is None else data.get(KEYWORD_APPROVAL)

        process_id = str() if approval_data.get('process_id') is None else approval_data.get('process_id')
        description = str() if approval_data.get('description') is None else approval_data.get('description')
        timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
        approved = approval_data.get('approved')
        parent_process_id = str() if approval_data.get('parent_process_id') is None \
            else approval_data.get('parent_process_id')

        return Approval(process_id, timestamp, approved, description, parent_process_id)

    @staticmethod
    def update_approval(data, approval):
        """Update in the given dictionary an `Approval`.

        Parameters
        ----------
        data : `dict`
            Dictionary to update in ``KEYWORD_PROCESS`` key.
        process : `Process`
            Content to update in ``data`` in ``KEYWORD_PROCESS`` key.

        See Also
        --------
        factory_manager.models.process.Process
        """
        if not isinstance(approval, Approval):
            raise TypeError("The content type to update in the dictionary must be an Approval.")
        data[KEYWORD_APPROVAL] = approval.to_dict()
