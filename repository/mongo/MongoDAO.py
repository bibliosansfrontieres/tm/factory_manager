# -*- coding: utf-8 -*-

from models.core.mongo_connection import MongoDBConnection
from settings import PREPROD_MONGODB_CONNECTION_HOST, PREPROD_MONGODB_CONNECTION_PORT, \
                       PREPROD_MONGODB_CONNECTION_USERNAME, PREPROD_MONGODB_CONNECTION_PASSWORD, \
                       PREPROD_MONGODB_NAME
from pymongo import DESCENDING

collection = 'deploy'


class MongoIO:

    def __init__(self,
                 collection,
                 host=PREPROD_MONGODB_CONNECTION_HOST,
                 port=PREPROD_MONGODB_CONNECTION_PORT,
                 username=PREPROD_MONGODB_CONNECTION_USERNAME,
                 password=PREPROD_MONGODB_CONNECTION_PASSWORD,
                 db=PREPROD_MONGODB_NAME
                 ):

        self._connection = MongoDBConnection(host, port, username, password, db)
        self._collection = self._connection.get_collection(collection)

    def write(self, record):
        try:
            self._collection.insert_one(record)
        except Exception as e:
            print("Cannot insert record in database : {}".format(e))

    def update_or_insert(self, query, newdata):
        try:
            # Update or insert if non-existent.
            self._collection.update_one(query, {"$set": newdata}, upsert=True)
        except Exception as e:
            print("Cannot insert or update record in database : {}".format(e))

    def update(self, query, newdata):
        try:
            # Update, fail if non-exitent.
            self._collection.update_one(query, {"$set": newdata}, upsert=False)
        except Exception as e:
            print("Cannot update record in database : {}".format(e))

    def update_many(self, query, newdata):
        try:
            # Update, fail if non-exitent.
            self._collection.update(query, {"$set": newdata}, upsert=False)
        except Exception as e:
            print("Cannot update records in database : {}".format(e))

    def read(self, query, projection={}):
        try:
            cursor = self._collection.find(query, projection)
            return cursor
        except Exception as e:
            print("Cannot find record in database : {}".format(e))
            return {}

    def read_latest(self, query, projection={}, sort_field='_id'):
        try:
            cursor = self._collection.find(query, projection).sort([(sort_field, DESCENDING)]).limit(1)
            return cursor[0]
        except Exception as e:
            print("Cannot find record in database : {}".format(e))
            return {}

    def delete_all(self, query):
        try:
            self._collection.delete_many(query)
        except Exception as e:
            print("Cannot find record in database : {}".format(e))
        return {}

    def make_unique(self, query):
        """
        Checks if requests gives at most one answer.
        Will otherwise delete all element and insert back first element
        """
        result = self.read(query=query)
        if self._collection.count_documents(query) > 1:
            self.delete_all(query)
            self.write(result[0])

    def count(self, query):
        return self._collection.count_documents(query)

    def close(self):
        self._connection.close()
