# -*- coding: utf-8 -*-

import getpass
import logging
from datetime import datetime

from models.core.message import Message
from models.core.mongo_connection import MongoDBConnection
from settings import PREPROD_MONGODB_CONNECTION_HOST, PREPROD_MONGODB_CONNECTION_PORT, \
    PREPROD_MONGODB_CONNECTION_USERNAME, PREPROD_MONGODB_CONNECTION_PASSWORD


class MongoHandler(logging.Handler):

    class MongoFormatter(logging.Formatter):

        msg = None

        def format(self, record):
            data = {
                'user': getpass.getuser(),
                'level': record.levelname,
                'date': datetime.fromtimestamp(record.created).strftime('%Y-%m-%dT%H:%M:%SZ'),
                'log': record.msg,
            }

            if self.msg and isinstance(self.msg, Message):
                data['process_id'] = self.msg.process.id

            return data

    def __init__(self, host, port, username, password, db, collection):
        super().__init__()

        self._connection = MongoDBConnection(host=PREPROD_MONGODB_CONNECTION_HOST,
                                             port=PREPROD_MONGODB_CONNECTION_PORT,
                                             username=PREPROD_MONGODB_CONNECTION_USERNAME,
                                             password=PREPROD_MONGODB_CONNECTION_PASSWORD)
        self._collection = self._connection.get_collection(collection)

        self.formatter = self.MongoFormatter()

    def update_msg(self, msg):
        self.formatter.msg = msg

    def emit(self, record):
        try:
            document = self.format(record)
            self._collection.insert_one(document)
        except Exception:
            self.handleError(record)

    def close(self):
        self._connection.close()
