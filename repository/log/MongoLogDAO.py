from models.core.mongo_connection import MongoDBConnection
from models.core.log import Logs
from settings import PREPROD_MONGODB_CONNECTION_HOST, PREPROD_MONGODB_CONNECTION_PORT, \
    PREPROD_MONGODB_CONNECTION_USERNAME, PREPROD_MONGODB_CONNECTION_PASSWORD, PREPROD_MONGODB_NAME

LOG_COLLECTION = 'logs'
LOG_LEVELS = ['HIGHEST', 'DEBUG', 'INFO', 'WARNING', 'ERROR']


class MongoLogDAO:
    def __init__(self):
        self.connection = MongoDBConnection(PREPROD_MONGODB_CONNECTION_HOST,
                                            PREPROD_MONGODB_CONNECTION_PORT,
                                            PREPROD_MONGODB_CONNECTION_USERNAME,
                                            PREPROD_MONGODB_CONNECTION_PASSWORD,
                                            PREPROD_MONGODB_NAME)
        self.collection = self.connection.get_collection(LOG_COLLECTION)

    def get_logs(self, process_id, level, limit, min_date, max_date):
        """Get the logs for given process id.

        Parameters
        ----------
        process_id : `int`, optional
            Id of the process to get the logs.
        level : `str`
            Minimum log level to show.
        limit : `int`
            Maximum amount of logs entries to show.
        min_date : `datetime.datetime`, optional
            Inferior date of the search (included).
        max_date : `datetime.datetime`, optional
            Superior date of the search (excluded)

        Returns
        -------
        logs : `list` of `str`
            List of logs for given process.
        """

        if level.upper() not in LOG_LEVELS:
            raise Exception("Invalid level.")
        levels = LOG_LEVELS[LOG_LEVELS.index(level.upper()):]

        query = {
            '$and': [
                {'level': {'$in': levels}}
            ]
        }
        if process_id:
            query['$and'].append(
                {'process_id': process_id}
            )
        if min_date:
            query['$and'].append(
                {'date': {'$gte': min_date}}
            )
        if max_date:
            query['$and'].append(
                {'date': {'$lt': max_date}}
            )

        # query, projection, order, max_amount
        cursor = self.collection.find(query, {'_id': 0}).sort('_id', -1).limit(limit)

        logs = []
        for document in cursor:
            logs.append(document)

        self.connection.close()

        if level.upper() == 'HIGHEST':
            return self.get_highest(process_id, logs)

        return logs

    def post_log(self, data):
        """
        Store whatever `Log` object is in data.
        """
        log_msg = Logs(data)
        try:
            self.collection.insert_one(log_msg.to_dict())
            self.connection.close()
        except Exception as e:
            raise Exception("Error inserting log message : {}. {}".format(log_msg.to_dict(), e))

    def get_highest(self, process_id, logs):
        """Get Highest error level for given logs list.
        Parameters
        ----------
        logs : `list`, mandatory

        Returns
        -------
        level : `str`
            LOG_LEVEL
        """
        if len(logs) == 0:
            return Logs.error_lvl("No logs found.", "ERROR")

        LOG_LEVELS.reverse()

        for lvl in LOG_LEVELS:
            for log in logs[:100]:
                if lvl == log['level']:
                    # Set LOG_LEVEL order back
                    LOG_LEVELS.reverse()
                    return Logs.error_lvl(process_id, lvl)

        return Logs.error_lvl("Unknown error.")
