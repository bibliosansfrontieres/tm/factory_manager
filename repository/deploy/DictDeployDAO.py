from models.core.deploy_config import Deploy
from models.core.deploy_config import KEYWORD_DEPLOY
from models.core.cap_config import CapConfig


class DictDeployDAO:

    @staticmethod
    def get_config(data):

        deploy_data = data.get(KEYWORD_DEPLOY)

        if deploy_data is None:
            process_id = str()
            devices = list()
            cap_config = CapConfig(process_id=str(), project_name=str(), timezone=str(), containers=list())

        elif not all(key in deploy_data.keys() for key in ['process_id', 'devices']):
            raise Exception("""The given deployment data does not contains the correct format.
                            Expected is {"deployment":{"process_id": "xxx", "devices": "yyy"}""")

        else:
            process_id = deploy_data.get('process_id')
            devices = deploy_data.get('devices')
            cap_config = CapConfig(process_id=str(), project_name=str(), timezone=str(), containers=list()) \
                if deploy_data.get('cap_config') is None else DictDeployDAO.get_capconf(deploy_data.get('cap_config'))

        return Deploy(process_id, devices, cap_config)

    @staticmethod
    def update_deploy(data, deploy):
        """Update the given dictionary to a proper `Deploy` object.

        Parameters
        ----------
        data : `dict`
            Dictionary to add``KEYWORD_DEPLOY`` key.
        deploy : `Deploy` object.
            Content to update in ``data`` in ``KEYWORD_DEPLOY`` key.

        See Also
        --------
        models.core.deploy_config
        """
        if not isinstance(deploy, Deploy):
            raise TypeError("The content type to update in the dictionary must be a Process.")
        data[KEYWORD_DEPLOY] = deploy.to_dict()

    @staticmethod
    def get_capconf(cap_config):
        _process_id = cap_config.get('process_id')
        _project_name = cap_config.get('project_name')
        _timezone = cap_config.get('timezone')
        _containers = cap_config.get('containers')
        return CapConfig(_process_id, project_name=_project_name, timezone=_timezone, containers=_containers)
