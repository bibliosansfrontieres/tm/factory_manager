

class DeployHelpers:
    def __init__(self):
        pass

    def get_and_update_mac_list(self, msg, mac_list):
        """
        Build configuration and update mac list.
        """
        #  TODO: make sure MAC are unique.
        new_msg = msg.process.to_dict()

        if len(mac_list) > 0:
            for item in mac_list:
                mac_address = str() if item.get('mac_address') is None else item.get('mac_address')
                name = str() if item.get('name') is None else item.get('name')
                project_name = str() if item.get('project_name') is None else item.get('project_name')
                new_msg['deployment']['devices'].append({
                    'mac_address': mac_address,
                    'name': name,
                    'project_name': project_name
                })
        return new_msg
