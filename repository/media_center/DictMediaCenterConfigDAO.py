import datetime
from models.core.mc_config import MediaCenterConfig
from models.core.mc_config import KEYWORD_CONFIG
from settings import DOCKER_VARS


class DictMediaCenterConfigDAO:
    """Data Access Object to manage a `MediaCenterConfig` instances in dictionaries.
    """

    @staticmethod
    def get_expire():
        """
        Returns an expirity value as a `str` (to make sure we always
        have a `str` in our object) controlled by value in settings.
        """
        now = datetime.datetime.now()
        default_expirity_delay = datetime.timedelta(days=DOCKER_VARS['DEFAULT_VALIDITY'])
        return (now + default_expirity_delay).__str__()

    @staticmethod
    def get_config(data):
        """Get a `MediaCenterConfig` instance from given dictionary.

        Parameters
        ----------
        data : `dict`
            Dictionary with the content of the `MediaCenterConfig`. Should have the next fields:

            - ``KEYWORD_CONFIG``:   Dictionary with the content of a `MediaCenterConfig`.
                                    Should have the following fields:

                - ``'engine'``: engine of the `MediaCenterConfig`.
                - ``'languages'``: list of languages of the `MediaCenterConfig`.
                - ``'deployable'``: bool. True if the build should make a "deployable" image.
                - ``'expire'``: str. datetime.now() format of the date at which containers should expire.
                - ``'api_url'``: str. URL to OLIP API.
                - ``'port_range'``: str. TCP port range to be used by OLIP containers.
                - ``'port_range_id'``: int. TCP port range ID, as in list id.

        Returns
        -------
        config : `MediaCenterConfig`
            The `MediaCenterConfig` result from parsing the ``data`` dictionary.

        Raises
        ------
        Exception
            Raised if the constant ``KEYWORD_CONFIG`` is not in ``data`` keys.
        Exception
            Raised if the content in ``KEYWORD_CONFIG`` does not contains the correct format for a
            `MediaCenterConfig` object.

        See Also
        --------
        factory_manager.models.mc_config.MediaCenterConfig
        factory_manager.models.mc_config.KEYWORD_CONFIG
        """
        config_data = dict() if data.get(KEYWORD_CONFIG) is None else data.get(KEYWORD_CONFIG)

        engine = str() if config_data.get('engine') is None else config_data.get('engine')
        languages = str() if config_data.get('languages') is None else config_data.get('languages')
        deployable = True if config_data.get('deployable') is None else config_data.get('deployable')
        api_url = str() if config_data.get('api_url') is None else config_data.get('api_url')
        expire = DictMediaCenterConfigDAO.get_expire() \
            if config_data.get('expire') is None else config_data.get('expire')
        port_range_id = config_data.get('port_range_id')
        port_range = config_data.get('port_range')

        return MediaCenterConfig(
            engine,
            languages,
            deployable,
            expire,
            api_url,
            port_range=port_range,
            port_range_id=port_range_id
        )
