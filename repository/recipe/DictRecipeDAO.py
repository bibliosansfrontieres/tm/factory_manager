from models.core.recipe import Recipe

KEYWORD_RECIPE = "recipe"
"""`str` : Keyword used in process dictionary to index a `Recipe`."""


class DictRecipeDAO:
    """Data Access Object to manage a `Recipe` instances in dictionaries.
    """

    @staticmethod
    def get_recipe(data):
        """Get a `Process` instance from given dictionary.

        Parameters
        ----------
        data : `dict`
            Dictionary with the content of the `Process`. Should have the next fields:

            - ``KEYWORD_RECIPE``: dictionary with the content of a `Recipe`. Should have the next fields:

                - ``'id'``: id of the `Recipe`.
                - ``'factories'``: list of factories names.

        Returns
        -------
        recipe : `Recipe`
            The `Recipe` result from parsing the ``data`` dictionary.

        Raises
        ------
        Exception
            Raised if the constant ``KEYWORD_RECIPE`` is not in ``data`` keys.
        Exception
            Raised if the content in ``KEYWORD_RECIPE`` does not contains the correct format for a `Recipe`.

        See Also
        --------
        factory_manager.models.recipe.KEYWORD_RECIPE
        """
        if KEYWORD_RECIPE not in data:
            raise Exception("Missing '{}' keyword in dictionary.".format(KEYWORD_RECIPE))

        recipe_data = data.get(KEYWORD_RECIPE)
        if not all(key in recipe_data.keys() for key in ['id', 'name', 'factories']):
            raise Exception("The data does not contains the correct format for a recipe.")

        recipe_id = recipe_data.get('id')
        name = recipe_data.get('name')
        factories = recipe_data.get('factories')

        return Recipe(recipe_id, name, factories)
