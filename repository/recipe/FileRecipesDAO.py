from models.core.recipes import Recipes
from models.core.recipe import Recipe

import os
import json


class FileRecipesDAO:
    """Data Access Object to manage a `Recipes` instances in json formatted file.
    """

    @staticmethod
    def get_recipes(path):
        """Get a `Recipes` instance from given file in ``path``.

        Parameters
        ----------
        path : `str`
            Path of the recipes file, to parse into `Recipes`.

        Returns
        -------
        recipes : `Recipes`
            The Recipes obtained from parsing the file in given ``path``.

        Raises
        ------
        Exception
            Raised if the file in ``path`` does not exists.
        Exception
            Raised if the file cannot be opened for reading.
        Exception
            Raised if the content of the file does not have the json format.

        See Also
        --------
        factory_manager.models.recipe.Recipes
        """
        recipes = Recipes()

        if not os.path.exists(path):
            raise Exception("The recipes file {} does not exist.".format(path))

        try:
            with open(path, 'r') as file:
                try:
                    data = json.load(file)
                except ValueError:
                    raise Exception("The file {} does not contains a valid json format.".format(path))

                for elem in data:
                    if not all(key in elem.keys() for key in ['id', 'name', 'recipe']):
                        raise Exception("""The file in {} does not contains
                        the correct format for a recipe.""".format(path))

                    recipe_id = elem.get('id')
                    name = elem.get('name')
                    factories = elem.get('recipe')

                    recipes.add_recipe(Recipe(recipe_id, name, factories))

        except IOError:
            raise Exception("Cannot open the recipes file {}.".format(path))

        return recipes
