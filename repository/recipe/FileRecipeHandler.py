from settings import RECIPES_PATH
from repository.recipe.FileRecipesDAO import FileRecipesDAO


class FileRecipeHandler:

    @staticmethod
    def getRecipeById(id):
        try:
            return FileRecipesDAO.get_recipes(RECIPES_PATH).get_recipe_by_id(id)
        except IOError:
            raise Exception("Cannot open the recipes file {}.".format(RECIPES_PATH))
