from settings import DOCKER_VARS
from models.core.container_conf import ContainerConf
from repository.omeka.MsgOmekaPackagesDAO import MsgOmekaPackagesDAO
from models.core.producer.producers.utils import MsgRelationItemsDAO, DevicePackages


class BuildContainerList:

    def __init__(self):
        self.CONTAINER_LIST = {
            "ideascube": self.ideascube,
            "kiwix": self.kiwix,
            "kolibri": self.kolibri
        }

    def get(self, msg):
        containers_conf = list()
        for c in self.CONTAINER_LIST.keys():
            containers_conf.append(self.CONTAINER_LIST[c](c, msg).to_dict())
        return containers_conf

    def kiwix(self, c, msg):
        name = c
        image_id = DOCKER_VARS['kiwix_prod_image']
        content = list()
        return ContainerConf(name, image_id, content)

    def kolibri(self, c, msg):
        name = c
        image_id = DOCKER_VARS['kolibri_prod_image']
        kolibri_content = MsgRelationItemsDAO.get_kolibri(msg)
        content = kolibri_content.language
        return ContainerConf(name, image_id, content)

    def ideascube(self, c, msg):
        name = c
        omeka_packages = MsgOmekaPackagesDAO.get_packages(msg)
        relation_items = MsgRelationItemsDAO.get_items(msg)

        device_packages = DevicePackages()
        device_packages.add_omeka_packages(omeka_packages)          # add omeka packages
        device_packages.add_kiwix_packages(relation_items)          # add kiwix packages

        image_id = DOCKER_VARS['ideascube_prod_image']
        content = device_packages.to_dict_no_parent()

        return ContainerConf(name, image_id, content)
