from models.core.message import Message

import json


class JsonMessageDAO:
    """Data Access Object to manage a `Message` instances in json formatted strings.
    """

    @staticmethod
    def get_message(body):
        """Get a `Message` instance from given string.

        Parameters
        ----------
        body : `unicode` or `bytes`
            String in json format to parse in the `Message`.

        Returns
        -------
        msg : `Message`
            The `Message` result from parsing the given string.

        Raises
        ------
        Exception
            Raised if the given ``body`` does not have the json format.

        See Also
        --------
        factory_manager.models.message.Message
        """
        try:
            data = json.loads(body.decode('utf-8'))
        except Exception:
            raise Exception("Message cannot be parsed, invalid json format.")

        return Message(data)
