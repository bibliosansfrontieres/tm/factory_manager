from models.core.cap_config import CapConfig
from repository.message.BuildContainerList import BuildContainerList
from repository.mongo.MongoDAO import MongoIO
from settings import PREPROD_MONGODB_CONNECTION_HOST, PREPROD_MONGODB_CONNECTION_PORT, \
        PREPROD_MONGODB_CONNECTION_USERNAME, PREPROD_MONGODB_CONNECTION_PASSWORD

KEYWORD_CAPCONF = "cap_config"


class BuildCAPConfDAO:
    """Data Access Object to extract a `CapConfig` instances from `Message` objects.
    """

    @staticmethod
    def make_config(msg):
        """Get a `RelationItems` instance from given `Message`.

        Parameters
        ----------
        msg : `Message`
            Message content to parse into `RelationItems`.

        Returns
        -------
        relation_items : `RelationItems`
            The `RelationItems` result from parsing the given ``msg``.

        Raises
        ------
        Exception
            Raised if ``msg`` does not contains the correct information for each relation item.
        """
        process_id = msg.process.id
        project_name = msg.process.project.name
        timezone = "Europe/Paris"
        cont_conf = BuildContainerList()
        containers = cont_conf.get(msg)

        return CapConfig(process_id, project_name, timezone, containers)

    @staticmethod
    def get_config(process_id):
        cursor = MongoIO(collection="configuration",
                         host=PREPROD_MONGODB_CONNECTION_HOST,
                         port=PREPROD_MONGODB_CONNECTION_PORT,
                         username=PREPROD_MONGODB_CONNECTION_USERNAME,
                         password=PREPROD_MONGODB_CONNECTION_PASSWORD)

        query = ({"process_id": process_id})
        projection = ({'_id': 0})
        conf = cursor.read_latest(query, projection=projection)
        return CapConfig(conf['process_id'],
                         project_name=conf['project_name'],
                         timezone=conf['timezone'],
                         containers=conf['containers'])
