from models.core.mongo_connection import MongoDBConnection
from settings import PREPROD_MONGODB_CONNECTION_HOST, PREPROD_MONGODB_CONNECTION_PORT, \
    PREPROD_MONGODB_CONNECTION_USERNAME, PREPROD_MONGODB_CONNECTION_PASSWORD, PREPROD_MONGODB_NAME
from repository.process.MongoProcessHandler import MongoProcessHandler
from pymongo import DESCENDING

COLLECTION = 'process'


class MongoProcessDAO:
    def __init__(self):
        self.connection = MongoDBConnection(
            PREPROD_MONGODB_CONNECTION_HOST,
            PREPROD_MONGODB_CONNECTION_PORT,
            PREPROD_MONGODB_CONNECTION_USERNAME,
            PREPROD_MONGODB_CONNECTION_PASSWORD,
            PREPROD_MONGODB_NAME
            )
        self.collection = self.connection.get_collection(COLLECTION)

    def get_process_by_id(self, process_id):
        """Get the process for given id.

        Parameters
        ----------
        process_id : `str`, optional
            Id of the process to get the detail.

        Returns
        -------
        process : `Object` of `process`
            Object instance of process for given id.
        """

        if process_id:  # Is process_id given?
            query = {
                '$and': [
                    {'id': process_id}
                ]
            }
        else:
            query = {}

        # query, projection
        cursor = self.collection.find(query, {'_id': 0}).sort('$natural', DESCENDING).limit(100)

        if process_id:
            try:
                process = cursor[0]
            except Exception:
                print("There is not such process")
                process = {}
        else:
            process = []
            for document in cursor:
                process.append(document)

        self.connection.close()
        return process

    def get_processes_by_recipe(self, recipe_id=None):
        """Get the list of process for given recipe id.

        Parameters
        ----------
        recipe_id : `int`, mandatory
            Id of the recipe.

        Returns
        -------
        processes : `list` of `str`
            A list of process_id.
        """

        query = MongoProcessHandler.formatRecipeID(recipe_id)

        # query, projection
        cursor = self.collection.find(query, {'_id': 0}).sort('$natural', DESCENDING)

        processes = []
        for document in cursor:
            processes.append(document)

        self.connection.close()
        return processes, 200

    def get_approval(self, process_id):

        query = MongoProcessHandler().formatID(process_id)
        cursor = self.collection.find(query, {'_id': 0, 'approval': 1})
        self.connection.close()

        if cursor.count() == 0:
            return False

        return cursor[0]['approval']['approved']

    def get_process_status(self, process_id):

        return self.get_process_by_id(process_id)['status']
