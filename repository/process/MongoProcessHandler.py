# -*- coding: utf-8 -*-

from models.core.mongo_connection import MongoDBConnection
from settings import PREPROD_MONGODB_CONNECTION_HOST, PREPROD_MONGODB_CONNECTION_PORT, \
    PREPROD_MONGODB_CONNECTION_USERNAME, PREPROD_MONGODB_CONNECTION_PASSWORD, PREPROD_MONGODB_NAME

COLLECTION = 'process'


class MongoProcessHandler():

    def __init__(self,
                 host=PREPROD_MONGODB_CONNECTION_HOST,
                 port=PREPROD_MONGODB_CONNECTION_PORT,
                 username=PREPROD_MONGODB_CONNECTION_USERNAME,
                 password=PREPROD_MONGODB_CONNECTION_PASSWORD,
                 db=PREPROD_MONGODB_NAME,
                 collection=COLLECTION
                 ):
        self._connection = MongoDBConnection(host=PREPROD_MONGODB_CONNECTION_HOST,
                                             port=PREPROD_MONGODB_CONNECTION_PORT,
                                             username=PREPROD_MONGODB_CONNECTION_USERNAME,
                                             password=PREPROD_MONGODB_CONNECTION_PASSWORD)
        self._collection = self._connection.get_collection(collection)

    def formatProcess(self, message):
        return message.process.to_dict()

    def formatID(self, process_id):
        return {
            'id': process_id
        }

    def formatApprove(self, message):
        return {
            'approval': message.process.approval.to_dict()
        }

    def formatDevices(self, deployment):
        return {
            'deployment.devices': deployment['deployment']['devices']
        }

    def formatDeployProcessID(self, deployment):
        return {
            'deployment.process_id': deployment['deployment']['process_id']
        }

    def formatParentProcess(self, deployment):
        return {
            'deployment.parent_process_id': deployment['deployment']['parent_process_id']
        }

    @staticmethod
    def formatRecipeID(recipe_id):
        return {
            'recipe.id': {"$in": recipe_id}
        }

    def emit(self, message):
        try:
            self._collection.insert_one(self.formatProcess(message))
        except Exception as e:
            raise Exception(
                "The data does not contains the correct format for a process : ".format(e))

    def read_latest(self, query, projection={}, sort_field='creation_time'):
        try:
            cursor = self._collection.find(query, projection).sort([(sort_field, -1)]).limit(1)
            return cursor[0]
        except Exception as e:
            print("Cannot find record in database : {}".format(e))
            return {}

    def update(self, filter, query):
        try:
            self._collection.update_one(filter, {"$set": query})
        except Exception:
            raise Exception(
                "The data does not contains the correct format for a process.")

    def count(self, query):
        return self._collection.count_documents(query)

    def close(self):
        self._connection.close()
