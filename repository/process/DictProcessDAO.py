from models.core.process import Process
from models.core.recipe import KEYWORD_RECIPE
from repository.recipe.DictRecipeDAO import DictRecipeDAO
from models.core.omeka_project import KEYWORD_PROJECT
from repository.omeka.DictProjectDAO import DictProjectDAO
from models.core.mc_config import KEYWORD_CONFIG
from repository.approval.DictApprovalDAO import DictApprovalDAO
from models.core.approval import KEYWORD_APPROVAL
from repository.deploy.DictDeployDAO import DictDeployDAO
from models.core.deploy_config import KEYWORD_DEPLOY
from repository.user.DictUserDAO import DictUserDAO
from models.core.user import KEYWORD_USER
from repository.media_center.DictMediaCenterConfigDAO import DictMediaCenterConfigDAO
from datetime import datetime

KEYWORD_PROCESS = "process"
"""`str` : Keyword used in dictionary to index a `Process`."""


class DictProcessDAO:
    """Data Access Object to manage a `Process` instances in dictionaries.
    """

    @staticmethod
    def get_process(data):
        """Get a `Process` instance from given dictionary.

        Parameters
        ----------
        data : `dict`
            Dictionary with the content of the `Process`. Should have the next fields:

            - ``KEYWORD_PROCESS``: dictionary with the content of a `Process`. Should have the next fields:

                - ``'id'``: id of the `Process`.
                - ``KEYWORD_RECIPE``: dictionary with the content of a `Recipe`.
                - ``KEYWORD_PROJECT``: dictionary with the content of a `OmekaProject`.
                - ``KEYWORD_CONFIG``: dictionary with the content of a `MediaCenterConfig`.

        Returns
        -------
        process : `Process`
            The `Process` result from parsing the ``data`` dictionary.

        Raises
        ------
        Exception
            Raised if the constant ``KEYWORD_PROCESS`` is not in ``data`` keys.
        Exception
            Raised if the content in ``KEYWORD_PROCESS`` does not contains the correct format for a `Process`.

        See Also
        --------
        factory_manager.models.process.Process
        factory_manager.models.recipe.KEYWORD_RECIPE
        factory_manager.models.omeka_project.KEYWORD_PROJECT
        factory_manager.models.mc_config.KEYWORD_CONFIG
        """
        if KEYWORD_PROCESS not in data:
            raise Exception("Missing '{}' in dictionary.".format(KEYWORD_PROCESS))

        process_data = data.get(KEYWORD_PROCESS)
        if not all(key in process_data.keys() for key in ['id',
                                                          'description',
                                                          'timestamp',
                                                          'image_url',
                                                          'status',
                                                          'work_dir',
                                                          'catalog_path',
                                                          KEYWORD_USER,
                                                          KEYWORD_RECIPE,
                                                          KEYWORD_PROJECT,
                                                          KEYWORD_CONFIG,
                                                          KEYWORD_APPROVAL,
                                                          KEYWORD_DEPLOY]):
            raise Exception("The data does not contains the correct format for a process, received : {}"
                            .format(process_data.keys()))

        process_id = process_data.get('id')
        description = process_data.get('description')
        timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
        image_url = process_data.get('image_url')
        status = process_data.get('status')
        work_dir = process_data.get('work_dir')
        catalog_path = process_data.get('catalog_path')
        recipe = DictRecipeDAO.get_recipe(process_data)
        project = DictProjectDAO.get_project_extended(process_data)
        user = DictUserDAO.get_user(process_data)
        config = DictMediaCenterConfigDAO.get_config(process_data)
        approval = DictApprovalDAO.get_approval(process_data)
        deploy = DictDeployDAO.get_config(process_data)

        return Process(process_id, recipe, project, user, config, description,
                       timestamp, approval, deploy, image_url, status, work_dir=work_dir, catalog_path=catalog_path)

    @staticmethod
    def update_process(data, process):
        """Update in the given dictionary a `Process`.

        Parameters
        ----------
        data : `dict`
            Dictionary to update in ``KEYWORD_PROCESS`` key.
        process : `Process`
            Content to update in ``data`` in ``KEYWORD_PROCESS`` key.

        See Also
        --------
        factory_manager.models.process.Process
        """
        if not isinstance(process, Process):
            raise TypeError("The content type to update in the dictionary must be a Process.")
        data[KEYWORD_PROCESS] = process.to_dict()
