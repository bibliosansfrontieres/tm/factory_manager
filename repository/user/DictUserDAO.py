from models.core.user import User, KEYWORD_USER


class DictUserDAO:
    """Data Access Object to manage a `OmekaProject` instances in dictionaries.
    """

    @staticmethod
    def get_user(data):

        user_data = dict() if data.get(KEYWORD_USER) is None else data.get(KEYWORD_USER)  # Do not fail if User empty
        username = "empty" if user_data.get('username') is None else user_data.get('username')
        password = str()

        return User(username=username, password=password)
