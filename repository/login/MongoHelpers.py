from repository.mongo.MongoDAO import MongoIO
from datetime import datetime, timedelta


class MongoHelpers:
    def __init__(self):
        self.cursor = MongoIO(collection="sessions")

    def is_authenticated(self, token):

        query = {
            "token": token
        }

        projection = {
            "validity": 1,
            "_id": 0
        }

        result = self.cursor.read_latest(query, projection=projection)

        #  spoof token validity to old date (day -1) in case it is not in database.
        result['validity'] = datetime.now() + timedelta(days=-1) \
            if result.get('validity') is None else result['validity']

        return result['validity'] > datetime.now()

    def add_session(self, token):

        query = {
            "token": token,
            "validity": datetime.now() + timedelta(hours=+8)
        }

        self.cursor.write(query)

    def flush_old_sessions(self):
        filter = {
            "validity": {
                "$lt": datetime.now()
            }
        }
        self.cursor.delete_all(filter)
