import ldap
from settings import LDAP_INFO


class LDAPLoginDAO:

    def __init__(self):
        """
        Init connection to LDAP directory server.
        """
        self.LDAP_INFO = LDAP_INFO
        ldap.OPT_NETWORK_TIMEOUT = 10

    def init_connection(self):
        return ldap.initialize(self.LDAP_INFO['LDAP_HOST'])

    def auth_user(self, username, password):
        search_filter = self.LDAP_INFO['USER_DN'] + username
        self.user_dn = search_filter + "," + self.LDAP_INFO['BASE_DN']
        self.connection = self.init_connection()

        login_result = dict()
        try:
            self.connection.bind_s(self.user_dn, password)
            login_result['logged'] = True
            login_result['message'] = "Login successful."
        except ldap.LDAPError as e:
            login_result['logged'] = False
            login_result['message'] = e.args[0]
        self.connection.unbind_s()
        return login_result
