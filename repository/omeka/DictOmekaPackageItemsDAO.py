from models.core.producer.producers.utils.omeka_package_item import OmekaPackageItem
from models.core.producer.producers.utils.omeka_package_item import KEYWORD_PACKAGE_ITEMS
from models.core.producer.producers.utils.omeka_package_items import OmekaPackageItems


class DictOmekaPackageItemsDAO:
    """Data Access Object to manage a `OmekaPackageItems` instances in `dict` objects.
    """

    @staticmethod
    def get_package_items(data):
        """Get a `OmekaPackageItems` instance from given `dict`.

        Parameters
        ----------
        data : `dict`
            Dictionary content to parse into `OmekaPackageItems`.

        Returns
        -------
        omeka_packages_items : `OmekaPackageItems`
            The `OmekaPackageItems` result from parsing the given ``data``.

        Raises
        ------
        Exception
            Raised if the constant ``KEYWORD_PACKAGE_ITEMS`` is not in ``data`` keys.
        Exception
            Raised if the content in ``KEYWORD_PROCESS`` does not contains the correct format for each item.
        """
        items_list = []

        if KEYWORD_PACKAGE_ITEMS not in data:
            raise Exception("Missing '{}' in the data.".format(KEYWORD_PACKAGE_ITEMS))
        items_data = data.get(KEYWORD_PACKAGE_ITEMS)

        for item_data in items_data:
            if not all(key in item_data for key in ['id',
                                                    'title',
                                                    'summary',
                                                    'lang',
                                                    'credits',
                                                    'collection',
                                                    'path',
                                                    'tags',
                                                    'preview',
                                                    'filename',
                                                    'file_url',
                                                    'thumbnail_url',
                                                    'mime_type']):
                raise Exception("The message does not contains the correct format for a package item.")

            item_id = item_data.get('id')
            title = item_data.get('title')
            summary = item_data.get('summary')
            lang = item_data.get('lang')
            item_credits = item_data.get('credits')
            collection = item_data.get('collection')
            path = item_data.get('path')
            tags = item_data.get('tags')
            preview = item_data.get('preview')
            filename = item_data.get('filename')
            file_url = item_data.get('file_url')
            thumbnail_url = item_data.get('thumbnail_url')
            mime_type = item_data.get('mime_type')

            new_item = OmekaPackageItem(item_id=item_id,
                                        title=title,
                                        summary=summary,
                                        lang=lang,
                                        item_credits=item_credits,
                                        collection=collection,
                                        path=path,
                                        tags=tags,
                                        filename=filename,
                                        file_url=file_url,
                                        thumbnail_url=thumbnail_url,
                                        mime_type=mime_type,
                                        preview=preview)
            items_list.append(new_item)

        return OmekaPackageItems(items_list)
