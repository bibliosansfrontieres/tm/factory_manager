from models.core.producer.producers.utils.omeka_package import KEYWORD_OMEKA_PACKAGES
from models.core.producer.producers.utils.omeka_package import OmekaPackage
from models.core.producer.producers.utils.omeka_packages import OmekaPackages
from .DictOmekaPackageItemsDAO import DictOmekaPackageItemsDAO


class MsgOmekaPackagesDAO:
    """Data Access Object to manage a `OmekaPackages` instances in `Message` objects.
    """

    @staticmethod
    def get_packages(msg):
        """Get a `OmekaPackages` instance from given `Message`.

        Parameters
        ----------
        msg : `Message`
            Message content to parse into `OmekaPackages`.

        Returns
        -------
        omeka_packages : `OmekaPackages`
            The `OmekaPackages` result from parsing the given ``msg``.

        Raises
        ------
        Exception
            Raised if ``msg`` does not contains the correct information for each packages.
        """
        packages_list = []
        packages_data = msg.get_value(KEYWORD_OMEKA_PACKAGES)

        for pkg_data in packages_data:
            if not all(key in pkg_data.keys() for key in ['id', 'slug', 'description', 'goal', 'language',
                                                          'last_exportable_modification', 'ideascube_name', 'items']):
                raise Exception("The message does not contains the correct format for a package.")

            pkg_id = pkg_data.get('id')
            slug = pkg_data.get('slug')
            description = pkg_data.get('description')
            goal = pkg_data.get('goal')
            language = pkg_data.get('language')
            last_exportable_modification = pkg_data.get('last_exportable_modification')
            ideascube_name = pkg_data.get('ideascube_name')
            subject = pkg_data.get('subject')
            related_items = DictOmekaPackageItemsDAO.get_package_items(pkg_data)

            new_pkg = OmekaPackage(pkg_id, slug, description, goal, language, last_exportable_modification,
                                   ideascube_name, subject, related_items)
            packages_list.append(new_pkg)

        return OmekaPackages(packages_list)

    @staticmethod
    def get_slug_list(msg):
        """Get a list of package slug from a given `Message`.

        Parameters
        ----------
        msg : `Message`
            Message content to parse.

        Returns
        -------
        packages_list : `list` of `str`

        Raises
        ------
        Exception
            Raised if a `OmekaPackage`` has no slug.
        """
        packages_list = []
        packages_data = msg.get_value(KEYWORD_OMEKA_PACKAGES)

        for pkg_data in packages_data:
            if 'slug' not in pkg_data.keys():
                raise Exception("A package has no slug!")

            slug = pkg_data.get('slug')
            packages_list.append({"name": slug, "status": "present"})

        return packages_list

    @staticmethod
    def update_packages(msg, omeka_packages):
        """Update in the given `Message` the `OmekaPackages`.

        Parameters
        ----------
        msg : `Message`
            Message to update.
        omeka_packages : `OmekaPackages`
            Content to update in ``msg`` in ``KEYWORD_PROCESS`` key.
        """
        msg.set_value(KEYWORD_OMEKA_PACKAGES, omeka_packages.to_dict())
