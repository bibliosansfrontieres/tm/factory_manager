from models.core.omeka_project import OmekaProject
from models.core.omeka_project import KEYWORD_PROJECT


class DictProjectDAO:
    """Data Access Object to manage a `OmekaProject` instances in dictionaries.
    """

    @staticmethod
    def get_project_base(data):
        """Get an `OmekaProject` instance by given minimal information inside dictionary.

        Parameters
        ----------
        data : `dict`
            Dictionary with the content of the `OmekaProject`. Should have the next fields:

            - ``'project_id'``: id of the `OmekaProject`.
            - ``'project_name'``: name of the `OmekaProject`.

        Returns
        -------
        project : `OmekaProject`
            The `OmekaProject` with the minimal information by result from parsing the given dictionary.

        Raises
        ------
        Exception
            Raised if ``'project_id'`` or ``'project_name'`` are not in ``data`` keys.

        See Also
        --------
        factory_manager.models.omeka_project.OmekaProject
        """

        project = dict() if data.get(KEYWORD_PROJECT) is None else data.get(KEYWORD_PROJECT)

        id = int() if project.get('id') is None else project.get('id')
        name = str() if project.get('name') is None else project.get('name')

        return OmekaProject(id, name)

    @staticmethod
    def get_project_extended(data):
        """Get a `OmekaProject` instance from given dictionary.

        Parameters
        ----------
        data : `dict`
            Dictionary with the content of the `OmekaProject`. Should have the next fields:

            - ``KEYWORD_PROJECT``: dictionary with the content of a `Process`. Should have the next fields:

                - ``'id'``: id of the `OmekaProject`.
                - ``'name'``: name of the `OmekaProject`
                - ``'title'``: title of the `OmekaProject`.
                - ``'description'``: description of the `OmekaProject`.
                - ``'language'``: language of the `OmekaProject`.
                - ``'device'``: device name of the `OmekaProject`.
                - ``'country'``: country name of the `OmekaProject`.

        Returns
        -------
        project : `OmekaProject`
            The `OmekaProject` result from parsing the ``data`` dictionary.

        Raises
        ------
        Exception
            Raised if the constant ``KEYWORD_PROJECT`` is not in ``data`` keys.
        Exception
            Raised if the content in ``KEYWORD_PROJECT`` does not contains the correct format for a `OmekaProject`.

        See Also
        --------
        factory_manager.models.omeka_project.OmekaProject
        factory_manager.models.omeka_project.KEYWORD_PROJECT
        """
        project_data = dict() if data.get(KEYWORD_PROJECT) is None else data.get(KEYWORD_PROJECT)

        project_id = int() if project_data.get('id') is None else project_data.get('id')
        name = str() if project_data.get('name') is None else project_data.get('name')
        title = str() if project_data.get('title') is None else project_data.get('title')
        description = str() if project_data.get('description') is None else project_data.get('description')
        language = str() if project_data.get('language') is None else project_data.get('language')
        device = str() if project_data.get('device') is None else project_data.get('device')
        country = str() if project_data.get('country') is None else project_data.get('country')

        return OmekaProject(project_id, name, title, description, language, device, country)
