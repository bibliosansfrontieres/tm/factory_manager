# -*- coding: utf-8 -*-
from datetime import datetime
from repository.mongo.MongoDAO import MongoIO
from repository.docker.DockerDAO import DockerDAO
from models.core.created_container import container_collection


class CleanUpMongoHelpers:
    def __init__(self, logger):
        self.logger = logger
        self.cursor = MongoIO(collection=container_collection)
        self.docker = DockerDAO(logger)
        self._now = datetime.now()

    def get_old_containers(self):

        query = {
            "expire": {
                "$lt": self._now
            }
        }

        projection = {
            "container_id": 1,
            "_id": 0
        }

        return self.cursor.read(query, projection=projection)

    def remove_old_containers(self):

        for container in self.get_old_containers():
            try:
                self.mark_as_deleted(container['container_id'])
                self.docker.remove(container['container_id'])
                self.logger.info('Removed container ID : {}'.format(container['container_id']))
            except Exception:
                pass

    def mark_as_deleted(self, container_id):
        query = {
            "container_id": container_id
        }
        newdata = {
            '$set': {
                'deleted': True
            }
        }
        self.cursor.update(query, newdata)
