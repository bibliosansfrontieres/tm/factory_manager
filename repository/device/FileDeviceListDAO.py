import json
from models.core.producer.producers.utils.device_list import DeviceList


class FileDeviceListDAO:

    @staticmethod
    def get_device_list(path):

        data = {}
        try:
            with open(path, "r") as file:
                try:
                    data = json.load(file)
                except Exception:
                    raise Exception(
                        "The file {} does not contains a valid .json format for a device list.".format(path))
        except IOError:
            pass

        return DeviceList(data)

    @staticmethod
    def update_device_list(path, device_list):

        try:
            with open(path, "w") as file:
                json.dump(device_list.to_dict(), file, sort_keys=True, indent=2)
        except IOError:
            raise IOError("Cannot open file {} to write.".format(path))
