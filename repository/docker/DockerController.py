# -*- coding: utf-8 -*-

from .DockerDAO import DockerDAO
from settings import DOCKER_VARS
from models.core.vm import VirtualMachineModel


class DockerController(DockerDAO):

    def __init__(self):

        super().__init__(dict())

    def list(self):
        """
        Get all containers and sort to have some meaningful output.
        """

        return [
                VirtualMachineModel(
                    container_id=i.id,
                    name=i.name,
                    project_name=i.name.split('.')[0],
                    process_id=i.name.split('.')[1],
                    mediacenter=i.name.split('.')[2],
                    url='http://' + i.name + '.' + DOCKER_VARS['DOCKER_DOMAIN'],
                    created_at=i.attrs['Created'],
                    owner="Unknown" if i.attrs['Config']['Labels'].get('owner')
                        is None else i.attrs['Config']['Labels']['owner']
                ).to_dict()
                for i in self.find_container_by_name(str())
                if ('ideascube' in i.name or 'olip'in i.name) and ('kolibri' not in i.name and 'api' not in i.name)
        ]

    def run(self, vars):
        """
        We don't want this method accessible from front. For now.
        """
        raise Exception('You are not allowed to use the run() methods from there.')

    def container_cleanup(self, name):
        """
        Same function as in parent object without dependency to logger.
        """
        result = self.find_container_by_name(name)
        if result:
            for container in result:
                self.remove(container.id)
