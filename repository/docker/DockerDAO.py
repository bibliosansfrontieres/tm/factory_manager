# -*- coding: utf-8 -*-

import docker
from os import environ
from settings import DOCKER_VARS
from repository.mongo.MongoDAO import MongoIO
from models.core.created_container import CreatedContainersModel
from models.core.created_container import container_collection


class DockerDAO:

    def __init__(self, logger, host=DOCKER_VARS['DOCKER_HOST']):

        self.logger = logger
        environ["DOCKER_HOST"] = DOCKER_VARS["DOCKER_HOST"]
        self.client = docker.from_env()

    def remove(self, id):
        """
        Simply deletes a container given its ID.
        """
        try:
            cont = self.client.containers.get(id)
            cont.remove(v=True, force=True)
        except docker.errors.NotFound:
            pass

    def _pause(self, id):
        """
        Simply pause a container given its ID.
        """
        try:
            cont = self.client.containers.get(id)
            cont.pause()
        except docker.errors.NotFound:
            pass

    def push(self, tag):
        """
        Push to docker hub.
        """
        creds = {
            "username": DOCKER_VARS['HUB_USER'],
            "password": DOCKER_VARS['HUB_PASS']
        }

        try:
            for line in self.client.images.push(tag, stream=True, decode=True, auth_config=creds):
                self.logger.debug(line)
                if isinstance(line, dict):  # grab docker errors if any as it won't necessarily raise an Exception.
                    if "errorDetail" in line.keys():
                        self.logger.warning("Can't push image {} : {}".format(tag, line['errorDetail'].get('message')))
        except Exception:
            self.logger.warning('Something went wrong pushing new {} image to DockerHub'.format(tag))

    def build(self, dockerfile_path, tag):
        """
        Build an image.
        """
        try:
            self.client.images.build(path=dockerfile_path, tag=tag)
        except Exception as e:
            self.logger.warning('Failed building image {}, continuing anyway. err({})'.format(tag, e))

    def find_container_by_name(self, name):
        """
        Returns a list of container objects.
        """
        return self.client.containers.list(filters={'name': name})

    def container_cleanup(self, name):
        """
        Check if a container with given name exists. If so, delete.
        """
        result = self.find_container_by_name(name)
        if result:
            for container in result:
                self.logger.info('Removing existing docker container with name {}'.format(container.name))
                self.remove(container.id)

    def freeze(self, name):
        """
        Get all containers with given name. If so, pause it.
        """
        result = self.find_container_by_name(name)
        if result:
            for container in result:
                self.logger.info('Pausing existing docker container with name {}'.format(container.name))
                self._pause(container.id)

    def update_container_list(self, vars, id):
        """
        Update container creation database with newly created container.
        """

        cont_obj = CreatedContainersModel(
            name=vars.name,
            container_id=id,
            expire=vars._msg.process.config.expire_to_datetime,
            process_id=vars._msg.process.id
            )

        cur = MongoIO(collection=container_collection)
        cur.write(cont_obj.to_dict())
        cur.close()

    def run(self, vars):
        """
            Run container with all defined container_vars.

        Parameters
        ----------
        vars : `AbstractDockerVars`, mandatory
            Object AbstractDockerVars.

        Returns
        -------
        Nothing
        Todo : Error handling.
        """
        self.logger.info('Build docker image for {}'.format(vars.name))
        self.container_cleanup(vars.name)

        result = self.client.containers.run(
            vars.image,
            detach=vars.detach,
            remove=vars.remove,
            volumes_from=vars.volumes_from,
            volumes=vars.volumes,
            name=vars.name,
            labels=vars.labels,
            network=vars.network,
            environment=vars.environment,
            privileged=vars.privileged,
            command=vars.command
        )

        self.update_container_list(vars, result.id)

        if vars.wait:
            result.wait()

        self.logger.debug('Container status : {}'.format(result.status))

        if vars.announce_container['announce']:
            try:
                self.logger.info("New container will be available at {image_url}".format(
                                 image_url=vars.announce_container['image_url']))
            except KeyError:
                self.logger.error('No URL found for image link.')
