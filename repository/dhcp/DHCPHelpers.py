from copy import deepcopy


class DHCPHelpers:
    def __init__(self):
        pass

    def get_device_deployment_status(self, mac_address):
        from services.deploy.deploy import DeployService
        job = DeployService.get_device_job(mac_address)[0]

        try:
            return None if job == dict() else job['project']['name']
        except KeyError:
            raise KeyError(job)

    def add_project_name(self, leases):
        """
        Add project name to each leases if device has already been deployed.
        For information, leases structure is :
        [
            {
              "arguments": {
                "leases": [
                  {
                    "cltt": 1556805878,
                    "fqdn-fwd": false,
                    "fqdn-rev": false,
                    "hostname": "",
                    "hw-address": "f4:4d:30:c2:7c:71",
                    "ip-address": "10.10.30.14",
                    "state": 0,
                    "subnet-id": 1,
                    "valid-lft": 3600
                  }
                ]
              }
            }
        ]
        """
        new_leases = deepcopy(leases)
        new_leases[0]['arguments']['leases'].clear()

        for lease in leases[0]['arguments']['leases']:
            lease['project_name'] = self.get_device_deployment_status(lease['hw-address'])
            new_leases[0]['arguments']['leases'].append(lease)

        return new_leases
