from repository.dhcp.DHCPHelpers import DHCPHelpers
from settings import KEA_API
import requests


class dhcpDAO:
    def __init__(self):
        self.KEA_API = KEA_API

    def get_lease(self, mac_address):
        params = {
                  "command": "lease4-get",
                  "arguments": {"identifier-type": "hw-address",
                                "identifier": mac_address,
                                "subnet-id": 1},
                  "service": ["dhcp4"]
                 }
        r = requests.post(self.KEA_API, json=params)
        return r.json()

    def get_all_leases(self):
        params = {
                  "command": "lease4-get-all",
                  "service": ["dhcp4"]
                 }
        r = requests.post(self.KEA_API, json=params)
        return r.json()

    def get_all_leases_with_content(self):
        return DHCPHelpers().add_project_name(self.get_all_leases())
