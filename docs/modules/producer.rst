Producer
########

Description of Producer.

.. autoclass:: factory_manager.models.abstract_producer.AbstractProducer
   :members:
   :inherited-members:
   :member-order: bysource