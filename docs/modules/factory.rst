Factory
#######

Description of factory.

.. autoclass:: factory_manager.models.abstract_factory.AbstractFactory
   :members:
   :inherited-members:
   :member-order: bysource