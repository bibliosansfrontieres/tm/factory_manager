Data Access Object
##################

Description of the dao (and why this name).

Message
=======

.. autoclass:: factory_manager.models.message.JsonMessageDAO
   :members:
   :inherited-members:
   :member-order: bysource

Process
=======

.. autoclass:: factory_manager.models.process.DictProcessDAO
   :members:
   :inherited-members:
   :member-order: bysource

Omeka Project
=============
.. autoclass:: factory_manager.models.omeka_project.DictProjectDAO
   :members:
   :inherited-members:
   :member-order: bysource

Recipe
======

.. autoclass:: factory_manager.models.recipe.DictRecipeDAO
   :members:
   :inherited-members:
   :member-order: bysource

.. autoclass:: factory_manager.models.recipe.FileRecipesDAO
   :members:
   :inherited-members:
   :member-order: bysource

Media Center Configuration
==========================

.. autoclass:: factory_manager.models.mc_config.DictMediaCenterConfigDAO
   :members:
   :inherited-members:
   :member-order: bysource
