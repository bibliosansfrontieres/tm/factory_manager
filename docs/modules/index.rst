Module Documentation
####################

For the developer, Factory Manager is organized in small
set of objects.

.. toctree::
   :hidden:
   :maxdepth: 2

   factory
   producer
   message
   process
   project
   recipe
   mc_config
   result
   constants
   dao

   omeka/index