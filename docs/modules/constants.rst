Constants
#########

Description of constants.

.. autodata:: factory_manager.models.process.KEYWORD_PROCESS
   :annotation:

.. autodata:: factory_manager.models.omeka_project.KEYWORD_PROJECT
   :annotation:

.. autodata:: factory_manager.models.recipe.KEYWORD_RECIPE
   :annotation:

.. autodata:: factory_manager.models.mc_config.KEYWORD_CONFIG
   :annotation:

.. autoclass:: factory_manager.models.result.ResultType
   :members:
