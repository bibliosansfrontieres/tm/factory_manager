Recipe
######

Description of recipe.

Recipe
======

Description of Recipe.

.. autoclass:: factory_manager.models.recipe.Recipe
   :members:
   :inherited-members:
   :member-order: bysource

Recipes
=======

Description of Recipes.

.. autoclass:: factory_manager.models.recipe.Recipes
   :members:
   :inherited-members:
   :member-order: bysource