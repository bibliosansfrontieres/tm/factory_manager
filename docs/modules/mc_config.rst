Media Center Configuration
##########################

Description of config.

.. autoclass:: factory_manager.models.mc_config.MediaCenterConfig
   :members:
   :inherited-members:
   :member-order: bysource