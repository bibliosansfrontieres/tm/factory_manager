Producers Results
#################

Why use Results.

Producer Result
===============

Description of result.

.. autoclass:: factory_manager.models.result.ProducerResult
   :members:
   :inherited-members:
   :member-order: bysource

Producers Results
=================

Description of results.

.. autoclass:: factory_manager.models.result.ProducersResults
   :members:
   :inherited-members:
   :member-order: bysource