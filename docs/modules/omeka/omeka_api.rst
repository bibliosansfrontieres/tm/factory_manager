Omeka API
#########

Description of Omeka API.

.. autoclass:: factory_manager.models.producers.utils.omeka_api.OmekaApi
   :members:
   :inherited-members:
   :member-order: bysource
