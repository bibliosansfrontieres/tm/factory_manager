Relation Item
#############

Description of Relation Item.

RelationItem
================

Description of RelationItem

.. autoclass:: factory_manager.models.producers.utils.relation_item.RelationItem
   :members:
   :inherited-members:
   :member-order: bysource

RelationItems
==================

Description of RelationItems

.. autoclass:: factory_manager.models.producers.utils.relation_item.RelationItems
   :members:
   :inherited-members:
   :member-order: bysource