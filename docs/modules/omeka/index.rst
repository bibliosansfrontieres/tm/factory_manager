Omeka modules Documentation
===========================

For bsf, several classes were developed to facilitate
the work and organization related to Omeka.

.. toctree::
   :hidden:
   :maxdepth: 1

   omeka_package
   omeka_item
   relation_item
   omeka_api
   dao