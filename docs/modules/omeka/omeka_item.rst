Omeka Package Item
##################

Description of Omeka packages items.

This example show the main usage of `OmekaPackageItem` from given OmekaPackage ``pkg``::

    .....

    # get `OmekaPackageItems` from `pkg`
    items = pkg.items

    # iterate over the items.
    for item in items.get_items_list():  # item: `OmekaPackageItem`

        # properties of `OmekaPackageItem`
        id = item.id
        title = item.title
        summary = item.summary
        language = item.language
        credits = item.credits
        collection = item.collection
        path = item.tags
        preview = item.preview

        # methods
        errors = item.get_errors()      # errors: `ItemError`

    .....

OmekaPackageItem
================

Description of OmekaPackageItem

.. autoclass:: factory_manager.models.producers.utils.omeka_package_item.OmekaPackageItem
   :members:
   :inherited-members:
   :member-order: bysource

OmekaPackagesItems
==================

Description of OmekaPackagesItems

.. autoclass:: factory_manager.models.producers.utils.omeka_package_item.OmekaPackageItems
   :members:
   :inherited-members:
   :member-order: bysource
