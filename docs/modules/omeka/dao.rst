Data Access Object
##################

Description of the dao.

OmekaPackages
=============

.. autoclass:: factory_manager.models.producers.utils.omeka_package.MsgOmekaPackagesDAO
   :members:
   :inherited-members:
   :member-order: bysource

OmekaPackagesItems
==================

.. autoclass:: factory_manager.models.producers.utils.omeka_package_item.DictOmekaPackageItemsDAO
   :members:
   :inherited-members:
   :member-order: bysource

RelationsItems
==============
.. autoclass:: factory_manager.models.producers.utils.relation_item.MsgRelationItemsDAO
   :members:
   :inherited-members:
   :member-order: bysource