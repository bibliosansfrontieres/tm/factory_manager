Omeka Package
#############

Description of Omeka packages.

This example show the main usage of `OmekaPackage` inside a `Producer` execute
function from given Message ``msg``::

    def execute(msg, logger):

        # get `OmekaPackages` from `Message`
        omeka_packages = MsgOmekaPackagesDAO.get_packages(msg)

        # iterate over the packages.
        for pkg in omeka_packages.get_packages_list():  # pkg: `OmekaPackage`

            # properties of `OmekaPackage`
            id = pkg.id
            slug = pkg.slug
            description = pkg.description
            goal = pkg.goal
            language = pkg.language
            last_exportable_modification = pkg.last_exportable_modification
            ideascube_name = pkg.ideascube_name
            items = pkg.items                           # items: `OmekaPackageItems`

        .....

OmekaPackage
============

Description of OmekaPackage

.. autoclass:: factory_manager.models.producers.utils.omeka_package.OmekaPackage
   :members:
   :inherited-members:
   :member-order: bysource

OmekaPackages
=============

Description of OmekaPackages

.. autoclass:: factory_manager.models.producers.utils.omeka_package.OmekaPackages
   :members:
   :inherited-members:
   :member-order: bysource