Introduction
############

Basic Concepts
==============

Quick review of important concepts you should have on mind.

* `Process`: Program specified by a `Recipe` during execution.
* `Factory`: Group of `Producers`.
* `Producer`: Small step of several that constitutes a program.
* `Recipe`: Group of `Factories` where order matters and define the steps of a `Process`.
* `Recipes File`: File where all the `Recipes` are listed in .json format.

Installation
============

You can install Factory Manager via pip_::

    pip install -e "git+https://gitlab.com/bibliosansfrontieres/tm/factory_manager"

Once Factory Manager is installed, you can run ``factory_manager --help`` to see basic usages options.

Dependencies
------------

When Factory Manager is installed, the following dependent Python packages should be automatically installed without any
action on your part:

* pika_, use of the AMQP 0-9-1 protocol to keep the asynchrony between the factories.

Upgrading
---------

If you installed a stable Factory Manager release via pip and wish to upgrade to the latest stable release, you can do
so doing::

    pip install -- upgrade -e "git+https://gitlab.com/bibliosansfrontieres/tm/factory_manager"

.. Links

.. _pip: http://www.pip-installer.org/
.. _pika: https://pypi.org/project/pika/