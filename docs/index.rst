Factory Manager |release|
#########################

.. ifconfig:: release.endswith('.dev')

   .. warning::

      This documentation is for the version of factory_manager currently under development.

Factory Manager is an application to automatize deployment process
for BSF_ projects, written in Python_. Highlights include:

* Asynchronous functioning between each job.
* Easy to organize and expand.

Ready to get started? Check out the :doc:`Quickstart<quickstart>` guide.

Source code
===========

You can access the source code at: https://gitlab.com/bibliosansfrontieres/tm/factory_manager

Documentation
=============

.. toctree::
   :maxdepth: 3

   intro
   quickstart
   modules/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`

.. Links

.. _BSF: https://www.bibliosansfrontieres.org/
.. _Python: http://www.python.org/
