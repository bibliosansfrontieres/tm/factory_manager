Quickstart
##########

Following are some quick steps and examples to get started.

Installation
============

.. warning::

    Not implemented. Coming soon !!!

Install FactoryManager on Python by running the command in your terminal, prefixing with ``sudo`` if permissions
warrant::

    $ pip install -e "git+http://gogs.cinema.montreuil.wan.bsf-intranet.org/reinier/factory_manager"

Prepare workspace
=================

.. warning::

    Not implemented. Coming soon !!!

From the command line, `cd` into a directory where you’d like to store your code, then run the following command::

    $ factory_manager --init-workspace project_name

This will create a project_name directory in your current directory, with the next structure::

    workspace_name
    ├── __data__
    │   └── recipes.json
    ├── factories
    │   ├── FactoryExample.py
    │   └── __init__.py
    ├── producers
    │   ├── __init__.py
    │   └── ProducerExample.py
    ├── launcher.py
    └── settings.py

Create a factory
================

First of all, create a class ``FactoryExample`` inheriting from class ``AbstractFactory`` with a fixed objective, the
name should express its purpose (See `naming convention`).

Example::

    # factories/FactoryExample.py
    from factory_manager.models import AbstractFactory


    class FactoryExample(AbstractFactory):

        # `list` of `AbstractProducers`: abstract property inherited from AbstractFactory.
        producers = [
            # instance of producer that will be created next.
            ProducerExample01(),
        ]

.. note::

    The ``FactoryExample`` is created by default when workspace is created.

Create a producer
=================

With the ``FactoryExample`` ready, we proceed with the creation of its producers who inherit from class
``AbstractProducer``: in this case the class ``ProducerExample01`` is the only one.

Example::

    # producers/ProducerExample01.py
    from factory_manager.models import AbstractProducer


    class ProducerExample01(AbstractProducer):

        # abstract method inherited from AbstractProducer.
        def execute(self, msg, logger):
            # your code here

.. note::

    The ``ProducerExample01`` is created by default when workspace is created.

Create a recipe
===============

| Depending on the process to run, we create a list of recipes that can be executed in a file in json format. Each
  recipe consist of a dictionary with the pairs: ``"id": <int>`` and ``"factories": <list of str>``.
| In this case we only create a recipe with the ``FactoryExample``

Example::

    # __data__/recipes.json
    [
        {
            "id": 1,
            "factories": ["FactoryExample"]
        },
    ]

.. note::

    The recipes.json file is created by default when workspace is created.

Configuration
=============

| The file `settings.py` contains the configuration of the program.
| Stablish the correct configuration for the RabbitMQ server before start execution of the program.

Example::

    ...

    RABBITMQ_CONNECTION_PARAMETERS = {
        'host': "127.0.0.1",            # RabbitMQ server address
        'port': 8080,                   # RabbitMQ server port
        'vhost': 'central_vhost',       # RabbitMQ virtual host
        'username': 'nc-user',          # Example user
        'password': '^$JSgO?7o2(^8na'   # Example password
    }

    ...

Running process
===============

.. warning::

    Not implemented. Coming soon !!!
    For the moment run the app.py

Run the `launcher.py` application with the following command to launch one Factory Manager::

    $ python launcher.py

While the factory manager is running you can interact via the console, allowing you to start factories by name or start
complete recipes by id.
To run the example recipe 1 found in the ``settings.RECIPES_PATH``::

    >>> [*] Waiting for commands. Command 'help' for more information.\n To exit CTRL+C or 'quit' command.")
    >>> lr 1
    [+] Successful launched factory FactoryExample.
