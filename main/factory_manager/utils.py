# -*- coding: utf-8 -*-

import importlib


def import_string(module_path, class_name):
    path = module_path + '.' + class_name

    try:
        module = importlib.import_module(path)
    except ImportError as e:
        raise Exception("Module {} cannot be imported : {}".format(path, e))

    try:
        return getattr(module, class_name)
    except AttributeError:
        raise Exception("Module {} does not define a {} attribute/class.".format(path, class_name))
