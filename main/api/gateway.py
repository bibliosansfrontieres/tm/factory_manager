# -*- coding: utf-8 -*-

from models import Gateway
from settings import RABBITMQ_CONNECTION_HOST, RABBITMQ_CONNECTION_PORT, RABBITMQ_CONNECTION_VHOST,\
    RABBITMQ_CONNECTION_USERNAME, RABBITMQ_CONNECTION_PASSWORD


gateway = Gateway(RABBITMQ_CONNECTION_HOST,
                  RABBITMQ_CONNECTION_PORT,
                  RABBITMQ_CONNECTION_VHOST,
                  RABBITMQ_CONNECTION_USERNAME,
                  RABBITMQ_CONNECTION_PASSWORD)
