# -*- coding: utf-8 -*-

import os
import sys
from flask_restplus import Api
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from controllers import ns_gateway, ns_logs, ns_deploy, ns_process, ns_approval, ns_login, ns_vm  # noqa: E402


api = Api(
    version="0.2.0",
    title="temps-modernes API",
    description="REST API to interact with the gateway in temps-modernes."
)

api.add_namespace(ns_gateway)
api.add_namespace(ns_logs)
api.add_namespace(ns_process)
api.add_namespace(ns_deploy)
api.add_namespace(ns_approval)
api.add_namespace(ns_login)
api.add_namespace(ns_vm)
