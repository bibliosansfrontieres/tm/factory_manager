import unittest
from unittest import mock
import requests

from models.core.omeka_project import OmekaProject


from models.core.producer.producers.utils import OmekaApi, OmekaPackage
from models.core.producer.producers.utils.omeka_package_items import OmekaPackageItems

from settings import OMEKA_API_KEY, OMEKA_BSF_PROJECT_TYPE_ID

project_json = {
    "id": 9225,
    "url": "https://omeka.bsf-intranet.org/api/items/9225",
    "public": True,
    "featured": False,
    "added": "2017-03-13T08:45:53+00:00",
    "modified": "2017-09-29T09:25:42+00:00",
    "item_type": {
        "id": OMEKA_BSF_PROJECT_TYPE_ID,
        "url": "https://omeka.bsf-intranet.org/api/item_types/5",
        "name": "BSFProject",
        "resource": "item_types"
    },
    "collection": {
        "id": 31,
        "url": "https://omeka.bsf-intranet.org/api/collections/31",
        "resource": "collections"
    },
    "owner": {
        "id": 1,
        "url": "https://omeka.bsf-intranet.org/api/users/1",
        "resource": "users"
    },
    "files": {
        "count": 0,
        "url": "https://omeka.bsf-intranet.org/api/files?item=9225",
        "resource": "files"
    },
    "tags": [{
        "id": 244,
        "url": "https://omeka.bsf-intranet.org/api/tags/244",
        "name": "\u0627\u0637\u0641\u0627\u0644",
        "resource": "tags"
    }, {
        "id": 241,
        "url": "https://omeka.bsf-intranet.org/api/tags/241",
        "name": "\u062d\u0642\u0648\u0642",
        "resource": "tags"
    }],
    "element_texts": [{
        "html": False,
        "text": "Foo",
        "element_set": {
            "id": 1,
            "url": "https://omeka.bsf-intranet.org/api/element_sets/1",
            "name": "Dublin Core",
            "resource": "element_sets"
        },
        "element": {
            "id": 50,
            "url": "https://omeka.bsf-intranet.org/api/elements/50",
            "name": "Title",
            "resource": "elements"
        }
    }, {
        "html": False,
        "text": "Lorem ipsum ...",
        "element_set": {
            "id": 1,
            "url": "https://omeka.bsf-intranet.org/api/element_sets/1",
            "name": "Dublin Core",
            "resource": "element_sets"
        },
        "element": {
            "id": 41,
            "url": "https://omeka.bsf-intranet.org/api/elements/41",
            "name": "Description",
            "resource": "elements"
        }
    }, {
        "html": False,
        "text": "Bar",
        "element_set": {
            "id": 1,
            "url": "https://omeka.bsf-intranet.org/api/element_sets/1",
            "name": "Dublin Core",
            "resource": "element_sets"
        },
        "element": {
            "id": 262,
            "url": "https://omeka.bsf-intranet.org/api/elements/262",
            "name": "Device",
            "resource": "elements"
        }
    }, {
        "html": False,
        "text": "fr",
        "element_set": {
            "id": 1,
            "url": "https://omeka.bsf-intranet.org/api/element_sets/1",
            "name": "Dublin Core",
            "resource": "element_sets"
        },
        "element": {
            "id": 44,
            "url": "https://omeka.bsf-intranet.org/api/elements/44",
            "name": "Language",
            "resource": "elements"
        }
    }],
    "extended_resources": []
}
packages_relations = [
    {
        "id": 10,
        "package_id": 6,
        "item_id": 16954,
        "extended_resources": []
    }, {
        "id": 11,
        "package_id": 13,
        "item_id": 16954,
        "extended_resources": []
    }, {
        "id": 13,
        "package_id": 14,
        "item_id": 16954,
        "extended_resources": []
    }
]
package_json = {
    "id": 21420,
    "slug": "Foo",
    "description": "Small description",
    "language": "fr",
    "last_exportable_modification": "2018-10-10 12:00:00",
    "ideascube_name": "id-cube",
    "relations": [{
            "item_id": 16954
        },
        {
            "item_id": 18191
        },
    ]
}
item_json = {
    "id": 445,
    "url": "https://omeka.bsf-intranet.org/api/items/9225",
    "public": True,
    "featured": False,
    "added": "2017-03-13T08:45:53+00:00",
    "modified": "2017-09-29T09:25:42+00:00",
    "item_type": {
        "id": OMEKA_BSF_PROJECT_TYPE_ID,
        "url": "https://omeka.bsf-intranet.org/api/item_types/5",
        "name": "Sound",
        "resource": "item_types"
    },
    "collection": {
        "id": 31,
        "url": "https://omeka.bsf-intranet.org/api/collections/31",
        "resource": "collections"
    },
    "owner": {
        "id": 1,
        "url": "https://omeka.bsf-intranet.org/api/users/1",
        "resource": "users"
    },
    "files": {
        "count": 0,
        "url": "https://omeka.bsf-intranet.org/api/files?item=9225",
        "resource": "files"
    },
    "tags": [{
        "id": 244,
        "url": "https://omeka.bsf-intranet.org/api/tags/244",
        "name": "\u0627\u0637\u0641\u0627\u0644",
        "resource": "tags"
    }, {
        "id": 241,
        "url": "https://omeka.bsf-intranet.org/api/tags/241",
        "name": "\u062d\u0642\u0648\u0642",
        "resource": "tags"
    }],
    "element_texts": [{
        "html": False,
        "text": "Foo",
        "element_set": {
            "id": 1,
            "url": "https://omeka.bsf-intranet.org/api/element_sets/1",
            "name": "Dublin Core",
            "resource": "element_sets"
        },
        "element": {
            "id": 50,
            "url": "https://omeka.bsf-intranet.org/api/elements/50",
            "name": "Title",
            "resource": "elements"
        }
    }, {
        "html": False,
        "text": "Lorem ipsum ...",
        "element_set": {
            "id": 1,
            "url": "https://omeka.bsf-intranet.org/api/element_sets/1",
            "name": "Dublin Core",
            "resource": "element_sets"
        },
        "element": {
            "id": 41,
            "url": "https://omeka.bsf-intranet.org/api/elements/41",
            "name": "Description",
            "resource": "elements"
        }
    }, {
        "html": False,
        "text": "fr",
        "element_set": {
            "id": 1,
            "url": "https://omeka.bsf-intranet.org/api/element_sets/1",
            "name": "Dublin Core",
            "resource": "element_sets"
        },
        "element": {
            "id": 44,
            "url": "https://omeka.bsf-intranet.org/api/elements/44",
            "name": "Language",
            "resource": "elements"
        }
    }, {
        "html": False,
        "text": "credits",
        "element_set": {
            "id": 1,
            "url": "https://omeka.bsf-intranet.org/api/element_sets/1",
            "name": "Dublin Core",
            "resource": "element_sets"
        },
        "element": {
            "id": 39,
            "url": "https://omeka.bsf-intranet.org/api/elements/39",
            "name": "Credits",
            "resource": "elements"
        }
    }, {
        "html": False,
        "text": "example_path",
        "element_set": {
            "id": 1,
            "url": "https://omeka.bsf-intranet.org/api/element_sets/1",
            "name": "Dublin Core",
            "resource": "element_sets"
        },
        "element": {
            "id": 256,
            "url": "https://omeka.bsf-intranet.org/api/elements/256",
            "name": "Path",
            "resource": "elements"
        }
    }],
    "extended_resources": []
}


class TestCaseOmekaApi(unittest.TestCase):

    def test_api_requests(self):
        url_packages = OmekaApi.OMEKA_PACKAGES_URL
        url_package_items = OmekaApi.OMEKA_ITEMS_URL
        url_packages_relations = OmekaApi.OMEKA_PACKAGE_RELATIONS_URL
        url_items_relations = OmekaApi.OMEKA_ITEM_RELATIONS_URL

        self.assertEqual(requests.get(url_packages, params={"key": OMEKA_API_KEY}).status_code,
                         requests.codes.ok, msg="Failed request to {}".format(url_packages))
        self.assertEqual(requests.get(url_package_items, params={"key": OMEKA_API_KEY}).status_code,
                         requests.codes.ok, msg="Failed request to {}".format(url_package_items))
        self.assertEqual(requests.get(url_packages_relations, params={"key": OMEKA_API_KEY}).status_code,
                         requests.codes.ok, msg="Failed request to {}".format(url_packages_relations))
        self.assertEqual(requests.get(url_items_relations, params={"key": OMEKA_API_KEY}).status_code,
                         requests.codes.ok, msg="Failed request to {}".format(url_items_relations))

    def test_get_metadata_from_elements(self):
        element_texts = project_json['element_texts']

        self.assertEqual(OmekaApi._get_item_title(element_texts), "Foo")
        self.assertEqual(OmekaApi._get_item_summary(element_texts), "Lorem ipsum ...")
        self.assertEqual(OmekaApi._get_item_language(element_texts), "fr")
        self.assertEqual(OmekaApi._get_item_device(element_texts), "Bar")


def mocked_requests_get(*args, **kwargs):

    class MockResponse:

        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    url_test_project = OmekaApi.OMEKA_ITEMS_URL + '9225'
    url_test_packages = OmekaApi.OMEKA_PACKAGE_RELATIONS_URL
    url_test_package = OmekaApi.OMEKA_PACKAGES_URL + '21420'
    url_test_item = OmekaApi.OMEKA_ITEMS_URL + '445'

    if args[0] == url_test_project:
        return MockResponse(project_json, 200)
    elif args[0] == url_test_packages:
        return MockResponse(packages_relations, 200)
    elif args[0] == url_test_package:
        return MockResponse(package_json, 200)
    elif args[0] == url_test_item:
        return MockResponse(item_json, 200)

    return MockResponse(None, 404)


class TestCaseOmekaProject(unittest.TestCase):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_project_update(self, *args, **kwargs):

        test_project = OmekaProject(9225, "test-project")
        OmekaApi.update_project(test_project)

        self.assertEqual(test_project.id, 9225)
        self.assertEqual(test_project.title, "Foo")
        self.assertEqual(test_project.description, "Lorem ipsum ...")
        self.assertEqual(test_project.language, "fr")
        self.assertEqual(test_project.device, "Bar")


def mocked_get_package_items(*args, **kwargs):
    return OmekaPackageItems()


def mocked_get_package(pkg_id):
    if pkg_id == 6:
        return OmekaPackage(pkg_id, 's1', '...', 'goal', 'lang', '10-10-2018', 'ideas_cube', [])
    elif pkg_id == 13:
        return OmekaPackage(pkg_id, 's2', '...', 'goal', 'lang', '10-10-2018', 'ideas_cube', [])
    elif pkg_id == 14:
        return OmekaPackage(pkg_id, 's3', '...', 'goal', 'lang', '10-10-2018', 'ideas_cube', [])

    raise Exception("""Function mock: unexpected package id (Check each package_id in `packages_relation`
                    dictionary)""")


class TestCaseOmekaPackages(unittest.TestCase):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('models.core.producer.producers.utils.OmekaApi.get_items',
                side_effect=mocked_get_package_items)
    def test_get_package(self, *args, **kwargs):
        pkg = OmekaApi.get_package_by_id(21420)

        self.assertEqual(pkg.slug, 'Foo')
        self.assertEqual(pkg.description, 'Small description')
        self.assertEqual(pkg.language, 'fr')

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('models.core.producer.producers.utils.OmekaApi.get_package_by_id',
                side_effect=mocked_get_package)
    def test_get_packages(self, *args, **kwargs):
        omeka_packages = OmekaApi.get_packages_by_project_id(project_id=16954)
        pkg_list = omeka_packages._packages_list

        self.assertEqual(len(pkg_list), 3)
        self.assertEqual(pkg_list[0].slug, 's1')
        self.assertEqual(pkg_list[1].slug, 's2')
        self.assertEqual(pkg_list[2].slug, 's3')


def mocked_collection(*args, **kwargs):
    return None


class TestCaseOmekaPackageItems(unittest.TestCase):

    @mock.patch('models.core.producer.producers.utils.OmekaApi._get_item_collection',
                side_effect=mocked_collection)
    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_get_items(self, *args, **kwargs):
        item = OmekaApi.get_item_by_id(445)

        self.assertEqual(item.title, "Foo")
        self.assertEqual(item.summary, "Lorem ipsum ...")
        self.assertEqual(item.language, "fr")
        self.assertEqual(item.credits, "credits")
        self.assertEqual(item.path, "example_path")
        self.assertEqual(item.collection, "no collection")


if __name__ == '__main__':
    unittest.main()
