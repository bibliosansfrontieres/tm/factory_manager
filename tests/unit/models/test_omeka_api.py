from models.core.producer.producers.utils.omeka_api import OmekaApi
from copy import deepcopy
from unittest import mock
import pytest
import requests


element_texts = [
    {
        'element': {
            'id': 274,
            'name': 'sha256sum',
            'resource': 'elements',
            'url': 'https://omeka.bsf-intranet.org/api/elements/274'
            },
        'element_set': {
            'id': 3,
            'name': 'Item Type Metadata',
            'resource': 'element_sets',
            'url': 'https://omeka.bsf-intranet.org/api/element_sets/3'
            },
        'html': False,
        'text': '220e33c22f41b07512c2dbc5fed610fa53bc8a0b94fc31bb49dcd77f4eb2dcc2'
    },
    {
        'element': {
            'id': 276,
            'name': 'Version',
            'resource': 'elements',
            'url': 'https://omeka.bsf-intranet.org/api/elements/276'
            },
        'element_set': {
            'id': 3,
            'name': 'Item Type Metadata',
            'resource': 'element_sets',
            'url': 'https://omeka.bsf-intranet.org/api/element_sets/3'
            },
        'html': False,
        'text': '2019-04-05'
    },
    {
        'element': {
            'id': 50,
            'name': 'Title',
            'resource': 'elements',
            'url': 'https://omeka.bsf-intranet.org/api/elements/50'
            },
        'element_set': {
            'id': 1,
            'name': 'Dublin Core',
            'resource': 'element_sets',
            'url': 'https://omeka.bsf-intranet.org/api/element_sets/1'
            },
        'html': False,
        'text': 'wiktionary.sw'
    },
    {
        'element': {
            'id': 41,
            'name': 'Description',
            'resource': 'elements',
            'url': 'https://omeka.bsf-intranet.org/api/elements/41'
            },
        'element_set': {
            'id': 1,
            'name': 'Dublin Core',
            'resource': 'element_sets',
            'url': 'https://omeka.bsf-intranet.org/api/element_sets/1'
            },
        'html': False,
        'text': 'Ufafanuzi kutoka Wiktionary, kamusi iliyo huru'
    },
    {
        'element': {
            'id': 44,
            'name': 'Language',
            'resource': 'elements',
            'url': 'https://omeka.bsf-intranet.org/api/elements/44'
            },
        'element_set': {
            'id': 1,
            'name': 'Dublin Core',
            'resource': 'element_sets',
            'url': 'https://omeka.bsf-intranet.org/api/element_sets/1'
            },
        'html': False,
        'text': 'mm'
    },
    {
        'element': {
            'id': 39,
            'name': 'Creator',
            'resource': 'elements',
            'url': 'https://omeka.bsf-intranet.org/api/elements/39'
            },
        'element_set': {
            'id': 1,
            'name': 'Dublin Core',
            'resource': 'element_sets',
            'url': 'https://omeka.bsf-intranet.org/api/element_sets/1'
            },
        'html': False,
        'text': 'DVB Tvnews'
    },
    {
        "html": False,
        "text": "IDB",
        "element_set": {
          "id": 3,
          "url": "https://omeka.bsf-intranet.org/api/element_sets/3",
          "name": "Item Type Metadata",
          "resource": "element_sets"
        },
        "element": {
          "id": 262,
          "url": "https://omeka.bsf-intranet.org/api/elements/262",
          "name": "Outil",
          "resource": "elements"
        }
    }
]

file_data = [{
    "id": 14177,
    "url": "https://omeka.bsf-intranet.org/api/files/14177",
    "file_urls": {
        "original": "https://omeka.bsf-intranet.org/files/original/sante_hindi.pdf",
        "fullsize": "https://omeka.bsf-intranet.org/files/fullsize/sante_hindi.jpg",
        "thumbnail": "https://omeka.bsf-intranet.org/files/thumbnails/sante_hindi.jpg",
        "square_thumbnail": "https://omeka.bsf-intranet.org/files/square_thumbnails/sante_hindi.jpg"
    },
    "added": "2017-06-14T09:17:51+00:00",
    "modified": "2019-03-15T22:44:41+00:00",
    "filename": "sante_hindi.pdf",
    "authentication": "1a82e562ce7bcd46d55ba755a6d1906d",
    "has_derivative_image": True,
    "mime_type": "application/pdf",
    "order": None,
    "original_filename": "sante_hindi.pdf",
    "size": 19925494,
    "stored": True,
    "type_os": "PDF document, version 1.6\n",
    "metadata": "sante_hindi.pdf",
    "item": {
        "id": 18812,
        "url": "https://omeka.bsf-intranet.org/api/items/18812",
        "resource": "items"
    },
    "element_texts": [],
    "extended_resources": []
}]

item_data = {
    'added': '2019-07-17T08:53:55+00:00',
    'collection': {
        'id': 34,
        'resource': 'collections',
        'url': 'https://omeka.bsf-intranet.org/api/collections/34'
        },
    'extended_resources': [],
    'featured': False,
    'files': {
        'count': 1,
        'resource': 'files',
        'url': 'https://omeka.bsf-intranet.org/api/files?item=44717'
        },
    'id': 44717,
    'item_type': {
        'id': 1,
        'name': 'Text',
        'resource': 'item_types',
        'url': 'https://omeka.bsf-intranet.org/api/item_types/1'
        },
    'modified': '2019-07-17T08:53:55+00:00',
    'owner': {
        'id': 4,
        'resource': 'users',
        'url': 'https://omeka.bsf-intranet.org/api/users/4'
        },
    'public': True,
    'tags': [
        {
            'id': 1840,
            'name': 'Books',
            'resource': 'tags',
            'url': 'https://omeka.bsf-intranet.org/api/tags/1840'
        },
        {
            'id': 564,
            'name': 'literature',
            'resource': 'tags',
            'url': 'https://omeka.bsf-intranet.org/api/tags/564'
        }],
    'url': 'https://omeka.bsf-intranet.org/api/items/44717'
}

collection_data = {
    "id": 34,
    "url": "https://omeka.bsf-intranet.org/api/collections/34",
    "public": True,
    "featured": False,
    "added": "2017-02-13T15:43:42+00:00",
    "modified": "2018-04-13T10:49:11+00:00",
    "owner": {
        "id": 1,
        "url": "https://omeka.bsf-intranet.org/api/users/1",
        "resource": "users"
    },
    "items": {
        "count": 4097,
        "url": "https://omeka.bsf-intranet.org/api/items?collection=34",
        "resource": "items"
    },
    "element_texts": [
        {
            "html": False,
            "text": "cultures",
            "element_set": {
                "id": 1,
                "url": "https://omeka.bsf-intranet.org/api/element_sets/1",
                "name": "Dublin Core",
                "resource": "element_sets"
            },
            "element": {
                "id": 50,
                "url": "https://omeka.bsf-intranet.org/api/elements/50",
                "name": "Title",
                "resource": "elements"
            }
        }
    ],
    "extended_resources": []
}

pkg_data = {
    "id": 10,
    "slug": "connaitre-et-aimer-son-corps-fr",
    "description": "Être bien dans son corps, ça participe à être bien dans sa tête. Et vous ? Comment voyez-vous votre corps ?",  # noqa: E501
    "language": "fr",
    "last_exportable_modification": "2018-05-08 15:19:08",
    "ideascube_name": "Relation au corps",
    "subject": "Culture",
    "relations": [
      {
        "item_id": 16954
      },
      {
        "item_id": 18191
      },
      {
        "item_id": 18847
      },
      {
        "item_id": 19291
      },
      {
        "item_id": 19292
      },
      {
        "item_id": 19972
      },
      {
        "item_id": 20805
      },
      {
        "item_id": 23838
      },
      {
        "item_id": 23841
      },
      {
        "item_id": 28646
      },
      {
        "item_id": 31228
      },
      {
        "item_id": 31231
      },
      {
        "item_id": 33825
      },
      {
        "item_id": 35792
      },
      {
        "item_id": 35793
      },
      {
        "item_id": 36027
      },
      {
        "item_id": 39001
      },
      {
        "item_id": 40107
      },
      {
        "item_id": 40118
      },
      {
        "item_id": 40119
      }
    ],
    "contents": [
      {
        "item_id": 14590
      },
      {
        "item_id": 15545
      },
      {
        "item_id": 15547
      },
      {
        "item_id": 15549
      },
      {
        "item_id": 16897
      },
      {
        "item_id": 16898
      },
      {
        "item_id": 16899
      },
      {
        "item_id": 16900
      },
      {
        "item_id": 16901
      },
      {
        "item_id": 16902
      }
    ],
    "extended_resources": []
}


class FakeRequestsSessionCollection(requests.Session):
    def get(self, *args, **kwargs):
        return FakeRequestsResponseCollection()


class FakeRequestsResponseCollection:
    @property
    def status_code(self):
        return 200

    def json(self):
        return collection_data


class FakeRequestsSessionPackage(requests.Session):
    def get(self, *args, **kwargs):
        return FakeRequestsResponsePackage()


class FakeRequestsResponsePackage:
    @property
    def status_code(self):
        return 200

    def json(self):
        return pkg_data


def test_get_metadata_by_id():
    assert OmekaApi._get_metadata_by_id(element_texts, 1) is None


def test_get_item_title():
    assert OmekaApi._get_item_title(element_texts) == 'wiktionary.sw'


def test_get_item_summary():
    assert OmekaApi._get_item_summary(element_texts) == 'Ufafanuzi kutoka Wiktionary, kamusi iliyo huru'


def test_get_item_language():
    assert OmekaApi._get_item_language(element_texts) == 'mm'


def test_get_item_credits():
    assert OmekaApi._get_item_credits(element_texts) == 'DVB Tvnews'


def test_get_item_device():
    assert OmekaApi._get_item_device(element_texts) == 'IDB'


def test_get_item_path():
    assert OmekaApi._get_item_path(file_data, 1) == "files/original/" + file_data[0]['filename']

    with pytest.raises(KeyError):
        new_data = deepcopy(file_data)
        new_data[0].pop('filename')
        OmekaApi._get_item_path(new_data, 1)

    assert OmekaApi._get_item_path([], 1) == ""


def test_get_item_preview():
    assert OmekaApi._get_item_preview(file_data) == 'files/fullsize/sante_hindi.jpg'
    assert OmekaApi._get_item_preview([{
        'file_urls': {
            'fullsize': None
        }
    }]) == ""

    new_data = deepcopy(file_data)
    new_data[0]['file_urls']['fullsize'] = None
    assert OmekaApi._get_item_preview(new_data) == ""


def test_get_item_collection():
    # requests.Session = mock.Mock()
    s = FakeRequestsSessionCollection()
    assert OmekaApi._get_item_collection(item_data, s) == "cultures"


def test_get_item_tags():
    assert OmekaApi._get_item_tags(item_data) == "Books,literature"


def test_get_package_by_id(mocker):
    import models.core.producer.producers.utils.omeka_api as omeka_api

    s = FakeRequestsSessionPackage()
    omeka_api.OmekaPackage = mock.Mock()
    OmekaApi.get_items = mock.Mock(return_value=True)

    OmekaApi.get_package_by_id(1337, s)

    assert OmekaApi.get_items.call_count == 1
    omeka_api.OmekaPackage.assert_called_once_with(
        1337,
        "connaitre-et-aimer-son-corps-fr",
        "Être bien dans son corps, ça participe à être bien dans sa tête. Et vous ? Comment voyez-vous votre corps ?",
        None,
        "fr",
        "2018-05-08 15:19:08",
        "Relation au corps",
        "Culture",
        True
    )
