from models.core.omeka_project import OmekaProject
import pytest


project_id = 25654
name = "test name"
title = "A title"
description = "A description"
language = "es"
device = "ideascube"
country = "France"


class TestAOmekaProjectModel:

    def test_omekaproject_properties(self):

        app = OmekaProject(
            project_id,
            name,
            title=title,
            language=language,
            description=description,
            device=device,
            country=country)

        assert app.id == project_id
        assert app.name == name
        assert app.title == title
        assert app.description == description
        assert app.language == language
        assert app.country == country
        assert app.device == device

    def test_approval_data_types_checkers(self):

        with pytest.raises(TypeError):
            project_id = "1"
            global name
            OmekaProject(
                project_id,
                name
                )

            project_id = 1
            name = list()
            OmekaProject(
                project_id,
                name
                )
            name = "test name"

    def test_omekaproject_setters(self):

        app = OmekaProject(
            project_id,
            name,
            title=title,
            language=language,
            description=description,
            device=device,
            country=country)

        app.title = "another title"
        app.description = "foo"
        app.language = "bar"
        app.device = "koombook"
        app.country = "en"

        assert app.title == "another title"
        assert app.description == "foo"
        assert app.language == "bar"
        assert app.device == "koombook"
        assert app.country == "en"

        with pytest.raises(AttributeError):
            app.name = "fail"
            app.project_id = 20500
            # make sure project_id and project name can't be changed once object is instanciated.

    def test_approval_serializer(self):
        app = OmekaProject(
            project_id,
            name,
            title=title,
            language=language,
            description=description,
            device=device,
            country=country)

        assert app.to_dict() == {
            "id": project_id,
            "name": name,
            "description": description,
            "language": language,
            "title": title,
            "country": country,
            "device": device}
