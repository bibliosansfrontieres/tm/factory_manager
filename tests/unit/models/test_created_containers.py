import pytest
from models.core.created_container import CreatedContainersModel
from datetime import datetime


class TestCreatedContainerModel:
    name = "name"
    container_id = "container_id"
    expire = datetime.now()
    process_id = "process_id"
    deleted = True

    def test_created_container_properties(self):

        app = CreatedContainersModel(
            self.name,
            self.container_id,
            self.expire,
            self.process_id,
            deleted=self.deleted
            )

        assert app.name == self.name
        assert app.container_id == self.container_id
        assert app.expire == self.expire
        assert app.process_id == self.process_id
        assert app.deleted == self.deleted

    def test_created_container_data_types_checkers(self):

        with pytest.raises(TypeError):
            CreatedContainersModel(
                name=1,  # should be string.
                container_id=self.container_id,
                expire=self.expire,
                process_id=self.process_id,
                deleted=self.deleted
                )

            CreatedContainersModel(
                name=self.name,
                container_id=1,  # should be string.
                expire=self.expire,
                process_id=self.process_id,
                deleted=self.deleted
                )

            CreatedContainersModel(
                name=self.name,
                container_id=self.container_id,
                expire="string",  # should be datetime.
                process_id=self.process_id,
                deleted=self.deleted
                )

            CreatedContainersModel(
                name=self.name,
                container_id=self.container_id,
                expire=self.expire,
                process_id=1,  # should be string.
                deleted=self.deleted
                )

            CreatedContainersModel(
                name=self.name,
                container_id=self.container_id,
                expire=self.expire,
                process_id=self.process_id,
                deleted="string"  # should be bool.
                )

    def test_created_container_setters(self):

        app = CreatedContainersModel(
            self.name,
            self.container_id,
            self.expire,
            self.process_id,
            deleted=self.deleted
            )

        app.deleted = True

        assert app.deleted is True

        with pytest.raises(AttributeError):  # non-setters
            app.container_id = "fail"
            app.name = "fail"
            app.expire = datetime.now()
            app.process_id = "fail"

    def test_created_container_serializer(self):

        app = CreatedContainersModel(
            self.name,
            self.container_id,
            self.expire,
            self.process_id,
            deleted=self.deleted
            )

        assert app.to_dict() == {
            "name": self.name,
            "container_id": self.container_id,
            "expire": self.expire,
            "process_id": self.process_id,
            "deleted": self.deleted
            }
