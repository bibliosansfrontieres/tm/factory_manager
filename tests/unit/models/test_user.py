from models.core.user import User

data = {
    "username": "foo",
    "password": "bar"
}


class TestUserModel:

    def test_user_properties(self):
        app = User(data['username'], data['password'])

        assert app.username == data["username"]
        assert app.password == data["password"]

    def test_user_serializer(self):
        app = User(data['username'], data['password'])

        assert app.to_dict() == {
            "username": data["username"],
            "password": str()
            }
