from models.core.log import Logs
from datetime import datetime


data = {
    "process_id": "azertyuiop",
    "user": "bob",
    "date": datetime.now(),
    "log": "any",
    "level": "ERROR"
}


class TestLogModel:

    def test_log_properties(self):
        app = Logs(data)

        assert app.process_id == data["process_id"]
        assert app.user == data["user"]
        assert app.log == data["log"]
        assert app.level == data["level"]
        assert app.date == data["date"]

    def test_default_values(self):
        data = dict()

        app = Logs(data)
        assert app.process_id == str()
        assert app.user == str()
        assert app.log == "No log content"
        assert app.level == "ERROR"
        assert isinstance(app.date, datetime)

    def test_log_serializer(self):
        app = Logs(data)
        assert app.to_dict() == {
            "process_id": data["process_id"],
            "user": data["user"],
            "log": data["log"],
            "level": data["level"],
            "date": str(data["date"])
            }

    def test_error_level(self):
        app = Logs.error_lvl(data['process_id'], data['level'])
        assert app == {
            "process_id": data['process_id'],
            "error_lvl": data['level']
        }
