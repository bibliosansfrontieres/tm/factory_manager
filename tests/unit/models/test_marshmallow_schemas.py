
from models.core.marshmallow_schemas import ContentSchema, ApplicationsSchema

content_data = {
    "content_id": "content_id",
    "name": "name",
    "description": "description",
    "version": "version",
    "endpoint": {
        "url": "url",
        "container": "container",
        "name": "name"
    },
    "download_path": "download_path",
    "destination_path": "destination_path"
}

container_data = {
    'image': 'image',
    'expose': 1,
    'name': 'name'
}

application_data = {
    'name': 'name',
    'description': 'description',
    'version': 'version',
    'picture': 'picture',
    'containers': [
        container_data
    ],
    'bundle': 'bundle',
    'contents': [
        content_data
    ]
}


def test_ContentSchema_default_values():
    # when
    result = ContentSchema().load(content_data)

    # then
    assert result.size == '0'
    assert result.language == 'en'
    assert result.subject is None


def test_ApplicationsSchema_default_values():
    # when
    result = ApplicationsSchema().load(application_data)

    # then
    assert result.search is None
    assert result.authentication is None


def test_ApplicationsSchema_serialization():
    # when
    result = ApplicationsSchema().dump(ApplicationsSchema().load(application_data))

    # then
    assert not any([x for x in ApplicationsSchema().dump(result).keys() if x == 'search'])  # key not present.
    assert not any([x for x in ApplicationsSchema().dump(result).keys() if x == 'authentication'])
    assert result == {
        'picture': 'picture',
        'bundle': 'bundle',
        'contents': [{
            'size': '0',
            'destination_path': 'destination_path',
            'download_path': 'download_path',
            'content_id': 'content_id',
            'language': 'en',
            'subject': None,
            'name': 'name',
            'version': 'version',
            'description': 'description',
            'endpoint': {'container': 'container', 'name': 'name', 'url': 'url'}}],
        'name': 'name',
        'version': 'version',
        'description': 'description',
        'containers': [{'expose': 1, 'image': 'image', 'name': 'name'}]
    }
