from models.core.approval import Approval
import pytest


process_id = "azertyuiop"
timestamp = "2019-06-04T13:03:10Z"
approved = True
description = "any"
parent_process_id = "poiuytreza"


class TestApprovalModel:

    def test_approval_properties(self):

        app = Approval(process_id, timestamp=timestamp, approved=approved, description=description,
                       parent_process_id=parent_process_id)
        assert app.process_id == process_id
        assert app.timestamp == timestamp
        assert app.approved == approved
        assert app.description == description
        assert app.parent_process_id == parent_process_id

    def test_approval_data_types_checkers(self):

        process_id = None
        with pytest.raises(TypeError):
            global description
            global parent_process_id
            Approval(process_id, approved=approved, description=description,
                     parent_process_id=parent_process_id)

    def test_approval_setters(self):

        app = Approval(process_id, approved=approved, description=description,
                       parent_process_id=parent_process_id)

        app.approved = False
        app.parent_process_id = "foo"

        assert app.approved is False
        assert app.parent_process_id == "foo"

        with pytest.raises(AttributeError):
            app.process_id = "fail"  # make sure process_id can't be changed once instanciated.

    def test_approval_serializer(self):
        app = Approval(process_id, approved=approved, description=description,
                       parent_process_id=parent_process_id)

        assert app.to_dict() == {"process_id": process_id,
                                 "approved": approved,
                                 "description": description,
                                 "parent_process_id": parent_process_id,
                                 "timestamp": app.timestamp
                                 }
