import pytest
from models.core.recipe import Recipe

recipe_id = 1
name = "name"
factories = ["FactoryPreparation"]


class TestRecipeModel:

    def test_recipe_properties(self):

        app = Recipe(recipe_id, name=name, factories=factories)

        assert app.id == recipe_id
        assert app.name == name
        assert app.factories == factories

    def test_recipe_data_types_checkers(self):

        with pytest.raises(TypeError):
            recipe_id = "1"
            global factories
            Recipe(recipe_id, name=name, factories=factories)

            recipe_id = 1
            factories = [1, 2, 3]
            Recipe(recipe_id, name=name, factories=factories)
            factories = ["FactoryPreparation"]  # set value back

    def test_recipe_serializer(self):

        app = Recipe(recipe_id, name=name, factories=factories)

        assert app.to_dict() == {
            "id": recipe_id,
            "name": name,
            "factories": factories
            }
