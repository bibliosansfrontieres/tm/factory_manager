from models.core.slack_notification import Slack
from tests.unit.build_message import Msg
from settings import SLACK_NOTIF
import requests

msg = Msg().build_generic_message()


def test_forge_msg_output():
    # given
    app = Slack(msg)

    # when
    app.forge_msg()

    # then
    assert app.payload['channel'] == SLACK_NOTIF["channel"]
    assert app.payload['username'] == SLACK_NOTIF["username"]
    assert app.payload['icon_emoji'] == SLACK_NOTIF["icon_emoji"]
    assert "@" + msg.process.user.username in app.payload["text"]
    assert msg.process.project.name in app.payload["text"]
    assert msg.process.recipe.name in app.payload["text"]


def test_sending_message(mocker):
    """Message should be sent only in PROD env
    """
    # given
    cur = Slack(msg)
    cur.forge_msg()

    # then
    mocker.patch('requests.post')
    cur.ENVIRON = "PROD"
    cur.send()
    assert requests.post.call_count == 1

    cur.ENVIRON = "DEV"
    cur.send()
    assert requests.post.call_count == 1
