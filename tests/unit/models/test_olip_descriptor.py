
from models.core.olip_descriptor import (
    Descriptor,
    Application,
    Container,
    Content,
    Endpoint,
    Authentication)

import pytest


def test_Descriptor_types():
    with pytest.raises(TypeError):
        Descriptor(applications=list(), terms=str())

    with pytest.raises(TypeError):
        Descriptor(applications=str(), terms=list())


def test_Descriptor_comparison():
    assert Descriptor(list(), list()) == Descriptor(list(), list())


def test_Application_types():
    with pytest.raises(TypeError):
        Application(containers=list(), contents=str())

    with pytest.raises(TypeError):
        Application(containers=str(), contents=list())


def test_Application_default_values():
    # when
    result = Application(
        name='name',
        description='description',
        version='version',
        picture='picture',
        bundle='bundle',
        containers=list()
    )

    #  then
    assert result.authentication is None
    assert result.search is None
    assert result.contents == list()


def test_Application_comparison():
    assert Application(
        name='name',
        description='description',
        version='version',
        picture='picture',
        bundle='bundle',
        containers=list()
    ) == Application(
        name='name',
        description='description',
        version='version',
        picture='picture',
        bundle='bundle',
        containers=list()
    )


def test_Container_types():
    with pytest.raises(TypeError):
        Container(
            image=str(),
            name=str(),
            expose="a string"
        )


def test_Container_comparison():
    assert Container(
            image=str(),
            name=str(),
            expose=1
        ) == Container(
            image=str(),
            name=str(),
            expose=1
        )


def test_Content_types():
    with pytest.raises(TypeError):
        Content(
            content_id=str(),
            name=str(),
            language=str(),
            subject=str(),
            description=str(),
            version=str(),
            size=str(),
            endpoint=str(),
            download_path=str(),
            destination_path=str(),
        )


def test_Content_comparison():
    # given
    endpoint = Endpoint(
        url='url',
        container='container',
        name='name)'
    )

    assert Content(
        content_id=str(),
        name=str(),
        language=str(),
        subject=str(),
        description=str(),
        version=str(),
        size=str(),
        endpoint=endpoint,
        download_path=str(),
        destination_path=str(),
    ) == Content(
        content_id=str(),
        name=str(),
        language=str(),
        subject=str(),
        description=str(),
        version=str(),
        size=str(),
        endpoint=endpoint,
        download_path=str(),
        destination_path=str(),
    )


def test_Authentication_default_values():
    # given
    result = Authentication()

    # then
    assert result.grant_types is None
    assert result.response_types is None
    assert result.token_endpoint_auth_method is None


def test_Authentication_comparison():
    assert Authentication(
            grant_types='grant_types',
            response_types='response_types',
            token_endpoint_auth_method='token_endpoint_auth_method'
        ) == Authentication(
            grant_types='grant_types',
            response_types='response_types',
            token_endpoint_auth_method='token_endpoint_auth_method'
        )
