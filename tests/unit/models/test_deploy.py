from models.core.deploy_config import Deploy
from models.core.cap_config import CapConfig
import pytest


process_id = "xxx"
devices = [{
    "mac_address": "mac1",
    "name": "name1",
    "project_name": "test_project1"
 }, {
     "mac_address": "mac2",
     "name": "name2",
     "project_name": "test_project2"
 }]

cap_config = CapConfig(
    "yyy",
    project_name="kb-bsf-hello",
    timezone="Europe/Paris"
    )
parent_process_id = "zzz"


class TestDeployModel:

    def test_deploy_properties(self):

        app = Deploy(
            process_id,
            devices,
            cap_config,
            parent_process_id=parent_process_id
        )

        assert app.process_id == process_id
        for device in app.devices.devices:
            assert device.to_dict() in devices
        assert app.cap_config == cap_config
        assert app.parent_process_id == parent_process_id

    def test_deploy_data_types_checkers(self):

        with pytest.raises(TypeError):
            process_id = None
            global devices
            global cap_config
            Deploy(
                process_id,
                devices,
                cap_config,
                parent_process_id=parent_process_id
            )

            process_id = "xxx"
            devices = str()
            Deploy(
                process_id,
                devices,
                cap_config,
                parent_process_id=parent_process_id
            )

            devices = ["MAC1", "MAC2"]
            cap_config = dict()
            Deploy(
                process_id,
                devices,
                cap_config,
                parent_process_id=parent_process_id
            )

            cap_config = CapConfig(
                "yyy",
                project_name="kb-bsf-hello",
                timezone="Europe/Paris"
                )

    def test_deploy_setters(self):

        app = Deploy(
            process_id,
            devices,
            cap_config,
            parent_process_id=parent_process_id
        )

        new_cap_conf = CapConfig(
                            "foobar",
                            project_name="kb-bsf-other",
                            timezone="Europe/London"
                            )

        app.cap_config = new_cap_conf
        app.parent_process_id = "foo"

        assert app.cap_config == new_cap_conf
        assert app.parent_process_id == "foo"

        with pytest.raises(AttributeError):
            app.process_id = "fail"
            app.devices = ["MAC3", "MAC4"]
            # make sure these properties can't be changed once instanciated.

    def test_deploy_serializer(self):
        app = Deploy(
            process_id,
            devices,
            cap_config,
            parent_process_id=parent_process_id
        )

        assert app.to_dict() == {"process_id": process_id,
                                 "devices": devices,
                                 "cap_config": {
                                     "process_id": "yyy",
                                     "project_name": "kb-bsf-hello",
                                     "timezone": "Europe/Paris",
                                     "containers": None
                                     },
                                 "parent_process_id": parent_process_id}
