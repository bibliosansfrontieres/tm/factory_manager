from repository.dhcp.DHCPHelpers import DHCPHelpers
from services.deploy.deploy import DeployService


lease = [
            {
              "arguments": {
                "leases": [
                  {
                    "cltt": 1556805878,
                    "fqdn-fwd": False,
                    "fqdn-rev": False,
                    "hostname": "hostname",
                    "hw-address": "AA:BB:CC:DD:EE:DD",
                    "ip-address": "127.0.0.1",
                    "state": 0,
                    "subnet-id": 1,
                    "valid-lft": 3600
                  }
                ]
              }
            }
        ]


def test_add_project_name(mocker):
    """
    Checks if DHCPHelpers.get_device_deployment_status is called with correct argument,
    and if returned dictionary contains provided project name.
    """
    mocker.patch.object(DHCPHelpers, 'get_device_deployment_status', return_value="kb-bsf-test")
    cur = DHCPHelpers()
    result_lease = cur.add_project_name(lease)

    DHCPHelpers.get_device_deployment_status.assert_called_once_with(
        lease[0]['arguments']['leases'][0]['hw-address']
        )
    assert result_lease[0]['arguments']['leases'][0]['project_name'] == "kb-bsf-test"


def test_get_device_deployment_status(mocker):

    # With empty response
    mocker.patch.object(DeployService, 'get_device_job', return_value=({}, 200))
    cur = DHCPHelpers()

    result = cur.get_device_deployment_status(lease[0]['arguments']['leases'][0]['hw-address'])
    assert result is None

    # With non-empty response
    job = ({'project': {'name': 'foo'}}, 200)
    mocker.patch.object(DeployService, 'get_device_job', return_value=job)
    cur = DHCPHelpers()

    result = cur.get_device_deployment_status(lease[0]['arguments']['leases'][0]['hw-address'])
    assert result == job[0]['project']['name']
