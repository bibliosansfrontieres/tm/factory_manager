from repository.process.MongoProcessHandler import MongoProcessHandler
from tests.unit.build_message import Msg


class FakeMongoDBConnection(MongoProcessHandler):
    def __init__(self):
        self.data = {
            "deployment": {
                "devices": [
                    {
                            "id": 2,
                            "name": "AA:BB:CC:DD:EE:FF",
                            "mac_address": "AA:BB:CC:DD:EE:FF"
                    }
                ],
                "process_id": "process_id",
                "parent_process_id": "process_id"
                }
        }


def test_formatProcess():

    cur = FakeMongoDBConnection()
    msg = Msg().build_generic_message()
    result = cur.formatProcess(msg)

    assert result == msg.process.to_dict()


def test_formatID():

    cur = FakeMongoDBConnection()
    process_id = "AnID"
    result = cur.formatID(process_id)

    assert result == {
        "id": process_id
    }


def test_formatApprove():

    cur = FakeMongoDBConnection()
    msg = Msg().build_generic_message()
    result = cur.formatApprove(msg)

    assert result == {
        "approval": msg.process.approval.to_dict()
    }


def test_formatDevices():

    cur = FakeMongoDBConnection()

    result = cur.formatDevices(cur.data)

    assert result == {
       'deployment.devices': cur.data['deployment']['devices']
    }


def test_formatParentProcess():

    cur = FakeMongoDBConnection()
    result = cur.formatParentProcess(cur.data)

    assert result == {
       'deployment.parent_process_id': cur.data['deployment']['parent_process_id']
    }


def test_formatDeployProcessID():

    cur = FakeMongoDBConnection()
    result = cur.formatDeployProcessID(cur.data)

    assert result == {
       'deployment.process_id': cur.data['deployment']['process_id']
    }


def test_formatRecipeID():

    result = FakeMongoDBConnection.formatRecipeID(1)

    assert result == {
        'recipe.id': {"$in": 1}
    }
