from repository.message.BuildCAPConfDAO import BuildCAPConfDAO
from tests.unit.build_message import Msg
from models.core.cap_config import CapConfig


def test_make_config():
    msg = Msg().build_generic_message()
    capconf = BuildCAPConfDAO.make_config(msg)

    assert isinstance(capconf, CapConfig)
    assert capconf.process_id == Msg().process_data['process']['id']
    assert capconf.project_name == Msg().process_data['process']['project']['name']
    assert capconf.timezone == "Europe/Paris"
    for cont in capconf.containers:
        assert isinstance(cont, dict)
        assert cont['name'] in ['ideascube', 'kiwix', 'kolibri']
