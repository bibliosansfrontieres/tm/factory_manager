from repository.login.LDAP_DAO import LDAPLoginDAO
import ldap


LDAP_INFO = {
    "LDAP_HOST": "localhost",
    "USER_DN": "USER_DN",
    "BASE_DN": "BASE_DN"
    }
username = "foo"
password = "bar"


def test_init_connection(mocker):
    mocker.patch('ldap.initialize')

    cur = LDAPLoginDAO()
    cur.LDAP_INFO = LDAP_INFO
    cur.init_connection()

    ldap.initialize.assert_called_once_with(LDAP_INFO['LDAP_HOST'])


def test_user_auth(mocker):
    mocker.patch('ldap.initialize')

    cur = LDAPLoginDAO()
    cur.LDAP_INFO = LDAP_INFO
    connection = cur.init_connection()

    cur.auth_user(username, password)

    connection.bind_s.assert_called_once_with(cur.user_dn, password)
