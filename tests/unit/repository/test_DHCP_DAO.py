from repository.dhcp.DhcpDAO import dhcpDAO
import requests


mac_address = "AA:BB:CC:DD:EE:DD"
KEA_API = "http://dhcp_api:1234"


def test_get_lease(mocker):
    mocker.patch('requests.post')

    cur = dhcpDAO()
    cur.KEA_API = KEA_API
    cur.get_lease(mac_address)

    params = {
        "command": "lease4-get",
        "arguments": {"identifier-type": "hw-address",
                      "identifier": mac_address,
                      "subnet-id": 1},
        "service": ["dhcp4"]
        }

    requests.post.assert_called_once_with(KEA_API, json=params)


def test_get_all_leases(mocker):
    mocker.patch('requests.post')

    cur = dhcpDAO()
    cur.KEA_API = KEA_API
    cur.get_all_leases()

    params = {
        "command": "lease4-get-all",
        "service": ["dhcp4"]
        }

    requests.post.assert_called_once_with(KEA_API, json=params)
