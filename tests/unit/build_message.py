from models.core.message import Message


class Msg:
    def __init__(self):
        self.process_data = {
            'process': {
                'id': '79487c238117479abad37f32021bef6d',
                'description': None,
                'timestamp': None,
                'image_url': '',
                'work_dir': '/tmp',
                'catalog_path': '/tmp/catalog.yml',
                'user': {
                    'username': 'empty',
                    'password': ''
                },
                'status': 'success',
                'recipe': {
                    'id': 1,
                    'name': 'Prepare',
                    'factories': ['FactoryPreparation']
                },
                'project': {
                    'id': 35909,
                    'name': 'kb-bsf-hello',
                    'title': 'no title',
                    'description': 'no description',
                    'language': 'fr',
                    'device': 'koombook',
                    'country': 'France'
                },
                'media_center_configuration': {
                    'engine': 'ideascube',
                    'template': 'ideascube',
                    'languages': ['fr'],
                    'expire': '2019-08-12 09:22:26.769080'
                },
                'approval': {
                    'process_id': '',
                    'timestamp': '2019-06-04T08:50:14Z',
                    'approved': None,
                    'description': '',
                    'parent_process_id': ''
                },
                'deployment': {
                    'process_id': '',
                    'devices': [],
                    'cap_config': {
                        'process_id': '',
                        'project_name': '',
                        'timezone': '',
                        'containers': []
                    },
                    'parent_process_id': ''
                }
            }
        }

    def add_omeka_data(self, msg):
        omeka_packages_data = [
            {
                'id': 459,
                'slug': 'test-pkg-1-ar',
                'description': 'Test pkg 1',
                'goal': None,
                'language': 'ar',
                'last_exportable_modification': '2019-03-05 11:38:43',
                'ideascube_name': 'Test pkg 1',
                'items': [
                    {
                        'id': 35906,
                        'title': 'test pdf',
                        'summary': 'test pdf',
                        'lang': 'fr',
                        'credits': 'Enabel',
                        'collection': 'creativity',
                        'path': 'files/original/be8382ce65fc0893488ff8295bd3fd8a.pdf',
                        'tags': '',
                        'preview': '',
                        'filename': 'be8382ce65fc0893488ff8295bd3fd8a.pdf',
                        'file_url': 'http://somewhere/media.pdf',
                        'thumbnail_url': 'http://somewhere/thumb.jpg',
                        'mime_type': 'text/pdf'
                    },
                    {
                        'id': 35908,
                        'title': 'test audio',
                        'summary': 'test audio',
                        'lang': 'ar',
                        'credits': 'creator',
                        'collection': 'information',
                        'path': 'files/original/3801be211c613f0bc2c0d9bb0c3e9e87.mp3',
                        'tags': '',
                        'preview': '',
                        'filename': '3801be211c613f0bc2c0d9bb0c3e9e87.mp3',
                        'file_url': 'http://somewhere/media.mp3',
                        'thumbnail_url': 'http://somewhere/thumb.jpg',
                        'mime_type': 'audio/mp3'
                    },
                    {
                        'id': 35911,
                        'title': 'video ex',
                        'summary': '-',
                        'lang': 'fr',
                        'credits': '-',
                        'collection': 'technical and vocational training',
                        'path': 'files/original/7e2674ba317ba1af1efdc5c3ed3e554b.mp4',
                        'tags': '',
                        'preview': '',
                        'filename': '7e2674ba317ba1af1efdc5c3ed3e554b.mp4',
                        'file_url': 'http://somewhere/media.mp4',
                        'thumbnail_url': 'http://somewhere/thumb.jpg',
                        'mime_type': 'video/mp4'
                    }
                ]
            }
        ]
        msg.set_value("omeka_packages", omeka_packages_data)
        msg.set_value("relation_items", list())
        return msg

    def build_generic_message(self):
        return self.add_omeka_data(Message(self.process_data))


class Logger:
    def __init__(self):
        pass

    def debug(self, msg):
        pass

    def info(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        pass
