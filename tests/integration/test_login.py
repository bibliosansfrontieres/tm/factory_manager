import requests


class TestLogin:

    def test_login(self, token):
        assert token is not None
        assert len(token) > 0

    def test_session_good_token(self, token):
        headers = {"X-Auth-Token": token}
        r = requests.get('http://localhost:5000/temps-modernes/api/login/session/' + token, headers=headers)

        assert r.status_code == 200

    def test_session_bad_token(self, token):
        headers = {"X-Auth-Token": token}
        r = requests.get('http://localhost:5000/temps-modernes/api/login/session/' + "bad_token", headers=headers)

        assert r.status_code == 403

    def test_session_bad_auth(self, token):
        headers = {"X-Auth-Token": "bad_token"}
        r = requests.get('http://localhost:5000/temps-modernes/api/login/session/' + token, headers=headers)

        assert r.status_code == 403

    def test_session_missing_auth(self, token):
        r = requests.get('http://localhost:5000/temps-modernes/api/login/session/' + token)

        assert r.status_code == 403
