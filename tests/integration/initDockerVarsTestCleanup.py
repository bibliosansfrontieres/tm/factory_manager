from models.core.AbstractDockerVars import AbstractDockerVars


class InitDockerVarsTestCleanup(AbstractDockerVars):
    def __init__(self, msg):
        self._msg = msg

    @property
    def image(self):
        return "busybox:latest"

    @property
    def name(self):
        return "please_destroy_me"

    @property
    def command(self):
        return ["sh", "-c", "sleep 3600"]

    @property
    def network(self):
        return "docker_default"
