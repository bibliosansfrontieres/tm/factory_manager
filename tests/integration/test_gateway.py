from models.core.process_status import ProcessStatus
from repository.docker.DockerDAO import DockerDAO
from settings import DOCKER_VARS
from pathlib import Path
from time import sleep
import pytest
import requests
import os


@pytest.fixture(scope="class")
def propagate_visualize_pid(request, token):
    """
    We will reuse this specific process pid in other tests. So we are propagating its results.
    """
    r = requests.post(
        'http://localhost:5000/temps-modernes/api/gateway/visualize',
        json={
            "media_center_configuration": {
                "engine": "ideascube",
                "languages": ["fr"],
                "expire": "2019-08-12 09:22:26.769080"
                },
            "project": {
                "name": "idc-bsf-qas",
                "id": 25634,
                "description": "__description"
                }
            },
        headers={"X-Auth-Token": token}
    )
    request.cls.r = r
    yield


@pytest.mark.usefixtures('propagate_visualize_pid')
class TestGateway:
    """
    Checks Factory Preparation/Configure/Build/Visualize/Approve/Finish
    TODO: Deploy/Update
    """

    def wait_and_check_status(self, pid, timeout=30):
        """
        Checks that process ends up successfully in the specified timeout.

        :param str pid: process_id to poll
        :param timeout: timeout in sec before asserting False, and thus considering the test as failed.
        :type timeout: integer
        :return: None or message.process if non empty.
        :raises AssertionError: if process status is not `ProcessStatus.success` in less then _timeout_ seconds.
        :raises AssertionError: if process status is `ProcessStatus.failure`.
        """
        timer = 0
        while True:
            r = requests.get(
                'http://localhost:5000/temps-modernes/api/process/?id={}'.format(pid)
            )

            if r.json()['status'] == ProcessStatus.failure:
                assert False
            elif r.json()['status'] == ProcessStatus.success:
                assert True
                break
            else:  # Probably still running, let's wait a bit and try again
                sleep(3)
                timer += 3

            if timer > timeout:
                print('Timeout while building process_id {}'.format(pid))
                assert False

        return None if r.json() == dict() else r.json()

    def check_visualize_avaibility(self, url, timeout=300):
        """
        Checks that visualization process ends up successfully in the specified timeout.
        Timeout is set at 300s, by default.

        :param str url: the URL of the image to poll.
        :return: nothing.
        :raises AssertionError: if requests.status_code is 502 for more than timeout.
        :raises AssertionError: if requests.status_code is different 302 and 502.
        """
        timer = 0
        while True:
            r = requests.head(url)

            if r.status_code == 302:  # return code when successful.
                assert True
                break
            elif r.status_code != 502:  # This is the status_code we get while image is building.
                assert False
            else:
                sleep(3)
                timer += 3

            if timer > timeout:
                print("Timeout while building visualization ({} sec.).".format(timeout))
                assert False

    def check_backup(self):
        """
        Checks if backup really happened.
        """
        backup_path = DOCKER_VARS['CONTAINER_ROOT_DIR'] + "/" + \
            self.r.json()['project']['name'] + "/" + \
            "ideascube/" + \
            self.r.json()['id'] + "/"

        idc_path = backup_path + "var_cache_idc.tar"
        cache_path = backup_path + "var_idc.tar"

        assert os.path.exists(idc_path)
        assert os.path.exists(cache_path)
        assert os.path.getsize(idc_path) > 0
        assert os.path.getsize(cache_path) > 0

    def check_ProducerRemoveContainerData(self, pid):
        """
        Checks if containers data really had been removed
        """

        assert not Path(DOCKER_VARS['LONGTERM_VOLUME_STORAGE'] + pid).exists()

    def test_visualize(self, token):
        """
        Actual request is done by the propagate_visualize_pid fixture.
        """

        pid = self.r.json()['id']  # Make a object variable so we will continue using this process_id.
        assert self.r.status_code == 200
        assert len(pid) > 0

        print("failed process_id in visualize: " + pid)  # For logs only on failure.

        image_url = self.wait_and_check_status(pid, timeout=120)['image_url']
        self.check_visualize_avaibility(image_url, timeout=600)

        # Didn't find a better way to push information back to Jenkins.
        with open(os.environ['VIZ_FILE'], 'w') as f:
            f.write(image_url)

    def test_approved_true(self, token):
        r = requests.post(
            'http://localhost:5000/temps-modernes/api/gateway/approve',
            json={
                "approval": {
                    "approved": True,
                    "process_id": self.r.json()['id'],
                    "description": "approval description"
                    }
            },
            headers={"X-Auth-Token": token}
        )

        assert r.status_code == 200
        assert len(r.json()['id']) > 0

        print("failed process_id in approval: " + r.json()['id'])  # For logs only on failure.

        self.wait_and_check_status(r.json()['id'], timeout=60)
        # self.check_backup()
        self.check_ProducerRemoveContainerData(r.json()['id'])

        # Check container has been removed.
        cur = DockerDAO(logger={})
        assert cur.find_container_by_name(r.json()['id']) == list()

    # TODO: Will need an API endpoint to query Docker/Mongo
    # def test_cleanup(self, token):
    #    """
    #    Runs a dummy container, and check if it gets deleted.
    #    """
    #    msg = Msg().build_generic_message()
    #    cur = DockerDAO(logger={})
    #    cur.run(initDockerVarsTestCleanup(msg))
#
    #    r = requests.post(
    #        'http://localhost:5000/temps-modernes/api/gateway/cleanup',
    #        json={},
    #        headers={"X-Auth-Token": token}
    #    )
#
    #    assert r.status_code == 200
    #    assert len(r.json()['id']) > 0
#
    #    # Our created container is marked as deleted: False in DB.
    #    res = MongoIO(container_collection, host="localhost")
    #    assert res.read_latest({"process_id": "79487c238117479abad37f32021bef6d"})['deleted'] is False
    #    res.close()
