import pytest
import requests


def pytest_addoption(parser):
    parser.addoption(
        "--user", action="store", default="bob", help="Provide LDAP username"
    )
    parser.addoption(
        "--pass", action="store", default="bob", help="Provide LDAP password"
    )


@pytest.fixture(scope="session")
def arg_user(request):
    return request.config.getoption("--user")


@pytest.fixture(scope="session")
def arg_pass(request):
    return request.config.getoption("--pass")


@pytest.fixture(scope="session")
def token(arg_user, arg_pass):
    r = requests.post(
        "http://localhost:5000/temps-modernes/api/login/",
        json={
            "username": arg_user,
            "password": arg_pass
        })
    if r.status_code != 200:
        return None
    else:
        return r.json()['token']
