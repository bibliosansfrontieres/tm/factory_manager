import requests


def test_missing_process_id():
    r = requests.head('http://localhost:5000/temps-modernes/api/process/')
    assert r.status_code == 200


def test_bad_process_id():
    r = requests.get('http://localhost:5000/temps-modernes/api/process/?id=1')
    assert r.json() == dict()


def test_good_process_id(token):
    """
    - First start some real process to fill db with some data.
    - Check validity of this process
    """
    r = requests.post(
            'http://localhost:5000/temps-modernes/api/gateway/prepare',
            json={
                "project": {
                    "name": "idc-bsf-qas",
                    "id": 25634,
                    "description": "__description"
                    }
            },
            headers={"X-Auth-Token": token}
        )

    rpid = requests.get('http://localhost:5000/temps-modernes/api/process/?id={}'.format(r.json()["id"]))
    assert rpid.json()['id'] == r.json()["id"]


def test_recipe_return_valid_process():
    r = requests.get('http://localhost:5000/temps-modernes/api/process/recipe/?id=1')
    for p in r.json():
        assert p['recipe']['id'] == 1


def test_multiple_recipe_return_valid_process():
    r = requests.get('http://localhost:5000/temps-modernes/api/process/recipe/?id=1&id=4')
    for p in r.json():
        assert p['recipe']['id'] == 1 or p['recipe']['id'] == 4


def test_recipe_bad_id_return_empty_list():
    r = requests.get('http://localhost:5000/temps-modernes/api/process/recipe/?id=666')
    assert r.json() == list()
