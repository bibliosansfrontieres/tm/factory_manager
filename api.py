#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from flask import Flask, Blueprint  # noqa: E402
"""ECS: Ajoute CORS au Temps modernes
   Date: 15/11/2018
"""
from flask_cors import CORS  # noqa: E402
from main.api import api  # noqa: E402

API_BLUEPRINT_PREFIX = "/temps-modernes/api"
FLASK_APP_HOST = "0.0.0.0"
FLASK_APP_PORT = 5000


app = Flask(__name__)
"""ECS: Ajoute all origins (*)
   Date: 15/11/2018
"""
cors = CORS(app, resources={r"/temps-modernes/api/*": {"origins": "*"}})


def initialize_app(flask_app):

    blueprint = Blueprint('api', __name__, url_prefix=API_BLUEPRINT_PREFIX)
    api.init_app(blueprint)
    flask_app.register_blueprint(blueprint)


def main():
    initialize_app(app)
    app.run(host=FLASK_APP_HOST, port=FLASK_APP_PORT, debug=False)


if __name__ == '__main__':

    try:
        main()
    except KeyboardInterrupt:
        exit(0)
