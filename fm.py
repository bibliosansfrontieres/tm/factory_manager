#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import argparse
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from models.core.factory.factory_manager import FactoryManager  # noqa: E402

from settings import RABBITMQ_CONNECTION_HOST, RABBITMQ_CONNECTION_PORT, RABBITMQ_CONNECTION_VHOST,\
    RABBITMQ_CONNECTION_USERNAME, RABBITMQ_CONNECTION_PASSWORD, RECIPES_PATH  # noqa: E402

FACTORIES_MODULES_PATH = "models.core.factory.factories"


def main():
    factory_manager = FactoryManager(RABBITMQ_CONNECTION_HOST, RABBITMQ_CONNECTION_PORT,
                                     RABBITMQ_CONNECTION_VHOST, RABBITMQ_CONNECTION_USERNAME,
                                     RABBITMQ_CONNECTION_PASSWORD, FACTORIES_MODULES_PATH, RECIPES_PATH)

    parser = argparse.ArgumentParser(description='Start factory_manager based on a recipe id',
                                     epilog='Example : python3 factory_manager/app.py 3')
    parser.add_argument('recipe_id', type=int, help='Recipe ID, check data/recipes_db.json for full list.')
    """
    These are not usable for now, and probably will never be useful.
    group.add_argument('-a', '--add', type=str, help='Add an instance of Factory,
                       waiting for a factory name here (as listed in recipes).')
    group.add_argument('-l', '--list', store_action=int, help='List already started factories.')
    """
    args = vars(parser.parse_args())

    factory_manager.load_recipe(args['recipe_id'])

    print("[*] To exit press CTRL+C or type 'help' for more choices.")
    for line in sys.stdin:
        split = line.split()

        if split:

            if split[0] == "help":
                print(
                    "- 'add' [factory_name, ...] : Create an instance of a factory by name.\n"
                    "- 'lr' OR 'load-recipe' [recipe_id] : Launch factories listed in the specified recipe by id.\n"
                    "- 'list' : List factories already launched.\n"
                    "- 'quit' : Stops the program."
                )

            elif split[0] == "quit" or split[0] == "q":
                return

            elif split[0] == "add":
                if len(split) == 1:
                    print("Unable to execute command add, arguments are missing.")
                else:
                    for elem in split[1:]:
                        factory_manager.add_factory(elem)

            elif split[0] == "lr" or split[0] == "load-recipe":
                if len(split) != 2:
                    print("Unable to execute command load-recipe, invalid amount of arguments.")
                else:
                    factory_manager.load_recipe(int(split[1]))

            elif split[0] == "list":
                factory_manager.list()

            else:
                print("[x] No recognizable command.")


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass

    print("[*] Exiting ...")
