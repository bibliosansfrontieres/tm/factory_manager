# -*- coding: utf-8 -*-

from repository.recipe.FileRecipesDAO import FileRecipesDAO
from models.core.factory.abstract_factory import AbstractFactory
from main.factory_manager import utils

APP_PREFIX = "tmp."
FINISH_EXCHANGE = APP_PREFIX + "finish"
LOGGER_QUEUE = APP_PREFIX + "log"


class FactoryManager:

    def __init__(self, host, port, vhost, username, password, modules_path, recipes_path,
                 finish_exchange=FINISH_EXCHANGE,
                 log_queue=LOGGER_QUEUE):

        self._factories_list = []

        self._finish_exchange = finish_exchange
        self._log_queue = log_queue

        # connection params
        self._host = host
        self._port = port
        self._vhost = vhost
        self._username = username
        self._password = password

        self._modules_path = modules_path
        self._recipes_path = recipes_path

    def add_factory(self, factory_name):

        try:
            new_factory = utils.import_string(self._modules_path,
                                              factory_name)(host=self._host,
                                                            port=self._port,
                                                            vhost=self._vhost,
                                                            username=self._username,
                                                            password=self._password,
                                                            recipes_path=self._recipes_path,
                                                            finish_exchange=self._finish_exchange,
                                                            log_queue=self._log_queue)
            if not isinstance(new_factory, AbstractFactory):
                raise Exception("Class {} is not instance of AbstractFactory.".format(self._modules_path +
                                '.' + factory_name))

            new_factory.start_consumers()
            self._factories_list.append(new_factory)

            print("[+] Successful launched factory {}.".format(factory_name))

        except Exception as e:
            print("[x] Cannot launch factory {} : {}".format(factory_name, e))

    def load_recipe(self, recipe_id):

        try:
            recipes = FileRecipesDAO.get_recipes(self._recipes_path)
            recipe = recipes.get_recipe_by_id(recipe_id)
            if not recipe:
                raise Exception("Recipe {} does not exist in {} .".format(recipe_id, self._recipes_path))

            for factory in recipe.factories:
                self.add_factory(factory)

        except Exception as e:
            print("[x] Cannot load factories from recipe {} : {}".format(recipe_id, e))

    def list(self):

        if self._factories_list:
            for factory in self._factories_list:
                print(" - " + factory.name)
        else:
            print(" No factory to list.")
