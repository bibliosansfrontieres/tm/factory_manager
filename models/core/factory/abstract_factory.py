# -*- coding: utf-8 -*-

import abc
import pika.exceptions

from repository.recipe.FileRecipesDAO import FileRecipesDAO
from models.consumer import ConsumerThread
from repository.message.JsonMessageDAO import JsonMessageDAO
from models.core.producer.producerResults import ProducerResults
from services.logger.MongoLogger import MongoLogger
from models.core.process_status import ProcessStatus
from services.process.MongoProcessLogger import MongoProcessLogger

APP_PREFIX = "tmp."


class AbstractFactory:
    """`Abstract class`.\n
    Main structure to run a process.

    Parameters
    ----------
    host : `str`
        Connection host.
    port : `int`
        Connection port.
    vhost : `str`
        Connection virtual host.
    username : `str`
        Connection username.
    password : `str`
        Connection password.
    recipes_path : `str`
        Path of the recipes file.
    finish_exchange : `str`
        Exchange to publish the message after execution and to continue the process.
    log_queue : `str`
        Name of the RabbitMQ queue for logs.

    See Also
    --------
    factory_manager.models.logger.RabbitMQLogger

    References
    ----------
    [1] https://gitlab.com/bibliosansfrontieres/tm/factory_manager/blob/master/temps-modernes-behaviour.png
    """

    def __init__(self, host, port, vhost, username, password, recipes_path, finish_exchange, log_queue):

        self._name = type(self).__name__
        self._recipes = FileRecipesDAO.get_recipes(recipes_path)
        self._finish_exchange = finish_exchange

        self._host = host
        self._port = port
        self._vhost = vhost
        self._username = username
        self._password = password

        # Use only one logger please, comment the other.
        # self._logger = RabbitMQLogger(__name__, host, port, vhost, username, password, log_queue)
        # Is using the same host, port, username, password than RabbitMQ connection (for the moment)
        self._logger = MongoLogger(__name__, host, port, username, password)

        self._logger.info("[{}] Initializing. (host: {}, port: {}, vhost: {})".format(self._name,
                                                                                      host,
                                                                                      port,
                                                                                      vhost))

    @property
    @abc.abstractmethod
    def producers(self):
        """`Abstract property`.\n
        `list` of `AbstractProducer`: List of producers inheriting from `AbstractProducer` to
         run inside the factory.
        """
        return []

    @property
    def name(self):
        """`str`: Name of the factory.
        """
        return self._name

    @property
    def queue_init(self):
        """`str`: Name of the factory's "init" queue.
        """
        return APP_PREFIX + self._name + ".init"

    @property
    def queue_execute(self):
        """`str`: Name of the factory's "execute" queue.
        """
        return APP_PREFIX + self._name + ".execute"

    def start_consumers(self):
        """Launch the consumers for the queue `init` and `execute` of the factory in different threads.
        """
        consumers = [
            ConsumerThread(self._init_callback,
                           self.queue_init,
                           self._host,
                           self._port,
                           self._vhost,
                           self._username,
                           self._password),
            ConsumerThread(self._start_callback,
                           self.queue_execute,
                           self._host,
                           self._port,
                           self._vhost,
                           self._username,
                           self._password)
        ]
        for t in consumers:
            t.start()

    def _start(self, ch, msg):
        """Start the execution.

        Execute all the producers in the factory. If no error occurred during the execution and
        is not the last factory in the process recipe then publish the `Message` updated to the
        next factory queue "execute".

        Parameters
        ----------
        ch : `pika.Channel`
            Channel used to publish the message to the new exchange using the same `Connection`.
        msg : `Message`
            Message that contains all the information of the current process.
        """
        if isinstance(self._logger, MongoLogger):
            self._logger.update_msg(msg)
        self._logger.info("[{}] Starting execution.".format(self._name))

        try:
            result = self._execute(msg)
            if result.has_warnings():
                self._logger.error("[{}] Factory processed with warning. Exiting process.".format(self.name))

            else:
                next_factory = msg.process.next_factory(self._name)
                if next_factory:
                    # publish the message to the queue of the next factory
                    routing_key = APP_PREFIX + next_factory + '.execute'

                    body = msg.get_body()
                    self._logger.info("[{}] Factory processed successfully. \
                                      Publishing message to exchange {} with routing key {}."
                                      .format(self._name, self._finish_exchange, routing_key))

                    ch.basic_publish(
                        exchange=self._finish_exchange,
                        routing_key=routing_key,
                        body=body,
                        properties=pika.BasicProperties(delivery_mode=2)
                    )
                else:
                    # is the last factory in the recipe
                    self._logger.info("[{}] Factory processed successfully. \
                                      Exiting process (final factory)".format(self._name))

        except pika.exceptions.ChannelClosed:
            self._logger.error("[{}] Failed to publish message to exchange {}.".format(self._name,
                                                                                       self._finish_exchange))
        except Exception as e:
            msg.process.status = ProcessStatus.failure
            MongoProcessLogger().update_message(msg)
            self._logger.error("[{}] {}\n Stopping process".format(self._name, e))

    def _init_callback(self, ch, method, properties, body):
        """Callback method for its own queue "init".

        Acknowledge the message in queue "init"; then if this is the first factory in the process recipe,
         the factory is executed.

        Parameters
        ----------
        ch : `pika.Channel`
        method : `pika.spec.Basic.Deliver`
        properties : `pika.spec.BasicProperties`
        body : `str`, `unicode`, or `bytes`

        Notes
        -----
        Each queue "init" receives one message from the api when the process is launched by a fan-out
         exchange; and during the process don't receive any more message.
        """
        try:
            msg = JsonMessageDAO.get_message(body)
            current_recipe = self._recipes.get_recipe_by_id(msg.process.recipe.id)

            first_factory = current_recipe.get_factory_by_index(0)
            if first_factory and first_factory == self._name:
                msg.process.recipe = current_recipe
                msg.process.status = ProcessStatus.running
                MongoProcessLogger().update_message(msg)
                self._start(ch, msg)

            ch.basic_ack(delivery_tag=method.delivery_tag)
        except Exception as e:
            self._logger.error("[{}] Cannot start process : {}".format(self._name, e))

    def _start_callback(self, ch, method, properties, body):
        """Callback method for its own queue "execute".

        Acknowledge the message in queue "execute"; then execute.

        Parameters
        ----------
        ch : `pika.Channel`
        method : `pika.spec.Basic.Deliver`
        properties : `pika.spec.BasicProperties`
        body : `str`, `unicode`, or `bytes`
        """
        try:
            ch.basic_ack(delivery_tag=method.delivery_tag)

            msg = JsonMessageDAO.get_message(body)
            self._start(ch, msg)

        except Exception as e:
            self._logger.error("[{}] Cannot start process : {}".format(self._name, e))

    def _execute(self, msg):
        """Execute the factory's producers synchronously.

        Parameters
        ----------
        msg : `Message`
            Message that contains all the information of the current process.

        Returns
        -------
        results : `Results`
            Results of the execution of all producers.
        """
        results = ProducerResults()

        for producer in self.producers:
            result = producer.start(msg, self._logger)
            results.add_result(result)

        return results
