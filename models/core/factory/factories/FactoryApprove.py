# -*- coding: utf-8 -*-

from models.core.factory.abstract_factory import AbstractFactory
from models.core.producer.producers.factory_approve import ProducerApprove, ProducerReject, ProducerRemoveContainerData


class FactoryApprove(AbstractFactory):

    producers = [
        ProducerApprove(),
        ProducerReject(),
        ProducerRemoveContainerData()
    ]
