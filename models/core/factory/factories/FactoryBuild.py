# -*- coding: utf-8 -*-

from models.core.factory.abstract_factory import AbstractFactory
from models.core.producer.producers.factory_build import ProducerMakeIdeascubePackages, \
     ProducerCopyTo, ProducerMakeOlipPackages


class FactoryBuild(AbstractFactory):

    producers = [
        ProducerMakeIdeascubePackages(),
        ProducerMakeOlipPackages(),
        ProducerCopyTo()
    ]
