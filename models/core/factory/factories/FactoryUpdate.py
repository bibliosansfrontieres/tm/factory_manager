# -*- coding: utf-8 -*-

from models.core.factory.abstract_factory import AbstractFactory
from models.core.producer.producers.factory_update import ProducerUpdateConf


class FactoryUpdate(AbstractFactory):

    producers = [
        ProducerUpdateConf()
    ]
