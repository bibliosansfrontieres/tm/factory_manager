# -*- coding: utf-8 -*-

from models.core.factory.abstract_factory import AbstractFactory
from models.core.producer.producers.factory_configure import ProducerUpdateCatalog, \
        ProducerUpdateDeviceList, ProducerUpdateMongoDB, ProducerUpdateDescriptors


class FactoryConfigure(AbstractFactory):

    producers = [
        # update
        ProducerUpdateCatalog(),
        ProducerUpdateDeviceList(),
        ProducerUpdateDescriptors(),
        ProducerUpdateMongoDB()
    ]
