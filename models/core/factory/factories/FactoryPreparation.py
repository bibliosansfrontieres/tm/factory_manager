# -*- coding: utf-8 -*-

from models.core.factory.abstract_factory import AbstractFactory
from models.core.producer.producers.factory_preparation import ProducerGetProject, \
        ProducerListPackagesOmeka, ProducerListItemsRelations, ProducerCheckPackagesOmeka, \
        ProducerCheckUrlAvailability, ProducerInitWorkspace


class FactoryPreparation(AbstractFactory):

    producers = [
        ProducerGetProject(),
        ProducerListPackagesOmeka(),
        ProducerListItemsRelations(),
        ProducerCheckPackagesOmeka(),
        ProducerCheckUrlAvailability(),
        ProducerInitWorkspace()
    ]
