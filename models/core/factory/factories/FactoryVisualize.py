# -*- coding: utf-8 -*-

from models.core.factory.abstract_factory import AbstractFactory
from models.core.producer.producers.factory_visualize import ProducerOlip, \
        ProducerIdeascube, ProducerProvisionOlip


class FactoryVisualize(AbstractFactory):

    producers = [
        ProducerIdeascube(),
        ProducerOlip(),
        ProducerProvisionOlip()
    ]
