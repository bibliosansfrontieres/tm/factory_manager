# -*- coding: utf-8 -*-

from models.core.factory.abstract_factory import AbstractFactory
from models.core.producer.producers.factory_cleanup import ProducerRemoveOldContainers


class FactoryCleanUp(AbstractFactory):

    producers = [
        ProducerRemoveOldContainers()
    ]
