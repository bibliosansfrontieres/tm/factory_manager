# -*- coding: utf-8 -*-

from models.core.factory.abstract_factory import AbstractFactory
from models.core.producer.producers.factory_deploy import ProducerDeploy


class FactoryDeploy(AbstractFactory):

    producers = [
            ProducerDeploy()
    ]
