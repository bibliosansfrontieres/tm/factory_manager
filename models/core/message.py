# -*- coding: utf-8 -*-

import json
from repository.process.DictProcessDAO import KEYWORD_PROCESS
from repository.process.DictProcessDAO import DictProcessDAO


class Message:
    """Message that stores all the content for a process.

    Parameters
    ----------
    data : `dict`
        Dictionary with the content of the `Message`. Should include at least these fields:

        - ``KEYWORD_PROCESS``: dictionary with the content of a `Process`.

    See Also
    --------
    factory_manager.models.process.Process
    factory_manager.models.process.KEYWORD_PROCESS
    """

    def __init__(self, data):

        self._data = data
        self._process = DictProcessDAO.get_process(data)

    @property
    def process(self):
        """`Process`: Current `Process` to manage the operations of the application.
        """
        return self._process

    def get_value(self, key):
        """Index in the `Message`.

        Parameters
        ----------
        key : `str`
            Key value to index in the internal dictionary.

        Returns
        -------
        value : `object`
            Value that correspond to the ``key`` in the internal dictionary.

        Raises
        ------
        Exception
            Raised if the internal dictionary does not contain the ``key`` in its keys.
        """
        if key not in self._data:
            raise Exception("Missing '{}' in the message.".format(key))
        return self._data.get(key)

    def set_value(self, key, value):
        """Update values in the internal dictionary.

        Parameters
        ----------
        key : `str`
            Key value to update in the internal dictionary. If ``key`` is not in the internal dictionary
            then the entry will be created with the respective ``value``.
        value : `object`
            New value to assign to key in the internal dictionary.

        Raises
        ------
        Exception
            Raised if the ``key`` corresponds to the value of the constant ``KEYWORD_PROCESS``.
        """
        if key == KEYWORD_PROCESS:
            raise Exception("Value of {} cannot be set.".format(KEYWORD_PROCESS))
        self._data[key] = value

    def get_body(self):
        """Serialize the `Message` as string.

        Update the internal `process` and serialize the `Message` in json format.

        Returns
        -------
        json_dict : `str`
            The `Message` serialized in json format.
        """
        return json.dumps(self.to_dict(), ensure_ascii=False)

    def to_dict(self):
        """Get the `Message` as dictionary.

        Update the internal `process` and give the `Message` as dictionary.

        Returns
        -------
        dict : `dict`
            The `Message` as dictionary.
        """
        DictProcessDAO.update_process(self._data, self._process)
        return self._data
