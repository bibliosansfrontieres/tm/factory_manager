# -*- coding: utf-8 -*-

import pika
import pika.exceptions


class RabbitMQConnection:

    def __init__(self, host, port, vhost, username, password):

        self._host = host
        self._port = port
        self._vhost = vhost

        self._credentials = pika.PlainCredentials(username, password)

        self._connection, self._channel = None, None
        self.connect()

    def connect(self):

        try:
            if not self._connection or self._connection.is_closed:
                self._connection = pika.BlockingConnection(pika.ConnectionParameters(self._host,
                                                                                     self._port,
                                                                                     self._vhost,
                                                                                     self._credentials,
                                                                                     connection_attempts=10,
                                                                                     retry_delay=3,
                                                                                     heartbeat=0))
            if not self._channel or self._channel.is_closed:
                self._channel = self._connection.channel()

        except pika.exceptions.AMQPConnectionError:
            raise Exception("Failed to establish connection with the RabbitMQ server.")

    def publish(self, body, exchange, routing_key):

        try:

            if not self._connection or self._connection.is_closed or not self._channel or \
                                                                     self._channel.is_closed:
                self.connect()
            self._channel.basic_publish(
                exchange=exchange,
                routing_key=routing_key,
                body=body,
                properties=pika.BasicProperties(delivery_mode=2)
            )

        except pika.exceptions.ChannelClosed:
            self.close()
            raise Exception("Impossible to publish message to exchange {}.".format(exchange))

    def start_consume(self, callback, queue):

        try:
            if not self._connection or self._connection.is_closed or not self._channel or \
                                                                     self._channel.is_closed:
                self.connect()

            self._channel.basic_qos(prefetch_count=1)
            self._channel.basic_consume(callback, queue=queue)
            self._channel.start_consuming()

        except Exception as e:
            self.close()
            raise Exception("Impossible to start consume queue {} : {}".format(queue, e))

    def close(self):

        if self._channel:
            self._channel.close()
        if self._connection:
            self._connection.close()

        self._connection, self._channel = None, None
