# -*- coding: utf-8 -*-
from models.core.cap_config import CapConfig
from models.core.deploy_devices_config import DeployDevices
KEYWORD_DEPLOY = "deployment"


class Deploy:

    def __init__(self, process_id, devices, cap_config, parent_process_id=str()):
        if not isinstance(process_id, str) or not isinstance(devices, list) or \
           not isinstance(cap_config, CapConfig):
            raise TypeError("Invalid arguments type for Deploy.")

        self._process_id = process_id
        self._devices = DeployDevices(devices)
        self._cap_config = cap_config
        self._parent_process_id = parent_process_id

    @property
    def process_id(self):
        return self._process_id

    @property
    def devices(self):
        """
        List of DeployDevice objects
        """
        return self._devices

    @property
    def cap_config(self):
        return self._cap_config

    @cap_config.setter
    def cap_config(self, value):
        self._cap_config = value

    @property
    def parent_process_id(self):
        return self._parent_process_id

    @parent_process_id.setter
    def parent_process_id(self, value):
        self._parent_process_id = value

    def to_dict(self):

        return {
            "process_id": self._process_id,
            "devices": self._devices.to_dict(),
            "cap_config": self._cap_config.to_dict(),
            "parent_process_id": self._parent_process_id
        }
