KEYWORD_USER = "user"


class User:
    """
    User Class for session handling.
    """
    def __init__(self, username=str(), password=str()):
        self._username = username
        self._password = password

    @property
    def username(self):
        return self._username

    @property
    def password(self):
        return self._password

    def to_dict(self):
        return {
            "username": self._username,
            "password": str()
        }
