class ContainerConf:
    def __init__(self, name, image_id, content):

        self.name = name
        self.image_id = image_id
        self.content = content

    def to_dict(self):
        return {
                "name": self.name,
                "image_id": self.image_id,
                "content": self.content
                }
