# -*- coding: utf-8 -*-

KEYWORD_PROJECT = "project"
"""`str` : Keyword used in process dictionary to index a `OmekaProject`."""


class OmekaProject:
    """Representation of a project in Omeka.

    Parameters
    ----------
    project_id : `str`
        Id of the `ProjectOmeka`.
    name : `str`
        Name of the project.
    title : `str`, optional
        Title of the project ("no title", by default).
    description : `str`, optional
        Description of the project ("no description", by default).
    language : `str`, optional
        Language of the project ("fr", by default).
    device : `str`, optional
        Device name of the project ("koombook", by default).
    """

    def __init__(self, project_id, name, title=None, description=None, language=None, device=None, country=None):
        if not isinstance(project_id, int) or not isinstance(name, str):
            raise TypeError("Invalid data type for OmekaProject object. project_id : {}, name : {}".format(
                            type(project_id), type(name)))

        self._id = project_id
        self._name = name

        self._title = "no title" if title is None else title
        self._description = "no description" if description is None else description
        self._language = "fr" if language is None else language
        self._device = "koombook" if device is None else device
        self._country = "France" if country is None else country

    @property
    def id(self):
        """`int`: Id of the project.
        """
        return self._id

    @property
    def name(self):
        """`str`: Name of the project.
        """
        return self._name

    @property
    def title(self):
        """`str`: Title of the project.
        """
        return self._title

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def description(self):
        """`str`: Description of the project.
        """
        return self._description

    @description.setter
    def description(self, value):
        self._description = value

    @property
    def language(self):
        """`str`: Language of the project.
        """
        return self._language

    @language.setter
    def language(self, value):
        self._language = value

    @property
    def device(self):
        """`str`: Device name of the project.
        """
        return self._device

    @device.setter
    def device(self, value):
        self._device = value

    @property
    def country(self):
        """`str`: Country of the project.
        """
        return self._country

    @country.setter
    def country(self, value):
        self._country = value

    def to_dict(self):
        """Get the `OmekaProject` as dictionary.

        Returns
        -------
        dict : `dict`
            The `OmekaProject` as dictionary with the following format:

            - ``'id'``: Id of the project.
            - ``'name'``: Name of the project.
            - ``'title'``: Title of the project.
            - ``'description'``: Description of the project.
            - ``'language'``: Language of the project.
            - ``'device'``: Device name of the project.
            - ``'country'``: Country of the project.
        """
        return {
            'id': self._id,
            'name': self._name,
            'title': self._title,
            'description': self._description,
            'language': self._language,
            'device': self._device,
            'country': self._country
        }
