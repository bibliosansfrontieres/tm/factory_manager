# -*- coding: utf-8 -*-
"""CAP documentation.
"""


class CapConfig:
    """CapConfig that contains the configuration for a CAP device.

    Parameters
    ----------
    process_id : `str`
        Id of the current `Process`.
    containers : `list` of `dict`, mandatory
        List of installed containers with their content.

    Raises
    ------
    TypeError
        Raised if any arguments for the `CapConfig` is not the expected type.

    Notes
    -----
    """

    def __init__(self, process_id, project_name=None, timezone=None, containers=None):
        if not isinstance(process_id, str) and not isinstance(project_name, str) and \
           not isinstance(timezone, str) and not isinstance(containers, list):
            raise TypeError("Invalid arguments type for CapConfig.")

        self._process_id = process_id
        self._project_name = project_name
        self._timezone = timezone
        self._containers = containers

    @property
    def process_id(self):
        """`int`: Id of the process.
        """
        return self._process_id

    @property
    def project_name(self):
        """`project_name` of `str`: project_name for this configuration.
        """
        return self._project_name

    @property
    def timezone(self):
        return self._timezone

    @property
    def containers(self):
        """`List` value expected.
        """
        return self._containers

    def to_dict(self):
        """Get the `Approval` as dictionary.

        Returns
        -------
        dict : `dict`
            The `Recipe` as dictionary with the following format:

            - ``'process_id'``: id of the `Process`.
        """
        return {
            "process_id": self._process_id,
            "project_name": self._project_name,
            "timezone": self._timezone,
            "containers": self._containers
        }
