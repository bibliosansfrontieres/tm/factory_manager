# -*- coding: utf-8 -*-


class DeployDevice:

    def __init__(self, mac_address=str(), name=str(), project_name=str()):
        if not isinstance(mac_address, str):
            raise TypeError("Invalid arguments type for DeployDevice.")

        self._mac_address = mac_address.lower()
        self._name = self._mac_address if name is str() else name
        self._project_name = project_name

    @property
    def mac_address(self):
        return self._mac_address

    @property
    def name(self):
        return self._name

    @property
    def project_name(self):
        return self._project_name

    def to_dict(self):

        return {
            "mac_address": self._mac_address,
            "project_name": self._project_name,
            "name": self._name
        }
