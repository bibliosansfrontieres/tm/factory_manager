# -*- coding: utf-8 -*-
"""Approval documentation.
"""

KEYWORD_APPROVAL = "approval"
"""`str` : Keyword used in process dictionary to index a `Approval`."""


class Approval:
    """Approval that contains the confirmation of process.

    Parameters
    ----------
    recipe_id : `int`
        Id of the `Recipe`.
    factories : `list` of `str`, optional
        Name of the factories in `Recipe` (empty list, by default).

    Raises
    ------
    TypeError
        Raised if any arguments for the `Approval` is not the expected type.

    Notes
    -----
    """
    def __init__(self, process_id, timestamp=None, approved=None, description=None,
                 parent_process_id=None):
        if not isinstance(process_id, str):
            raise TypeError("Invalid process_id type for Approval.")

        self._process_id = process_id
        self._timestamp = timestamp
        self._approved = approved
        self._description = description
        self._parent_process_id = parent_process_id

    @property
    def process_id(self):
        """`int`: Id of the process.
        """
        return self._process_id

    @property
    def timestamp(self):
        return self._timestamp

    @property
    def approved(self):
        """`Boolean` value expected.
        """
        return self._approved

    @approved.setter
    def approved(self, value):
        self._approved = value

    @property
    def description(self):
        return self._description

    @property
    def parent_process_id(self):
        return self._parent_process_id

    @parent_process_id.setter
    def parent_process_id(self, value):
        self._parent_process_id = value

    def to_dict(self):
        """Get the `Approval` as dictionary.

        Returns
        -------
        dict : `dict`
            The `Recipe` as dictionary with the following format:

            - ``'process_id'``: id of the `Process`.
        """
        return {
            "process_id": self._process_id,
            "timestamp": self._timestamp,
            "approved": self._approved,
            "description": self._description,
            "parent_process_id": self._parent_process_id
        }
