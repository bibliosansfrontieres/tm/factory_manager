# -*- coding: utf-8 -*-
from models.core.deploy_device_config import DeployDevice


class DeployDevices:

    def __init__(self, devices):
        if not isinstance(devices, list):
            raise TypeError("Invalid arguments type for DeployDevices.")

        self._devices = list()

        try:
            for device in devices:
                mac_address = str() if device.get('mac_address') is None else device.get('mac_address')
                name = str() if device.get('name') is None else device.get('name')
                project_name = str() if device.get('project_name') is None else device.get('project_name')
                self._devices.append(
                    DeployDevice(mac_address, name, project_name)
                )

        except KeyError as e:
            print('KeyError : {}'.format(e))
            self._devices = [DeployDevice()]

    @property
    def devices(self):
        return self._devices

    def to_dict(self):
        devs = list()
        for device in self._devices:
            devs.append(device.to_dict())

        return devs
