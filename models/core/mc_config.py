# -*- coding: utf-8 -*-
from datetime import datetime

KEYWORD_CONFIG = "media_center_configuration"
"""`str` : Keyword used in process dictionary to index a `MediaCenterConfig`."""


class MediaCenterConfig:
    """Configuration for the Media Center.

    Parameters
    ----------
    engine : {'ideascube', 'hugo'}
        Engine on which the Media Center will be built.
    languages : `list` of `str`
        List of languages that the Media Center supports.
    """

    def __init__(self, engine, languages, deployable, expire, api_url, port_range=None, port_range_id=None):

        self._engine = engine
        self._languages = languages
        self._deployable = deployable
        self._expire = expire
        self._api_url = api_url
        self._port_range = port_range
        self._port_range_id = port_range_id

    @property
    def engine(self):
        """`str`: Engine of the media center.
        """
        return self._engine

    @property
    def languages(self):
        """`list` of `str`:Languages of the media center.
        """
        return self._languages

    @property
    def deployable(self):
        """`list` of `str`:Languages of the media center.
        """
        return self._deployable

    @property
    def expire(self):
        """`datetime` object for container validity.
        """
        return self._expire

    @property
    def api_url(self):
        """`str` container the OLIP-specific URL for its API.
        """
        return self._api_url

    @api_url.setter
    def api_url(self, value):
        self._api_url = value

    @property
    def expire_to_datetime(self):
        """Convert a `str` in datetime.now().__str__() format into a datetime object.
        """
        return datetime.strptime(self._expire, '%Y-%m-%d %H:%M:%S.%f')

    @property
    def port_range(self):
        return self._port_range

    @port_range.setter
    def port_range(self, value):
        self._port_range = value

    @property
    def port_range_id(self):
        return self._port_range_id

    @port_range_id.setter
    def port_range_id(self, value):
        self._port_range_id = value

    def to_dict(self):
        """Get the `MediaCenterConfig` as dictionary.

        Returns
        -------
        dict : `dict`
            The `MediaCenterConfig` as dictionary with the following format:

            - ``'engine'``: Engine of the media center.
            - ``'languages'``: List of languages of the media center.
        """
        return {
            'engine': self._engine,
            'languages': self._languages,
            'deployable': self._deployable,
            'api_url': self._api_url,
            'expire': self._expire.__str__(),
            'port_range': self._port_range,
            'port_range_id': self._port_range_id
        }
