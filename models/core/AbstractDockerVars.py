from models.core.IDockerVars import IDockerVars
from settings import DOCKER_VARS


class AbstractDockerVars(IDockerVars):
    """
        see https://docker-py.readthedocs.io/en/stable/containers.html for more infos.
    """
    def __init__(self):
        pass

    @property
    def image(self):
        return "not_set"

    @property
    def volumes(self):
        pass

    @property
    def volumes_from(self):
        pass

    @property
    def name(self):
        return "default_Name"

    @property
    def detach(self):
        return True

    @property
    def remove(self):
        return True

    @property
    def environment(self):
        return ["PROJECT_NAME=idb-bsf-vagrant", "LANG=C.UTF-8"]

    @property
    def announce_container(self):
        return {"announce": False}

    @property
    def privileged(self):
        return False

    @property
    def labels(self):
        pass

    @property
    def network(self):
        return DOCKER_VARS['network']

    @property
    def command(self):
        pass

    @property
    def wait(self):
        return False
