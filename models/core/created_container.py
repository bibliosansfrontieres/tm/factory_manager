from datetime import datetime

container_collection = "containers"


class CreatedContainersModel:
    def __init__(self, name, container_id, expire, process_id, deleted=False):
        if not isinstance(name, str) or not isinstance(container_id, str) or \
           not isinstance(expire, datetime) or not isinstance(process_id, str) or \
           not isinstance(deleted, bool):
            raise TypeError("Invalid type or missing arguments for CreatedContainers object.")

        self._name = name
        self._container_id = container_id
        self._expire = expire
        self._process_id = process_id
        self._deleted = deleted

    @property
    def name(self):
        return self._name

    @property
    def container_id(self):
        return self._container_id

    @property
    def process_id(self):
        return self._process_id

    @property
    def expire(self):
        return self._expire

    @property
    def deleted(self):
        return self._deleted

    @deleted.setter
    def deleted(self, value):
        self._deleted = value

    def to_dict(self):
        return {
                "name": self._name,
                "container_id": self._container_id,
                "expire": self._expire,  # This is a datetime object, compatible with Mongo
                "process_id": self._process_id,
                "deleted": self._deleted
                }
