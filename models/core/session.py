from models.core.user import User
from itsdangerous import URLSafeTimedSerializer
from repository.login.MongoHelpers import MongoHelpers
from settings import FLASK_SECRET


class Session:
    """
    User Class for session handling.
    """
    def __init__(self, username=str(), password=str(), token=str()):

        self.login_serializer = URLSafeTimedSerializer(FLASK_SECRET)
        self._user = User(username, password)
        self._token = token
        self.help = MongoHelpers()

    @property
    def is_authenticated(self):
        return self.help.is_authenticated(self._token)

    def get_auth_token(self):
        """
        Encode a secure token based on login/pass.
        """
        return self.login_serializer.dumps([self._user.username, self._user.password])

    def add_session(self, token):
        """
        Write token to DB.
        """
        self._token = token
        self.help.add_session(token)

    def flush_old_sessions(self):
        """
        Some housekeeping, removing oudated entries.
        """
        self.help.flush_old_sessions()

    def to_dict(self):
        return {
            "user": self._user.to_dict(),
            "token": self._token
        }
