# -*- coding: utf-8 -*-
"""Recipe documentation.
"""

KEYWORD_RECIPE = "recipe"
"""`str` : Keyword used in process dictionary to index a `Recipe`."""


class Recipe:
    """Recipe that contains factories order of execution.

    Parameters
    ----------
    recipe_id : `int`
        Id of the `Recipe`.
    factories : `list` of `str`, optional
        Name of the factories in `Recipe` (empty list, by default).

    Raises
    ------
    TypeError
        Raised if any arguments for the `Recipe` is not the expected type.

    Notes
    -----
    The names of the factories inside ``factories`` should not have repeated factories names; to avoid
    problems in the process execution.
    """

    def __init__(self, recipe_id, name=str(), factories=None):
        if not isinstance(recipe_id, int) or (factories and not all(isinstance(f, str) for f in factories)):
            raise TypeError("Invalid arguments type for Recipe.")

        self._id = recipe_id
        self._name = name
        self._factories_list = [] if factories is None else factories

    @property
    def id(self):
        """`int`: Id of the recipe.
        """
        return self._id

    @property
    def name(self):
        """`string`: name of the recipe.
        """
        return self._name

    @property
    def factories(self):
        """`list` of `str`: List of factories names in recipe.
        """
        return self._factories_list

    def get_factory_by_index(self, index):
        """Get the name of the factory in the ``index`` position.

        Parameters
        ----------
        index : `int`
            Position of the requested factory.

        Returns
        -------
        factory_name : `str`
            Name of the factory in the ``index`` position. ``None`` in case of invalid index.
        """
        if 0 <= index < len(self._factories_list):
            return self._factories_list[index]
        return None

    def to_dict(self):
        """Get the `Recipe` as dictionary.

        Returns
        -------
        dict : `dict`
            The `Recipe` as dictionary with the following format:

            - ``'id'``: id of the `Recipe`.
            - ``'factories'``: list of factories names.
        """
        return {
            "id": self._id,
            "name": self._name,
            "factories": self._factories_list
        }
