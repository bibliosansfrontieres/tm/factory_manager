class Descriptor(object):
    """
    Olip descriptor.json file model.
    """
    def __init__(self, applications, terms):
        if not (isinstance(applications, list) and isinstance(terms, list)):
            raise TypeError('Applications and terms should be lists.')

        self.applications = applications
        self.terms = terms

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, Descriptor):
            return False

        eq = self.terms == other.terms \
            and self.applications == other.applications

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash(tuple(self.terms + self.applications))


class Application(object):

    def __init__(self, name, description, version, picture, containers, bundle, authentication=None,
                 contents=list(), search=None):
        if not (isinstance(containers, list) and isinstance(contents, list)):
            raise TypeError('Containers and contents should be lists.')

        self.name = name
        self.description = description
        self.version = version
        self.picture = picture
        self.containers = containers
        self.bundle = bundle
        self.contents = contents
        self.search = search
        self.authentication = authentication

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, Application):
            return False

        eq = self.name == other.name \
            and self.description == other.description \
            and self.version == other.version \
            and self.picture == other.picture \
            and self.containers == other.containers \
            and self.bundle == other.bundle \
            and self.contents == other.contents \
            and self.search == other.search \
            and self.authentication == other.authentication

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.name, self.description, self.version, self.picture,
                     self.containers, self.bundle, self.contents, self.search, self.authentication))


class Container(object):

    def __init__(self, image, name, expose=None):
        if expose:
            if not isinstance(expose, int):
                raise TypeError('Expose should be an Integer.')

        self.image = image
        self.name = name
        self.expose = expose

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, Container):
            return False

        eq = self.image == other.image \
            and self.name == other.name \
            and self.expose == other.expose

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.image, self.name, self.expose))


class Content(object):

    def __init__(self, content_id, name, description, language, subject,
                 version, size, endpoint, download_path, destination_path):

        if not (isinstance(endpoint, Endpoint)):
            raise TypeError('endpoint should be an Endpoint() instance.')

        self.content_id = content_id
        self.description = description
        self.name = name
        self.language = language
        self.subject = subject
        self.version = version
        self.size = size
        self.endpoint = endpoint
        self.download_path = download_path
        self.destination_path = destination_path

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, Content):
            return False

        eq = self.content_id == other.content_id \
            and self.name == other.name \
            and self.description == other.description \
            and self.language == other.language \
            and self.subject == other.subject \
            and self.version == other.version \
            and self.size == other.size \
            and self.endpoint == other.endpoint \
            and self.download_path == other.download_path \
            and self.destination_path == other.destination_path

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.content_id, self.description, self.name, self.language, self.subject,
                     self.version, self.size, self.endpoint, self.download_path, self.destination_path))


class Search(object):
    def __init__(self, opensearch_url, container):
        self.opensearch_url = opensearch_url
        self.container = container

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, Search):
            return False

        eq = self.opensearch_url == other.opensearch_url \
            and self.container == other.container

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.opensearch_url, self.container))


class Endpoint(object):

    def __init__(self, url, container, name):
        self.url = url
        self.container = container
        self.name = name

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, Endpoint):
            return False

        eq = self.url == other.url \
            and self.container == other.container \
            and self.name == other.name

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.url, self.container, self.name))


class Authentication(object):
    def __init__(self, grant_types=None, response_types=None, token_endpoint_auth_method=None):
        self.grant_types = grant_types
        self.response_types = response_types
        self.token_endpoint_auth_method = token_endpoint_auth_method

    def __eq__(self, other):
        if other is None:
            return False

        if not isinstance(other, Authentication):
            return False

        eq = self.grant_types == other.grant_types \
            and self.response_types == other.response_types \
            and self.token_endpoint_auth_method == other.token_endpoint_auth_method

        return eq

    def __repr__(self):
        return str(self.__dict__)

    def __hash__(self):
        return hash((self.grant_types, self.response_types, self.token_endpoint_auth_method))
