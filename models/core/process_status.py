class ProcessStatus:
    start = "started"
    running = "running"
    failure = "failure"
    success = "success"
