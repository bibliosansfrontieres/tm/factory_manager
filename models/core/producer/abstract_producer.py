# -*- coding: utf-8 -*-

import abc

from models.core.producer.exceptions import exceptions
from models.result import ResultType
from models.core.producer.producerResult import ProducerResult


class AbstractProducer:
    """`Abstract class`.\n
    Atomic class of a process. Here a function is executed.
    """

    def __init__(self):
        self._name = type(self).__name__

    @property
    def name(self):
        """`str`: Name of the producer.
        """
        return self._name

    def start(self, msg, logger):
        """Portal method to the producer's execution.

        Parameters
        ----------
        msg : `Message`
            Message that contains all the information of the current process.
        logger : `logging.Logger`
            Factory logger where the log messages will be published.

        Returns
        -------
        result : `ProducersResults`
            Dirty way to control whether the factory should continue or not.
            If the ResultType is SUCCESS the process will continue; if the ResultType is (an controlled error
            occurred) the process should be stopped after the execution of the container factory.

        Raises
        ------
        Exception
            Raised if any Exception is raised during the execution.
        """
        try:
            self.execute(msg, logger)
        except exceptions.ProducerWarning:
            return ProducerResult(ResultType.WARNING)
        except Exception as e:
            raise Exception("{} : {}".format(self._name, e))

        return ProducerResult(ResultType.SUCCESS)

    @abc.abstractmethod
    def execute(self, msg, logger):
        """`Abstract Method`.\n
        Producer's execution.

        Parameters
        ----------
        msg : `Message`
            Message that contains all the information of the current process.
        logger `logging.Logger`
            Factory logger where the log messages will be published.

        Raises
        ------
        exceptions.ProducerWarning
            Raised if during the execution of the producer an error occurred but the execution of the factory
            must continue.
        Exception
            Raised if during the execution an error occurred and the execution of the factory must me stopped.
        """
        pass
