# -*- coding: utf-8 -*-

from models.core.producer.abstract_producer import AbstractProducer
from repository.cleanup.CleanUpHelper import CleanUpMongoHelpers


class ProducerRemoveOldContainers(AbstractProducer):

    def execute(self, msg, logger):

        cleaner = CleanUpMongoHelpers(logger)
        cleaner.remove_old_containers()
