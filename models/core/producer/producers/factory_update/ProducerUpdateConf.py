# -*- coding: utf-8 -*-

from models.core.producer.abstract_producer import AbstractProducer
from repository.mongo.MongoDAO import MongoIO
from settings import PROD_MONGODB_CONNECTION_HOST, PROD_MONGODB_CONNECTION_PORT, \
    PROD_MONGODB_CONNECTION_USERNAME, PROD_MONGODB_CONNECTION_PASSWORD, PROD_MONGODB_NAME


class ProducerUpdateConf(AbstractProducer):

    def execute(self, msg, logger):

        cursor = MongoIO(collection="configuration",
                         host=PROD_MONGODB_CONNECTION_HOST,
                         port=PROD_MONGODB_CONNECTION_PORT,
                         username=PROD_MONGODB_CONNECTION_USERNAME,
                         password=PROD_MONGODB_CONNECTION_PASSWORD,
                         db=PROD_MONGODB_NAME)

        # We want to be sure one unique configuration exists for a given project_name
        conf_name = {"project_name": msg.process.deployment.cap_config.project_name}
        cursor.make_unique(conf_name)
        cursor.update_or_insert(conf_name, msg.process.deployment.cap_config.to_dict())
        logger.info("Wrote configuration for project {}".format(msg.process.deployment.cap_config.project_name))
