# -*- coding: utf-8 -*-

import os

from settings import OMEKA_CONFIG, IDEASCUBE_ALLOWED_LANGUAGES

KEYWORD_PACKAGE_ITEMS = "items"

METADATA_ERROR_TYPE = {
    50: "MISSING TITLE",
    41: "MISSING DESCRIPTION",
    44: "MISSING LANGUAGE",
    39: "MISSING CREATOR",
    400: "MISSING FILE",
    401: "MISSING MEDIA"
}


class ItemError:
    """Object to get a description of `OmekaPackageItem` error.

    Parameters
    ----------
    status_code : `int`
        Status code of error.
    """

    def __init__(self, status_code):
        self._status_code = status_code
        self._description = METADATA_ERROR_TYPE.get(status_code)

    @property
    def status_code(self):
        """`int`: Status code of error.
        """
        return self._status_code

    @property
    def description(self):
        """`str`: Description of error depending ot the status code.
        """
        return self._description


class OmekaPackageItem:
    """Omeka item inside packages. With the information required.

    Parameters
    ----------
    item_id : `int`
        Id of the item.
    title : `str`
        Title of the item.
    summary : `str`
        Summary of the item.
    lang : `str`
        Language of the item.
    item_credits : `str`
        Credits of the item.
    collection : `str`
        Collection of the item.
    path : `str`
        Path of the item.
    tags : `str`
        Tags of the item.
    filename : `str`
        Filename of the item.
    preview : `str`, optional
        Preview path of the item.
    file_url: `str`
        Omeka URL of file.
    thumbnail_url: `str`
        Omeka URL of thumbnail.
    mime_type: `str`
        Mime type of the file.

    Warnings
    --------
        The items language is actually patched to avoid conflicts with Ideascube. In case that the Ideascube
        does not accept the language of the item the language will be replaced by 'fr'.
    """

    def __init__(self, item_id, title, summary, lang, item_credits, collection, path, tags,
                 filename, file_url, thumbnail_url, mime_type, preview=None):

        self._id = item_id
        self._title = title
        self._summary = summary
        self._language = lang if (not lang or lang in IDEASCUBE_ALLOWED_LANGUAGES) else 'fr'
        self._credits = item_credits
        self._collection = "no collection" if (collection is None) else collection
        self._path = path
        self._tags = "" if (tags is None) else tags
        self._preview = "" if preview is None else preview
        self._filename = filename
        self._file_url = file_url
        self._mime_type = mime_type
        self._thumbnail_url = thumbnail_url
        self._filename_slug = str()
        self._file_extension = str()

    @property
    def id(self):
        """`int`: Id of the Omeka item.
        """
        return self._id

    @property
    def title(self):
        """`str`: Title of the Omeka item.
        """
        return self._title

    @property
    def summary(self):
        """`str`: Summary of the Omeka item.
        """
        return self._summary

    @property
    def language(self):
        """`str`: Language of the Omeka item.
        """
        return self._language

    @property
    def credits(self):
        """`str`: Author of the Omeka item.
        """
        return self._credits

    @property
    def collection(self):
        """`str`: Collection to which the item belongs.
        """
        return self._collection

    @property
    def path(self):
        """`str`: Path of the Omeka item.
        """
        return self._path

    @property
    def tags(self):
        """`str`: Tags of the item joined by a ',' (because Ideascube does not supports list).
        """
        return self._tags

    @property
    def preview(self):
        """`str`: Thumbnail path of the item.
        """
        return self._preview

    @property
    def file_url(self):
        """`str`: Thumbnail path of the item.
        """
        return self._file_url

    @property
    def filename(self):
        """`str`: filename of the item.
        """
        return self._filename

    @property
    def thumbnail_url(self):
        """`str`: Thumbnail path of the item.
        """
        return self._thumbnail_url

    @property
    def mime_type(self):
        """`str`: Mime type of the file.
        """
        return self._mime_type

    @property
    def filename_slug(self):
        """`str`: Slug for filename.
        """
        return self._filename_slug

    @filename_slug.setter
    def filename_slug(self, value):
        """`str`: Slug for filename.
        """
        self._filename_slug = value

    @property
    def file_extension(self):
        """`str`: File extension for self.filename.
        """
        return self._file_extension

    @file_extension.setter
    def file_extension(self, value):
        """`str`: File extension for self.filename.
        """
        self._file_extension = value

    def get_errors(self):
        """Get a list with the errors of the item.

        Returns
        -------
        errors : `list` of `ItemError`
            List of errors in item.

        See Also
        --------
        models.producers.utils.omeka_package_item.ItemError
        """
        errors = []

        if not self._title:
            errors.append(ItemError(50))
        if not self._summary:
            errors.append(ItemError(41))
        if not self._language:
            errors.append(ItemError(44))
        if not self._credits:
            errors.append(ItemError(39))
        if not self._path:
            errors.append(ItemError(401))
        if not os.path.exists(OMEKA_CONFIG['media_master_folder'] + self._path):
            print('Cannot find {}'.format(OMEKA_CONFIG['media_master_folder'] + self._path))
            errors.append(ItemError(400))

        return errors

    def to_dict(self):
        """Get the `OmekaPackageItem` as dictionary.

        Returns
        -------
        dict : `dict`
            The `OmekaPackageItem` as dictionary.
        """
        return {
            'id': self._id,
            'title': self._title,
            'summary': self._summary,
            'lang': self._language,
            'credits': self._credits,
            'collection': self._collection,
            'path': self._path,
            'tags': self._tags,
            'filename': self._filename,
            'file_url': self._file_url,
            'thumbnail_url': self._thumbnail_url,
            'preview': self._preview,
            'mime_type': self._mime_type
        }
