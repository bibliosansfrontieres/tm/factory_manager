# -*- coding: utf-8 -*-

import abc

from settings import EXTERNAL_PACKAGE_TYPE

KEYWORD_PACKAGE_DEVICES = "package_management"


class DeviceConfig:

    def __init__(self):
        self._kolibri_device = KolibriDevice(activated=False, version="0.10.3")
        self._portal_device = PortalDevice(activated=True)

    def activate_kolibri_items(self, relation_items):

        for item in relation_items:
            if item.type == EXTERNAL_PACKAGE_TYPE['khan_academy']:
                self._kolibri_device.activated = True
                self._kolibri_device.add_language(item.language)

    def to_dict(self):

        return {
            self._kolibri_device.name: self._kolibri_device.to_dict(),
            self._portal_device.name: self._portal_device.to_dict()
        }


class AbstractDevice:
    _name = ...
    _activated = False

    @property
    def name(self):
        return self._name

    @property
    def activated(self):
        return self._activated

    @activated.setter
    def activated(self, value):
        self._activated = value

    @abc.abstractmethod
    def to_dict(self):
        pass


class KolibriDevice(AbstractDevice):
    _name = "kolibri"

    def __init__(self, activated, version, language=None):

        self._activated = activated

        self._version = version
        self._language = [] if language is None else language

    @property
    def version(self):
        return self._version

    @property
    def language(self):
        return self._language

    @property
    def language_tostr(self):
        _lang_tostr = ",".join(self._language)
        return _lang_tostr

    def add_language(self, lang):
        if lang not in self._language:
            self._language.append(lang)

    def to_dict(self):

        return {
            "activated": self._activated,
            "version": self._version,
            "language": self._language
        }


class PortalDevice(AbstractDevice):
    _name = "portal"

    def __init__(self, activated):
        self._activated = activated

    def to_dict(self):
        return {
            "activated": self._activated
        }


class DevicePackage:

    def __init__(self, name, status="present"):
        self._name = name
        self._status = status

    def to_dict(self):
        return {
            "name": self._name,
            "status": self._status
        }


class DevicePackages:

    def __init__(self):
        self._device_packages = []

    def add_omeka_packages(self, omeka_packages):

        for pkg in omeka_packages:
            if pkg.slug:
                self._device_packages.append(DevicePackage(name=pkg.slug))

    def add_kiwix_packages(self, relation_items):

        for item in relation_items:
            if item.title and (item.type == EXTERNAL_PACKAGE_TYPE['kiwix_packages'] or
                               item.type == EXTERNAL_PACKAGE_TYPE['website']):
                self._device_packages.append(DevicePackage(name=item.title))

    def to_dict(self):
        tmp = []
        for elem in self._device_packages:
            tmp.append(elem.to_dict())

        return {
            KEYWORD_PACKAGE_DEVICES: tmp
        }

    def to_dict_no_parent(self):
        tmp = []
        for elem in self._device_packages:
            tmp.append(elem.to_dict())

        return tmp


class DeviceList:

    def __init__(self, data):
        self._data = data

    def prepare_project(self, project_name):
        self._data[project_name] = {}

    def update_config(self, device_config, project_name):
        self._data[project_name].update(device_config.to_dict())

    def update_packages(self, device_packages, project_name):
        self._data[project_name].update(device_packages.to_dict())

    def to_dict(self):
        return self._data
