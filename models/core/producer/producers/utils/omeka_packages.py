from models.core.producer.producers.utils.omeka_package import OmekaPackage


class OmekaPackages(list):
    """Collection of `OmekaPackage` instances.

    Parameters
    ----------
    packages_list : `list` of `OmekaPackage`, optional
        Content of the `OmekaPackages` (empty list, by default).

    Raises
    ------
    TypeError
        Raised if any arguments for the `OmekaPackages` is not the expected type.
    """

    def __init__(self, packages_list=None):
        if packages_list and not all(isinstance(pkg, OmekaPackage) for pkg in packages_list):
            raise TypeError("Invalid arguments type for OmekaPackages.")
        super().__init__(packages_list)

        self._packages_list = [] if packages_list is None else packages_list

    def count(self):
        """Amount of packages inside the collection.

        Returns
        -------
        len : `int`
            Amount of packages.
        """
        return len(self._packages_list)

    def to_dict(self):
        """Get the collection of packages as list.

        Returns
        -------
        list : `list`
            The `OmekaPackages` as list; with each package in dictionary format.
        """
        tmp = []

        for pkg in self._packages_list:
            tmp.append(pkg.to_dict())
        return tmp
