# -*- coding: utf-8 -*-
import os
import subprocess
import glob

from settings import CATALOGS, REMOTE_USER, BUBBLE_DEST_SERVER, BUBBLE_CATALOG_FOLDER, \
    GIT_CONFIG


def synchronize_zip(source, destination):

    zip_files = glob.glob(source + "*.zip")
    if zip_files:
        """Don't use rsync(1) here
        cf bibliosansfrontieres/tm/tmcloud/temps-modernes/packer-tm#7
        """
        subprocess.run([
            "cp --force --update "
            + ' '.join(zip_files)
            + ' ' + destination
            ],
            check=True,
            shell=True
        )


def synchronize(source_file, destination):

    subprocess.run([
        "rsync",
        "--no-whole-file",
        source_file,
        destination
    ])


def synchronize_descriptors(descriptors):
    """ Copy local descriptors to remote destination
    :param descriptors: Descriptors local infos.
    :type descriptors: list => [{
        'url': 'http://olip.ideascube.org/amd64/descriptor.json',
        'filename': '/tmp/idc-bsf-qas/tmprc98ueb4',
        'rsync_dst': 'root@olip.ideascube.org:/var/www/olip.ideascube.org//amd64/descriptor.json'
    }]
    """
    for c in descriptors:
        synchronize(
            c['filename'],
            c['rsync_dst']
        )


def _htmlize_catalog(catalog):
    with open(catalog, 'r') as file:
        content = file.read()

    return """
        <html>
            <head>
                <title>{}</title>
                <meta charset='utf-8' />
            </head>
            <body>
                <pre>
                    {}
                </pre>
            </body>
        </html>""".format(catalog, content)


def push_to_bubble(logger):
    destination = REMOTE_USER + "@" + BUBBLE_DEST_SERVER + ":" + BUBBLE_CATALOG_FOLDER

    os.chdir(GIT_CONFIG['catalog-io_local_repo'])

    for catalog in CATALOGS:
        html_content = _htmlize_catalog(catalog)

        source_file = catalog + ".html"
        with open(source_file, 'w') as file:
            file.write(html_content)

        synchronize(source_file, destination)
        synchronize(catalog, destination)

    logger.debug("Copied the {} catalog to {}".format(catalog, destination))
