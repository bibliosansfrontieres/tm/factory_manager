import time


class Timer:

    def __init__(self, logger=None, msg=str()):

        self._start_time = None
        self.logger = print if logger is None else logger.debug
        self.msg = msg

    def __enter__(self):
        """Start a new timer as a context manager"""

        self.logger('Starting Timer()')

        self._start_time = time.perf_counter()

        return self

    def __exit__(self, *exc_info):
        """Stop the context manager timer"""

        elapsed_time = time.perf_counter() - self._start_time

        self._start_time = None

        self.logger('{} : {} seconds'.format(self.msg, elapsed_time))
