# -*- coding: utf-8 -*-

from datetime import datetime

from settings import IDC_DEPLOY_CONFIG

KEYWORD_OMEKA_PACKAGES = "omeka_packages"


class OmekaPackage:
    """Package obtained from omeka. With the information required.

    Parameters
    ----------
    pkg_id : `int`
        Id of the omeka package.
    slug : `str`
        Slug of the omeka package.
    description : `str`
        Description of the omeka package.
    goal : `str`
        Goal of the omeka package.
    language : `str`
        Language of the omeka package.
    last_exportable_modification : `datetime`
        last exportable modification date of the omeka package.
    ideascube_name : `str`
        name of the Ideascube of the omeka package.
    related_items : `OmekaPackageItems`
        Items in the omeka package.
    """

    def __init__(self, pkg_id, slug, description, goal, language, last_exportable_modification,
                 ideascube_name, subject, related_items):

        self._id = pkg_id
        self._slug = slug
        self._description = description
        self._goal = goal
        self._language = language
        self._last_exportable_modification = str(last_exportable_modification)
        self._ideascube_name = ideascube_name
        self._subject = subject

        self._items = related_items

        self._txt_path = IDC_DEPLOY_CONFIG["omeka_json_destination"] + "/" + str(self._id) + "_" + \
            self._slug + ".txt"
        self._json_path = IDC_DEPLOY_CONFIG["omeka_json_destination"] + "/" + str(self._id) + "_" + \
            self._slug + ".json"

    @property
    def id(self):
        """`int`: Id of the omeka package.
        """
        return self._id

    @property
    def slug(self):
        """`str`: Omeka package slug.
        """
        return self._slug

    @property
    def description(self):
        """`str`: Omeka package description.
        """
        return self._description

    @property
    def goal(self):
        """`str`: Omeka package goal.
        """
        return self._goal

    @property
    def language(self):
        """`str`: Omeka package language.
        """
        return self._language

    @property
    def last_exportable_modification(self):
        """`str`: Omeka package last exportable modification date.
        """
        return self._last_exportable_modification

    @property
    def ideascube_name(self):
        """`str`: Ideascube name.
        """
        return self._ideascube_name

    @property
    def subject(self):
        """`str`: Subject/Category package.
        """
        return self._subject

    @property
    def items(self):
        """`OmekaPackageItems`: Items in package.
        """
        return self._items

    @property
    def txt_path(self):
        """`str`: Path of the .txt file in device (needed to create the package).
        """
        return self._txt_path

    @property
    def json_path(self):
        """`str`: Path of the .json file in device (needed to create the package).
        """
        return self._json_path

    def update_needed(self, catalog):
        """If the package version needs an update in given catalog compared to Omeka last_exportable_modification value.

        Parameters
        ----------
        catalog : `Catalog`
            Catalog to check if the package needs an update.

        Returns
        -------
        need_update : `bool`
            True if the package needs an update in ``catalog``. False in other case.
        """
        pkg_version = catalog.get_package_version(self._slug)
        return pkg_version < datetime.strptime(self._last_exportable_modification, "%Y-%m-%d %H:%M:%S")

    def to_dict(self):
        """Get the `OmekaPackage` as dictionary.

        Returns
        -------
        dict : `dict`
            The `OmekaPackage` as dictionary with the following format:

            - ``'id'``: id of the package.
            - ``'slug'``: slug of the package.
            - ``'description'``: description of the package.
            - ``'goal'``: goal of the package.
            - ``'language'``: language of the package.
            - ``'last_exportable_modification'``: last exportable modification date of the package.
            - ``'ideascube_name'``: name of the ideascube of the package.
            - ``'items'``: dictionary of `OmekaPackageItems` inside package.

        See Also
        --------
        models.producers.utils.omeka_package_item.OmekaPackageItems
        """
        return {
            'id': self._id,
            'slug': self._slug,
            'description': self._description,
            'goal': self._goal,
            'language': self._language,
            'last_exportable_modification': self._last_exportable_modification,
            'ideascube_name': self.	_ideascube_name,
            'subject': self._subject,
            'items': self._items.to_dict()
        }
