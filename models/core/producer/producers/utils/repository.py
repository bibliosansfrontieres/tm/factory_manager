# -*- coding: utf-8 -*-

import os
from git import Repo

from settings import GIT_CONFIG


class Repository:

    def __init__(self, path, branch):
        os.chdir(path)

        self._repo = Repo(path)

        self._repo.git.checkout(branch)
        self._repo.git.pull()

    def commit_push_catalog(self, project_name, logger):
        header = self._repo.head.commit

        if header.diff(None):
            logger.debug("{} : Catalog has been modified. Pushing to Github".format(project_name))

            self._repo.git.add(GIT_CONFIG['omeka_catalog_relative_path'])
            self._repo.git.commit(m="Updating package list for device {}".format(project_name))

            self._repo.git.push()
        else:
            logger.debug("{} : Catalog has not been modified. Doing nothing more.".format(project_name))

    def commit_push_device_list(self, project_name, logger):
        header = self._repo.head.commit

        if header.diff(None):
            logger.debug("{} : Device list has been modified. Pushing to Github.".format(project_name))

            self._repo.git.add(GIT_CONFIG['device_list_relative_path'])
            self._repo.git.commit(m="Updating configuration for device {}".format(project_name))

            self._repo.git.push()
        else:
            logger.debug("{} : Device list has not changed. Doing nothing.".format(project_name))
