from settings import OLIP
from os import stat
from datetime import datetime


def convert_str_to_epoch(d):
    """
    We are waiting here for a "%Y-%m-%d %H:%M:%S" datetime object converted to string.
    :params d: "%Y-%m-%d %H:%M:%S" date format.
    :type d: `str`
    :return: `str` Epoch version of d
    """
    return str(
        datetime.timestamp(
            datetime.strptime(d, "%Y-%m-%d %H:%M:%S")
        )
    ).split('.')[0]


def make_endpoint_url(pkg):
    return "/" + pkg.slug + "/"


def make_download_path(pkg):
    return OLIP['base_package_url'] + "/" + pkg.slug + ".zip#unzip"


def make_destination_path(pkg):
    return pkg.slug + "/"


def get_package_size(zip_path):
    return str(stat(zip_path).st_size)
