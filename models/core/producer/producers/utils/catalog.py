# -*- coding: utf-8 -*-

import os
import yaml
import requests
from datetime import datetime


class Catalog:

    def __init__(self, path, url=None):

        try:
            if url is None:     # load from path
                with open(path, "r", encoding='utf-8') as file:
                    self._data = yaml.safe_load(file.read())

            else:               # load from url and store in path
                r = requests.get(url)
                if r.status_code != requests.codes.ok:
                    raise Exception("Failed request to {}. Status code: {}".format(url, r.status_code))

                self._data = yaml.load(r.text.encode('utf-8'))['all']
                with open(path, "w", encoding='utf-8') as file:
                    yaml.safe_dump(self._data, file, default_flow_style=False, encoding='utf-8')

        except Exception as e:
            raise Exception("Cannot create the catalog : {}".format(e))

    def get_package_version(self, pkg_slug):
        try:
            return datetime.strptime(self._data[pkg_slug]['version'], "%Y-%m-%d")
        except KeyError:  # package is not in catalog.
            return datetime.strptime('1970-01-01', "%Y-%m-%d")

    def store_catalog(self, path, project_name, logger):
        try:
            with open(path, 'w') as file:
                logger.info("{} : Writing new Omeka catalog to {}".format(project_name, path))
                full_catalog = {"all": self._data}

                yaml.safe_dump(full_catalog, file, default_flow_style=False, encoding='utf-8')

        except IOError:
            raise Exception("Cannot open file {}.".format(path))
        except Exception:
            raise Exception("Yaml parsing failed.")

    def update_catalog(self, path, project_name, logger):

        for pkg_slug in os.listdir(path):
            logger.debug("{} : Loading new metadata for package {}.".format(project_name, pkg_slug))

            pkg_path = path + pkg_slug
            try:
                with open(pkg_path, 'r', encoding='utf-8') as f:
                    yaml_content = yaml.safe_load(f.read())
                    self._data[pkg_slug] = yaml.safe_load(yaml_content)['all'][pkg_slug]

            except IOError:
                raise Exception("Cannot open file {}.".format(pkg_path))
            except Exception:
                raise Exception("Impossible to load file {}, invalid yaml format.".format(pkg_path))
