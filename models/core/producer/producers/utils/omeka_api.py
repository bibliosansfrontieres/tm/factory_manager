# -*- coding: utf-8 -*-

import requests
from os import path

from models.core.producer.producers.utils.omeka_package import OmekaPackage
from models.core.producer.producers.utils.omeka_packages import OmekaPackages
from models.core.producer.producers.utils.omeka_package_item import OmekaPackageItem
from models.core.producer.producers.utils.omeka_package_items import OmekaPackageItems
from models.core.producer.producers.utils.relation_item import RelationItem, RelationItems
from settings import OMEKA_API_URL, OMEKA_BSF_PROJECT_TYPE_ID, OMEKA_API_KEY, \
     DUBLIN_CORE_ELEMENTS


class OmekaApi:
    """Class to interact with the Omeka Api.
    """

    OMEKA_ITEMS_URL = OMEKA_API_URL + "items/"
    OMEKA_FILE_URL = OMEKA_API_URL + "files?item="
    OMEKA_PACKAGES_URL = OMEKA_API_URL + "packages/"
    OMEKA_PACKAGE_RELATIONS_URL = OMEKA_API_URL + "package_relations/"
    OMEKA_ITEM_RELATIONS_URL = OMEKA_API_URL + "item_relations/"

    @staticmethod
    def _get_metadata_by_id(item_element_texts, element_id):
        try:
            for elem in item_element_texts:
                if elem['element']['id'] == element_id:
                    return elem.get('text')
        except KeyError:
            pass

        return None

    @staticmethod
    def _get_item_title(item_element_texts):
        return OmekaApi._get_metadata_by_id(item_element_texts, DUBLIN_CORE_ELEMENTS['title'])

    @staticmethod
    def _get_item_summary(item_element_texts):
        return OmekaApi._get_metadata_by_id(item_element_texts, DUBLIN_CORE_ELEMENTS['summary'])

    @staticmethod
    def _get_item_language(item_element_texts):
        return OmekaApi._get_metadata_by_id(item_element_texts, DUBLIN_CORE_ELEMENTS['language'])

    @staticmethod
    def _get_item_credits(item_element_texts):
        return OmekaApi._get_metadata_by_id(item_element_texts, DUBLIN_CORE_ELEMENTS['credits'])

    @staticmethod
    def _get_item_file_url(file_data):
        return file_data[0]['file_urls']['original']

    @staticmethod
    def _get_item_thumbnail_url(file_data):
        return file_data[0]['file_urls']['fullsize']

    @staticmethod
    def _get_item_filename(file_data):
        return file_data[0]['filename']

    @staticmethod
    def _get_mime_type(file_data):
        return file_data[0]['mime_type']

    @staticmethod
    def _get_item_path(file_data, item_id):
        """
        This metadata is only used for build package over NFS.
        """
        try:
            filename = file_data[0]['filename']
        except KeyError as e:
            raise KeyError('Error getting file information for item {} : {}'.format(item_id, e))
        except IndexError:
            return ""  # means no media for item.

        return "files/original/" + filename

    @staticmethod
    def _get_item_preview(file_data):
        """
        Checks if an URL exists for fullsize thumbnail,
        if so returns relative path "files/fullsize/" + filename (extension replaced by ".jpg")
        if not, empty string.
        This is used for over NFS builded packages.
        """
        try:
            return "" if file_data[0]['file_urls']['fullsize'] is None \
                else "files/fullsize/" + path.splitext(file_data[0]['filename'])[0] + ".jpg"
        except IndexError:
            return ""

    @staticmethod
    def _get_item_device(item_element_texts):
        return OmekaApi._get_metadata_by_id(item_element_texts, DUBLIN_CORE_ELEMENTS['device'])

    @staticmethod
    def _get_item_collection(item_data, s):
        collection = item_data.get('collection')
        if collection:
            collection_url = collection.get('url')

            r = s.get(collection_url)
            if r.status_code != requests.codes.ok:
                raise Exception("Cannot get collection from Api (status_code: {})".format(r.status_code))
            collection_data = r.json()
            collection_element_texts = collection_data.get('element_texts')

            return OmekaApi._get_item_title(collection_element_texts)       # collections is an item

        return None

    @staticmethod
    def _get_item_tags(item_data):
        tmp = []
        for elem in item_data.get('tags'):
            tmp.append(elem.get('name'))
        tags = ",".join(tmp)

        return tags

    # PROJECT #

    @staticmethod
    def update_project(project):
        """Update the given project information with the title, description, language, device.

        Parameters
        ----------
        project : `OmekaProject`
            Omeka project to update.

        Raises
        ------
        Exception
            Raised if the request to the omeka api failed.
        Exception
            Raised if the status code of the request is not a valid status code.
        TypeError
            Raised if the item obtained by the request is not a project.
        """
        url = OmekaApi.OMEKA_ITEMS_URL + str(project.id)
        try:
            r = requests.get(url, params={"key": OMEKA_API_KEY})
            if r.status_code != requests.codes.ok:
                raise Exception("Failed request to get project.")

            item_data = r.json()
            item_type = item_data['item_type']['id']

            if item_type != OMEKA_BSF_PROJECT_TYPE_ID:
                raise TypeError("Project {} is not a valid project.".format(project.id))

            item_element_texts = item_data.get('element_texts')

            project.title = OmekaApi._get_item_title(item_element_texts)
            project.description = OmekaApi._get_item_summary(item_element_texts)
            project.language = OmekaApi._get_item_language(item_element_texts)
            project.device = OmekaApi._get_item_device(item_element_texts)

        except Exception as e:
            raise Exception("Cannot get project {} : {}".format(project.id, e))

    # PACKAGES #

    @staticmethod
    def get_package_by_id(pkg_id, s):
        """Get a package by given id from the Omeka Api.

        Parameters
        ----------
        pkg_id : `int`
            Id of the package to get from Api.

        Returns
        -------
        pkg : `OmekaPackage`
            Package with given ``pkg_id`` id with it's respective information from Omeka.

        Raises
        ------
        Exception
            Raised if the status code of the request is not a valid status code.
        """
        url = OmekaApi.OMEKA_PACKAGES_URL + str(pkg_id)

        r = s.get(url, params={"key": OMEKA_API_KEY})
        if r.status_code != requests.codes.ok:
            raise Exception("Cannot get packages from Api (status_code: {})".format(r.status_code))
        pkg_data = r.json()

        slug = pkg_data.get('slug')
        description = pkg_data.get('description')
        goal = pkg_data.get('goal')
        language = pkg_data.get('language')
        last_exportable_modification = pkg_data.get('last_exportable_modification')
        ideascube_name = pkg_data.get('ideascube_name')
        subject = pkg_data.get('subject')

        contents = pkg_data.get('contents')
        related_items = OmekaApi.get_items(contents, s)

        return OmekaPackage(pkg_id, slug, description, goal, language, last_exportable_modification,
                            ideascube_name, subject, related_items)

    @staticmethod
    def get_packages_by_project_id(project_id):
        """Get all the packages in a project by given project id.

        Parameters
        ----------
        project_id : `int`
            Id of the project to get packages.

        Returns
        -------
        omeka_packages : `OmekaPackages`
            Collection of `OmekaPackage` in the project with the given id.
        """
        packages_list = []
        page = 1

        s = requests.Session()
        while True:
            r = s.get(OmekaApi.OMEKA_PACKAGE_RELATIONS_URL,
                      params={"key": OMEKA_API_KEY, "item_id": project_id, "page": page})
            if r.status_code != requests.codes.ok:
                raise Exception("Cannot get packages relations from Omeka Api (status_code: {})"
                                .format(r.status_code))

            relations = r.json()
            for relation in relations:
                pkg_id = relation.get('package_id')

                new_pkg = OmekaApi.get_package_by_id(pkg_id, s)
                packages_list.append(new_pkg)

            if len(relations) < 50:
                break
            page += 1

        s.close()
        return OmekaPackages(packages_list)

    # PACKAGE ITEMS #

    @staticmethod
    def get_item_by_id(item_id, s):
        """Get one omeka item from the Omeka Api by given item id.

        Parameters
        ----------
        item_id : `int`
            Id of the tem to get.

        Returns
        -------
        item : `OmekaPackageItem`
            Item by given `item_id`.
        """
        url = OmekaApi.OMEKA_ITEMS_URL + str(item_id)
        fileurl = OmekaApi.OMEKA_FILE_URL + str(item_id)

        r = s.get(url, params={"key": OMEKA_API_KEY})
        if r.status_code != requests.codes.ok:
            raise Exception("Cannot get items from Api (status_code: {})".format(r.status_code))

        item_data = r.json()
        item_element_texts = item_data.get('element_texts')

        f = s.get(fileurl, params={"key": OMEKA_API_KEY})
        if f.status_code != requests.codes.ok:
            raise Exception("Cannot get files for item from Api (status_code: {}).".format(f.status_code))

        file_data = f.json()

        title = OmekaApi._get_item_title(item_element_texts)
        summary = OmekaApi._get_item_summary(item_element_texts)
        language = OmekaApi._get_item_language(item_element_texts)
        item_credits = OmekaApi._get_item_credits(item_element_texts)
        collection = OmekaApi._get_item_collection(item_data, s)
        tags = OmekaApi._get_item_tags(item_data)
        path = OmekaApi._get_item_path(file_data, item_id)
        try:
            file_url = OmekaApi._get_item_file_url(file_data)
        except Exception as e:
            raise Exception("Item {} has no file attached in Omeka ({}). Aborting.".format(item_id, e))
        thumbnail_url = OmekaApi._get_item_thumbnail_url(file_data)
        filename = OmekaApi._get_item_filename(file_data)
        preview = OmekaApi._get_item_preview(file_data)
        mime_type = OmekaApi._get_mime_type(file_data)

        return OmekaPackageItem(item_id, title, summary, language, item_credits, collection, path, tags,
                                filename, file_url, thumbnail_url, mime_type, preview=preview)

    @staticmethod
    def get_items(contents, s):
        items_list = []

        for elem in contents:
            item_id = elem.get('item_id')

            new_item = OmekaApi.get_item_by_id(item_id, s)
            items_list.append(new_item)

        return OmekaPackageItems(items_list)

    # ITEMS RELATIONS #
    @staticmethod
    def get_relation_item_by_id(item_id):
        url = OmekaApi.OMEKA_ITEMS_URL + str(item_id)

        r = requests.get(url, params={"key": OMEKA_API_KEY})
        if r.status_code == requests.codes.ok:
            item_data = r.json()
            item_element_texts = item_data.get('element_texts')

            item_type = item_data['item_type']['id']
            title = OmekaApi._get_item_title(item_element_texts)
            language = OmekaApi._get_item_language(item_element_texts)

            return RelationItem(item_id, item_type, title, language)

        return None

    @staticmethod
    def get_relations_items_by_project_id(project_id):
        """Get all the relations items from project by project id.

        Parameters
        ----------
        project_id : `int`
            Id of the project.

        Returns
        -------
        relation_items : `RelationItems`
            Get all the relations items for given project.
        """
        items_list = []
        page = 1

        while True:
            r = requests.get(OmekaApi.OMEKA_ITEM_RELATIONS_URL,
                             params={"key": OMEKA_API_KEY, "subject_item_id": project_id, "page": page})
            if r.status_code != requests.codes.ok:
                raise Exception("Cannot get item relations from Omeka Api (status_code: {})"
                                .format(r.status_code))

            relations = r.json()
            for relation in relations:
                item_id = relation.get('object_item_id')

                new_item = OmekaApi.get_relation_item_by_id(item_id)
                if new_item is not None:
                    items_list.append(new_item)

            if len(relations) < 50:
                break
            page += 1

        return RelationItems(items_list)
