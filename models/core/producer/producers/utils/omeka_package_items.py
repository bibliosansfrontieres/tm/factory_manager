from .omeka_package_item import OmekaPackageItem


class OmekaPackageItems(list):
    """Collection of `OmekaPackageItem` instances.

    Parameters
    ----------
    items_list : `list` of `OmekaPackageItem`, optional
        Content of the `OmekaPackageItems` (empty list, by default).

    Raises
    ------
    TypeError
        Raised if any arguments for the `OmekaPackageItems` is not the expected type.
    """

    def __init__(self, items_list=None):
        if items_list and not all(isinstance(item, OmekaPackageItem) for item in items_list):
            raise TypeError("Invalid arguments type for OmekaPackageItems.")

        super().__init__(items_list)

        self._items_list = [] if (items_list is None) else items_list

    def to_dict(self):
        """Get the `OmekaPackageItems` as list.

        Returns
        -------
        list : `list`
            The `OmekaPackageItems` as list; with each item in dictionary format.
        """
        tmp = []
        for item in self._items_list:
            tmp.append(item.to_dict())
        return tmp
