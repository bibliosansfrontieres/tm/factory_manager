# -*- coding: utf-8 -*-

import requests
from .device_list import DeviceConfig

KEYWORD_RELATION_ITEMS = 'relation_items'


class RelationItem:
    """Relation item obtained from omeka. With the information required.

    Parameters
    ----------
    item_id : `int`
        Id of the item.
    item_type : `str`
        Slug of the item.
    title : `str`
        Title of the item.
    language : `str`
        Language of the item.
    """

    def __init__(self, item_id, item_type, title, language):

        self._id = item_id
        self._type = item_type
        self._title = title
        self._language = language

    @property
    def id(self):
        """`int`: Id of the item.
        """
        return self._id

    @property
    def type(self):
        """`str`: Type of the item.
        """
        return self._type

    @property
    def title(self):
        """`str`: Title of the item.
        """
        return self._title

    @property
    def language(self):
        """`str`: Language of the item.
        """
        return self._language

    def get_errors(self, catalog):
        """Get a list with the errors of the item.

        Returns
        -------
        errors : `list` of `str`
            List of errors in item.
        """
        errors = []

        try:
            r = requests.head(catalog[self._title]['url'])
            if r.status_code != requests.codes.ok:
                errors.append("Request {} failed (status_code: {})."
                              .format(catalog[self._title]['url'], r.status_code))
        except KeyError:
            errors.append("Item '{}' is absent from catalog.".format(self._title))

        return errors

    def to_dict(self):
        """Get the `RelationItem` as dictionary.

        Returns
        -------
        dict : `dict`
            The `RelationItem` as dictionary with the following format:

            - ``'id'``: id of the item relation.
            - ``'type'``: type of the item relation.
            - ``'title'``: title of the item relation.
            - ``'lang'``: language of the item relation.
        """
        return {
            'id': self._id,
            'type': self._type,
            'title': self._title,
            'lang': self._language
        }


class RelationItems(list):
    """Collection of `RelationItem` instances.

    Parameters
    ----------
    items_list : `list` of `RelationItem`, optional
        Content of the `RelationItems` (empty list, by default).

    Raises
    ------
    TypeError
        Raised if any arguments for the `RelationItems` is not the expected type.
    """

    def __init__(self, items_list=None):
        if items_list and not all(isinstance(pkg, RelationItem) for pkg in items_list):
            raise TypeError("Invalid arguments type for RelationItems.")
        super().__init__(items_list)

        self._items_list = [] if items_list is None else items_list

    def count(self):
        """Amount of items inside the collection.

        Returns
        -------
        len : `int`
            Amount of item relations.
        """
        return len(self._items_list)

    def to_dict(self):
        """Get the collection of item relations as list.

        Returns
        -------
        list : `list`
            The `RelationItems` as list; with each relation item in dictionary format.
        """
        tmp = []

        for elem in self._items_list:
            tmp.append(elem.to_dict())
        return tmp


class MsgRelationItemsDAO:
    """Data Access Object to manage a `RelationItems` instances in `Message` objects.
    """

    @staticmethod
    def get_items(msg):
        """Get a `RelationItems` instance from given `Message`.

        Parameters
        ----------
        msg : `Message`
            Message content to parse into `RelationItems`.

        Returns
        -------
        relation_items : `RelationItems`
            The `RelationItems` result from parsing the given ``msg``.

        Raises
        ------
        Exception
            Raised if ``msg`` does not contains the correct information for each relation item.
        """
        items_list = []
        items_data = msg.get_value(KEYWORD_RELATION_ITEMS)

        for elem in items_data:
            # checking format for Item
            if not all(key in elem.keys() for key in ['id', 'type', 'title', 'lang']):
                raise Exception("The message does not contains the correct format for an Item.")

            item_id = elem.get('id')
            item_type = elem.get('type')
            title = elem.get('title')
            language = elem.get('lang')

            new_item = RelationItem(item_id, item_type, title, language)
            items_list.append(new_item)

        return RelationItems(items_list)

    @staticmethod
    def get_kolibri(msg):
        """Get a `KolibriDevice` instance from given `Message`.

        Parameters
        ----------
        msg : `Message`
            Message content to parse into `RelationItems`.

        Returns
        -------
        kolibri_device : `KolibriDevice`
        """
        device_config = DeviceConfig()
        device_config.activate_kolibri_items(MsgRelationItemsDAO.get_items(msg))

        return device_config._kolibri_device

    @staticmethod
    def update_items(msg, relation_items):
        """Update in the given `Message` the `RelationItems`.

        Parameters
        ----------
        msg : `Message`
            Message to update.
        relation_items : `RelationItems`
            Content to update in ``msg`` in ``KEYWORD_RELATION_ITEMS`` key.
        """
        msg.set_value(KEYWORD_RELATION_ITEMS, relation_items.to_dict())
