# -*- coding: utf-8 -*-

import json
import requests
import tempfile
from datetime import datetime

from models.core.marshmallow_schemas import DescriptorRootSchema, ContentSchema
from .olip_catalog_helper import convert_str_to_epoch, \
    make_endpoint_url, \
    make_download_path, \
    make_destination_path, \
    get_package_size
from settings import OLIP


class Catalog:

    def __init__(self, paths=None, urls=None):
        """
        :param paths: List of absolute path to local files.
        :param urls: List URLs to descriptor.json.
        :type paths: `list`
        :type urls: `list` of `tuple`
        """

        self.paths = paths
        self.urls = urls
        self._catalog = list()

        try:
            if not(urls):     # load from path
                for descriptor in paths:
                    with open(descriptor, "r", encoding='utf-8') as file:
                        self._catalog.append(DescriptorRootSchema().load(json.load(file)))

            else:             # load from url
                for url in urls:
                    r = requests.get(url)
                    if r.status_code != requests.codes.ok:
                        raise Exception("Failed request to {}. Status code: {}".format(url, r.status_code))

                    self._catalog.append(
                        (
                            url,
                            DescriptorRootSchema().load(r.json())
                        )
                    )

        except Exception as e:
            raise Exception("Cannot create some catalogs : {}".format(e))

    def get_package_version(self, pkg_slug):
        """
        Parse whole descriptor file (named catalog by convention) looking for a package slug.
        returns its version if found, or epoch(0).

        :param pkg_slug: content_id of a package in the descriptor file/catalog.
        :type pkg_slug: str
        :return: `datetime` object
        """
        # We make the assumption that all catalogs have same content (only arch differs)
        for app in self._catalog[0][1].applications:
            if app.name == OLIP['mediacenter']['name']:  # app = mediacenter
                try:
                    version = [i.version for i in app.contents if i.content_id == pkg_slug]
                    if len(version) == 0:  # package missing from descriptor.
                        raise KeyError
                    return datetime.fromtimestamp(int(version[0]))
                except (KeyError, ValueError):  # package is not in catalog or has bad format.
                    return datetime.fromtimestamp(0)

    def store(self, work_dir):
        """
        Pairing new catalog temp filename with URL, storing this to builded_catalogs.json for reuse in another factory.
        :param work_dir: Where to store this file.
        :type work_dir: `str`
        :return: Path to new catalogs database in JSON as :
        {
            'url': 'http://olip.ideascube.org/amd64/descriptor.json',
            'filename': '/tmp/idc-bsf-myproject/tmp3gazacvre'
        }
        :rtype: `str`
        """
        pairs = list()

        for i, catalog in enumerate(self._catalog):
            f = tempfile.NamedTemporaryFile(dir=work_dir).name

            with open(f, 'w') as fh:
                fh.write(DescriptorRootSchema().dumps(catalog[1], indent=4))

            pairs.append({
                'url': self.urls[i],
                'filename': f
            })

        # writing pairs of (url, temp catalog file) to disk for further work.
        with open(work_dir + "/builded_catalogs.json", 'w') as f:
            json.dump(pairs, f)

        return work_dir + "/builded_catalogs.json"

    def add(self, pkg, zip_path, logger):
        """
        Update catalogs with metadata of the newly created package.

        :param pkg: Package object to update.
        :param zip_path: Absolute path of the zip package file.
        :type pkg: `OmekaPackage`.
        :type zip_path: `str`
        """
        logger.debug("Editing the catalog for package {}...".format(pkg.slug))
        # Sorry in advance for this/these loop/s. A database would be nice I agree. Why don't you install one? :)
        for catalog in self._catalog:
            for app in catalog[1].applications:
                if app.name == OLIP['mediacenter']['name']:
                    item_index = next(
                        (
                            index for (index, d) in enumerate(app.contents) if d.content_id == pkg.slug
                        ),
                        None)
                    if item_index:  # item exists in app.
                        logger.debug("Package {} exists in catalog, updating..."
                            .format(pkg.slug))
                        app.contents[item_index].version = convert_str_to_epoch(pkg.last_exportable_modification)
                        app.contents[item_index].size = get_package_size(zip_path)
                        app.contents[item_index].endpoint.url = make_endpoint_url(pkg)
                        app.contents[item_index].endpoint.container = app.containers[0].name
                        app.contents[item_index].endpoint.name = pkg.ideascube_name
                        app.contents[item_index].name = pkg.ideascube_name
                        app.contents[item_index].download_path = make_download_path(pkg)
                        app.contents[item_index].destination_path = make_destination_path(pkg)
                        app.contents[item_index].description = pkg.description
                        app.contents[item_index].language = pkg.language
                        app.contents[item_index].subject = pkg.subject
                    else:  # does not exist in app. Add it.
                        logger.debug("Package {} does NOT exist in catalog, adding..."
                            .format(pkg.slug))
                        app.contents.append(
                            ContentSchema().load(
                                dict(
                                    content_id=pkg.slug,
                                    version=convert_str_to_epoch(pkg.last_exportable_modification),
                                    size=get_package_size(zip_path),
                                    endpoint=dict(
                                        url=make_endpoint_url(pkg),
                                        container=app.containers[0].name,
                                        name=pkg.ideascube_name
                                    ),
                                    name=pkg.ideascube_name,
                                    download_path=make_download_path(pkg),
                                    destination_path=make_destination_path(pkg),
                                    description=pkg.description,
                                    language=pkg.language,
                                    subject=pkg.subject
                                )
                            )
                        )
                    break
