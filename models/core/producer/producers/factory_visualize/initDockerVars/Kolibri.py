from models.core.producer.producers.utils import MsgRelationItemsDAO
from models.core.AbstractDockerVars import AbstractDockerVars
from settings import DOCKER_VARS


class InitDockerVarsKolibri(AbstractDockerVars):
    def __init__(self, msg):
        self._msg = msg

    @property
    def image(self):
        return DOCKER_VARS['kolibri_image']

    @property
    def volumes(self):
        return {
                    DOCKER_VARS['KOLIBRI_SOURCE']:
                    {
                        'bind': '/mnt/kolibri',
                        'mode': 'ro'
                    }
                }

    @property
    def name(self):
        return "kolibri." + self._msg.process.project.name + "." + self._msg.process.id + ".ideascube"

    @property
    def environment(self):
        langlist = MsgRelationItemsDAO.get_kolibri(self._msg)
        return ["LANGUAGES={}".format(langlist.language_tostr),
                "MOUNTPOINT={}".format(DOCKER_VARS["KOLIBRI_SOURCE"]),
                "LANG=C.UTF-8"]

    @property
    def privileged(self):
        return True

    @property
    def labels(self):
        KOLIBRI_HOSTNAME = self.name + "." + DOCKER_VARS['DOCKER_DOMAIN']
        labels = dict()
        labels['traefik.kolibri.frontend.rule'] = 'Host:{}'.format(KOLIBRI_HOSTNAME)
        return labels
