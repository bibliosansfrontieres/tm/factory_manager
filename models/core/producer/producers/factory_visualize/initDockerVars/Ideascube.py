from .IdeascubeProd import InitDockerVarsIdeascubeProd
from .IdeascubeDemo import InitDockerVarsIdeascubeDemo


def InitDockerVarsIdeascube(msg):
    if msg.process.config.deployable:
        return InitDockerVarsIdeascubeProd(msg)
    else:
        return InitDockerVarsIdeascubeDemo(msg)
