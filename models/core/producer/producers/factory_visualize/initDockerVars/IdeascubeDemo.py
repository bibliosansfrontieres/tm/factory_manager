from .IdeascubeProd import InitDockerVarsIdeascubeProd
from settings import DOCKER_VARS


class InitDockerVarsIdeascubeDemo(InitDockerVarsIdeascubeProd):
    def __init__(self, msg):
        if msg.process.config.deployable:
            raise Exception("InitDockerVarsIdeascubeDemo : process is deployable! And shouldn't.")
        self._msg = msg

    @property
    def volumes(self):
        var_idc_vol = DOCKER_VARS['SHORTERM_VOLUME_STORAGE'] + self._msg.process.id + "_var_ideascube"
        var_cache_idc_vol = DOCKER_VARS['SHORTERM_VOLUME_STORAGE'] + self._msg.process.id + "_var_cache_ideascube"
        return {
                    DOCKER_VARS['KIWIX_SOURCE']:
                    {
                        'bind': '/mnt/kiwix',
                        'mode': 'ro'
                    },
                    var_idc_vol:
                    {
                        'bind': '/var/ideascube',
                        'mode': 'rw'
                    },
                    var_cache_idc_vol:
                    {
                        'bind': '/var/cache/ideascube',
                        'mode': 'rw'
                    }
                  }
