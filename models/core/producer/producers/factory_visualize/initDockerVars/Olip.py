from models.core.AbstractDockerVars import AbstractDockerVars
from settings import DOCKER_VARS, OLIP


class InitDockerVarsOlip(AbstractDockerVars):
    def __init__(self, msg):
        self._msg = msg
        self.full_project_name = self._msg.process.project.name + "." + self._msg.process.id + ".olip"
        self.hostname = self.full_project_name + "." + DOCKER_VARS["DOCKER_DOMAIN"]

    @property
    def image(self):
        return DOCKER_VARS['olip_image']

    @property
    def volumes(self):
        return {
                    "/var/run/docker.sock":
                    {
                        'bind': '/var/run/balena-engine.sock',
                        'mode': 'rw'
                    }
                }

    @property
    def name(self):
        return self.full_project_name + ".builder"

    @property
    def environment(self):
        return ["end_user_server_name={}".format(self.full_project_name),
                "end_user_domain_name={}".format(self.hostname),
                # We look into settings.py for the first variable containing descriptor.json for amd64
                "end_user_olip_file_descriptor={}".format(OLIP["catalogs"][0]).replace('descriptor.json', ''),
                "branch={}".format(OLIP['olip-deploy-branch']),
                "owner={}".format(self._msg.process.user.username),
                "HOST_DATA_DIR={}".format(DOCKER_VARS["LONGTERM_VOLUME_STORAGE"] + self._msg.process.id),
                "EXTRA_HOSTS={}".format(OLIP["extra_hosts"]),
                "APPLICATIONS_PORT_RANGE={}".format(self._msg.process.config.port_range),
                "PORT_RANGE_ID={}".format(self._msg.process.config.port_range_id),
                "UNIQ_ID={}".format(self._msg.process.id),
                "OLIP_API_URL={}".format('api.' + self.hostname),
                "LANG=C.UTF-8"]

    @property
    def announce_container(self):
        return {
            "announce": True,
            "image_url": self._msg.process.image_url
            }

    @property
    def labels(self):
        OLIP_HOSTNAME = self.name + "." + DOCKER_VARS['DOCKER_DOMAIN']
        labels = dict()
        labels['traefik.olip.frontend.rule'] = 'Host:{}'.format(OLIP_HOSTNAME)
        labels['traefik.frontend.errors.net.status'] = '502'
        labels['traefik.frontend.errors.net.backend'] = 'errors'
        labels['traefik.frontend.errors.net.query'] = '/502.html'
        labels['owner'] = self._msg.process.user.username
        labels['port_range_id'] = str(self._msg.process.config.port_range_id)
        return labels

    @property
    def remove(self):
        return False
