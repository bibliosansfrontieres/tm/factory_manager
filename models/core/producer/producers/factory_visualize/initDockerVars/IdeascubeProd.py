from models.core.AbstractDockerVars import AbstractDockerVars
from settings import DOCKER_VARS, ENVIRON


class InitDockerVarsIdeascubeProd(AbstractDockerVars):
    def __init__(self, msg):
        if not msg.process.config.deployable:
            raise Exception("InitDockerVarsIdeascubeProd : process is not deployable!")
        self._msg = msg

    @property
    def image(self):
        return DOCKER_VARS['ideascube_image']

    @property
    def volumes(self):
        var_idc_vol = DOCKER_VARS['LONGTERM_VOLUME_STORAGE'] + self._msg.process.id + "_var_ideascube"
        var_cache_idc_vol = DOCKER_VARS['LONGTERM_VOLUME_STORAGE'] + self._msg.process.id + "_var_cache_ideascube"
        return {
                    DOCKER_VARS['KIWIX_SOURCE']:
                    {
                        'bind': '/mnt/kiwix',
                        'mode': 'ro'
                    },
                    var_idc_vol:
                    {
                        'bind': '/var/ideascube',
                        'mode': 'rw'
                    },
                    var_cache_idc_vol:
                    {
                        'bind': '/var/cache/ideascube',
                        'mode': 'rw'
                    }
                  }

    @property
    def name(self):
        return self._msg.process.project.name + "." + self._msg.process.id + ".ideascube"

    @property
    def environment(self):
        return ["PROJECT_NAME={}".format(self._msg.process.project.name),
                "DOCKER_DOMAIN={}".format(DOCKER_VARS["DOCKER_DOMAIN"]),
                "PROCESS_ID={}".format(self._msg.process.id),
                "ENVIRON={}".format(ENVIRON),
                "DEPLOYABLE={}".format(self._msg.process.config.deployable),
                "LANG=C.UTF-8"]

    @property
    def announce_container(self):
        return {
            "announce": True,
            "image_url": self._msg.process.image_url
            }

    @property
    def labels(self):
        IDC_HOSTNAME = self.name + "." + DOCKER_VARS['DOCKER_DOMAIN']
        KIWIX_HOSTNAME = "kiwix." + IDC_HOSTNAME
        SITE_HOSTNAME = "sites." + IDC_HOSTNAME
        labels = dict()
        labels['traefik.ideascube.frontend.rule'] = 'Host:{}'.format(IDC_HOSTNAME)
        labels['traefik.kiwix.frontend.rule'] = 'Host:{}'.format(KIWIX_HOSTNAME)
        labels['traefik.sites.frontend.rule'] = 'Host:{}'.format(SITE_HOSTNAME)
        labels['traefik.frontend.errors.net.status'] = '502'
        labels['traefik.frontend.errors.net.backend'] = 'errors'
        labels['traefik.frontend.errors.net.query'] = '/502.html'
        labels['owner'] = self._msg.process.user.username
        return labels

    @property
    def remove(self):
        return False
