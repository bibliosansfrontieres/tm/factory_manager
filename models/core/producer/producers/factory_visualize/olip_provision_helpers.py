
import asyncio
import aiohttp
import requests
import time
from urllib.parse import urlparse, parse_qs
from authlib.integrations.requests_client import OAuth2Session
from authlib.common.security import generate_token
from settings import EXTERNAL_PACKAGE_TYPE
from models.core.producer.producers.utils.timer import Timer


class appName:
    kiwix = "kiwix.app"
    mediacenter = "mediacenter.app"
    kolibri = "kolibri.app"
    nextcloud = "nextcloud.app"


class OlipProvision(object):
    def __init__(self, api_url, logger, relation_items, omeka_packages):
        self.headers = {"Content-Type": "application/x-www-form-urlencoded"}
        self.data = "username=admin&password=admin&confirm=true"

        dashboard_url = api_url.replace('5002', '8082').replace('api.', '')
        self.params = {
            'client_id': 'dashboard',
            'scope': 'openid',
            'redirect_uri': '{}/oidc-callback'.format(dashboard_url),
            'authorization_endpoint': '{}/oauth/authorize'.format(api_url)
        }

        self.response_type = "id_token token"
        self._installed_app = list()

        self.api_url = api_url
        self.logger = logger
        self.relation_items = relation_items
        self.omeka_packages = omeka_packages

    def wait_for_api(self, timeout=600):
        """
        Waits till API is up or timeout expires.
        """

        tic = time.perf_counter()
        time_counter = 0

        with Timer(logger=self.logger, msg="Waited for API"):
            while not (time_counter > timeout):
                time.sleep(10)
                time_counter = time.perf_counter() - tic

                try:
                    print('wait_for_api:', self.api_url)
                    r = requests.head(self.api_url)
                except ConnectionError:
                    self.logger.info('Failed connecting to API, retrying.')
                    continue

                if r.status_code != requests.codes.ok:
                    self.logger.info('API not ready yet, retrying.')
                else:
                    return

        raise TimeoutError('API did not answer in {} second. Exiting.'.format(timeout))

    def update_available_content(self, timeout=600):
        """
        Asks API to download descriptor.json and update its database.
        """
        tic = time.perf_counter()
        time_counter = 0

        with Timer(logger=self.logger, msg="Wait to update catalog in database"):
            while not (time_counter > timeout):
                time.sleep(10)
                time_counter = time.perf_counter() - tic

                try:
                    print('update_available_content:', self.api_url + '/applications/?visible=true&repository_update=true')
                    r = requests.head(self.api_url + '/applications/?visible=true&repository_update=true')
                except ConnectionError:
                    self.logger.info('Failed load catalog, retrying.')
                    continue

                if r.status_code != requests.codes.ok:
                    self.logger.info('Database not ready yet, retrying.')
                else:
                    requests.get(self.api_url + '/applications/?visible=true&repository_update=true')
                    return

        raise TimeoutError('Could not update database after {},'.format(timeout),
                           'descriptor.json is probably unreachable. Please call DNUM! Exiting.')

    def set_auth_header(self):
        """
        Gets an authorization token from API.
        Sets header for future requests.
        """
        client = OAuth2Session(
            self.params['client_id'],
            scope=self.params['scope'],
            redirect_uri=self.params['redirect_uri']
        )

        uri, _ = client.create_authorization_url(
            self.params['authorization_endpoint'],
            response_type=self.response_type,
            nonce=generate_token()
        )

        r = requests.post(uri, headers=self.headers, data=self.data, allow_redirects=False)

        parsed = urlparse(r.headers['Location'])
        token = parse_qs(parsed.fragment)['access_token'][0]

        self.auth_header = {
            'Authorization': 'Bearer ' + token
        }

    def _install_app(self, app_name):
        data = {
            "target_state": "installed"
        }

        try:
            r = requests.put(
                '{}/applications/{}/target-state'.format(self.api_url, app_name),
                data=data,
                headers=self.auth_header
            )
        except Exception:
            self.logger.error('Failed connecting to API to install {}'.format(app_name))

        if r.status_code != requests.codes.ok:
            self.logger.error('API returned HTTP error #{} "{}" while installing {}'.format(
                r.status_code,
                app_name,
                r.reason
            ))

        self._installed_app.append(app_name)

    def install_kolibri(self):
        """
        Install Kolibri app, soon will install language pack.
        """

        for item in self.relation_items:
            if item.type == EXTERNAL_PACKAGE_TYPE['khan_academy']:
                self._install_app(appName.kolibri)
                break  # We are not able to install language pack. Yet.

    def install_kiwix(self):
        """
        Install Kiwix app if some content expected. If no content, then exit.
        """

        for item in self.relation_items:  # First, install app.
            if item.type == EXTERNAL_PACKAGE_TYPE['kiwix_packages']:
                self._install_app(appName.kiwix)
                break

        if appName.kiwix in self._installed_app:
            for item in self.relation_items:
                if item.type == EXTERNAL_PACKAGE_TYPE['kiwix_packages']:
                    self._install_content(appName.kiwix, item.title)

    def install_mediacenter(self):

        if len(self.omeka_packages) == 0:
            return  # no packages.

        self._install_app(appName.mediacenter)

        for pkg in self.omeka_packages:
            self._install_content(appName.mediacenter, pkg.slug)

    def _install_content(self, app_name, content_name):
        """
        Send install_content order to API.
        """
        data = {
            "target_state": "installed"
        }

        try:
            r = requests.put(
                '{}/applications/{}/contents/{}/target-state'.format(
                    self.api_url,
                    app_name,
                    content_name
                ),
                data=data,
                headers=self.auth_header
            )
        except Exception as e:
            self.logger.error('Failed connecting to API to install {} : {}'.format(content_name, e))

        if r.status_code != requests.codes.ok:
            self.logger.error('API returned HTTP error #{} "{}" while installing {}'.format(
                r.status_code,
                content_name,
                r.reason
            ))

    async def _is_app_installed(self, app_name, sema, session):
        """
        Return True is app_name is installed.
        """
        async with sema, session.get(
            '{}/applications/?current_state=installed'.format(self.api_url),
            headers=self.auth_header
        ) as r:

            if r.status == 200:
                res = await r.json()

                try:
                    for app in res['data']:
                        if app['bundle'] == app_name:
                            return True
                except AttributeError:  # Probably res not a correct JSON
                    pass

        return False

    async def _check_app(self, sema):
        async with aiohttp.ClientSession() as session:
            while len(self._installed_app) > 0:
                for i, app in enumerate(self._installed_app):
                    if await self._is_app_installed(app, sema, session):
                        self.logger.info('{} successfully installed.'.format(app))
                        self._installed_app.pop(i)

                await asyncio.sleep(50)

        self.logger.info('Successfully installed all applications...')

    async def _is_relation_installed(self, content, data):
        """Only checking Kiwix packages as we are not installing Kolibri languages yet.
        :param content: A package to look for.
        :param data: The API response.
        :type content: `RelationItem` object.
        :type data: dict
        """

        for c in data:
            if c['content_id'] == content.title and c['current_state'] == 'installed':
                return True

        return False

    async def _check_kiwix_relations(self, sema):
        """Gets from API list of known kiwix packages, check their status until they are all installed."""

        kiwix_only_relations = [r for r in self.relation_items if r.type == EXTERNAL_PACKAGE_TYPE['kiwix_packages']]

        async with aiohttp.ClientSession() as session:
            while len(kiwix_only_relations) > 0:
                async with sema, session.get(
                    '{}/applications/{}/contents'.format(
                        self.api_url,
                        appName.kiwix
                    ),
                    headers=self.auth_header
                ) as r:

                    if r.status == 200:
                        res = await r.json()

                        try:
                            for i, content in enumerate(kiwix_only_relations):
                                if await self._is_relation_installed(content, res['data']):
                                    self.logger.info('{} successfully installed.'.format(content.title))
                                    kiwix_only_relations.pop(i)
                        except AttributeError:  # Probably res not a correct JSON
                            pass

                await asyncio.sleep(50)

        self.logger.info('Successfully installed all Kiwix packages...')

    async def _is_package_installed(self, pkg, data):
        """Checks if pkg.slug has current_status == installed."""

        for c in data:
            if c['content_id'] == pkg.slug and c['current_state'] == 'installed':
                return True

        return False

    async def _check_packages(self, sema):
        """Gets list of known packages, check their status until they are all installed.
        :param sema: The semaphore object which limits the number of concurrent requests."""

        async with aiohttp.ClientSession() as session:
            while len(self.omeka_packages) > 0:
                async with sema, session.get(
                    '{}/applications/{}/contents'.format(
                        self.api_url,
                        appName.mediacenter
                    ),
                    headers=self.auth_header
                ) as r:

                    if r.status == 200:
                        res = await r.json()

                        try:
                            for i, pkg in enumerate(self.omeka_packages):
                                if await self._is_package_installed(pkg, res['data']):
                                    self.logger.info('{} successfully installed.'.format(pkg.slug))
                                    self.omeka_packages.pop(i)
                        except AttributeError:  # Probably res not a correct JSON
                            pass

                await asyncio.sleep(50)

        self.logger.info('Successfully installed all Omeka packages...')

    async def _start_async_tasks(self, sema):
        app_task = asyncio.create_task(self._check_app(sema))
        relation_task = asyncio.create_task(self._check_kiwix_relations(sema))
        packages_task = asyncio.create_task(self._check_packages(sema))

        await app_task
        await relation_task
        await packages_task

    async def _set_tasks_timeout(self):
        """
        Timeout wrapper function.
        """
        timeout = 36000   # wait 10 hours.
        sema = asyncio.BoundedSemaphore(15)
        try:
            await asyncio.wait_for(self._start_async_tasks(sema), timeout=timeout)
        except asyncio.TimeoutError:
            raise TimeoutError('Visualize task cancelled: Timed-out after {} seconds.'.format(timeout))

    def callback_and_notify(self):
        """
        Will check all packages installation status.
        """
        self.logger.info('Installation orders done. Starting callbacks...')

        asyncio.run(self._set_tasks_timeout(), debug=True)
