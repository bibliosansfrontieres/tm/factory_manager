# -*- coding: utf-8 -*-
from models.core.producer.abstract_producer import AbstractProducer
from services.process.MongoProcessLogger import MongoProcessLogger
from repository.docker.DockerDAO import DockerDAO
from models.core.producer.producers.factory_visualize.initDockerVars.Ideascube import InitDockerVarsIdeascube
from models.core.producer.producers.factory_visualize.initDockerVars.Kolibri import InitDockerVarsKolibri
from settings import DOCKER_VARS


class ProducerIdeascube(AbstractProducer):

    def execute(self, msg, logger):

        if msg.process.config.engine != "ideascube":
            return

        logger.info('[{}] Starting Ideascube visualize.'.format(self.name))

        cont = DockerDAO(logger)

        msg.process.image_url = "http://{}.{}".format(InitDockerVarsIdeascube(msg).name, DOCKER_VARS['DOCKER_DOMAIN'])
        MongoProcessLogger().update_message(msg)

        cont.run(InitDockerVarsIdeascube(msg))
        cont.run(InitDockerVarsKolibri(msg))
