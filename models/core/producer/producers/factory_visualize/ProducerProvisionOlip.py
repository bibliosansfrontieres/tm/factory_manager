# -*- coding: utf-8 -*-

from models.core.producer.abstract_producer import AbstractProducer
from repository.omeka.MsgOmekaPackagesDAO import MsgOmekaPackagesDAO
from models.core.producer.producers.utils import MsgRelationItemsDAO
from services.notifications.slack import SlackNotification
from .olip_provision_helpers import OlipProvision


class ProducerProvisionOlip(AbstractProducer):
    """
    Connects to Olip API and start installation of all needed apps and content.
    As everything is async in Olip, we need a callback function to get installation status.
    """

    def execute(self, msg, logger):

        if msg.process.config.engine != "hugo":
            return

        omeka_packages = MsgOmekaPackagesDAO.get_packages(msg)
        relation_items = MsgRelationItemsDAO.get_items(msg)

        provision = OlipProvision(msg.process.config.api_url, logger, relation_items, omeka_packages)

        provision.wait_for_api()
        provision.set_auth_header()
        provision.update_available_content()

        provision.install_kolibri()
        provision.install_kiwix()
        provision.install_mediacenter()

        provision.callback_and_notify()

        # send direct notification to user.
        SlackNotification().send(
            channel='@' + msg.process.user.username,
            text=f"Hey, your VM has finished installing it's here : {msg.process.image_url}"
        )
