# -*- coding: utf-8 -*-
import os
from models.core.producer.abstract_producer import AbstractProducer
from services.process.MongoProcessLogger import MongoProcessLogger
from models.core.producer.producers.factory_visualize.initDockerVars.Olip import InitDockerVarsOlip
from repository.docker.DockerDAO import DockerDAO
from settings import DOCKER_VARS, OLIP


class ProducerOlip(AbstractProducer):

    def execute(self, msg, logger):

        if msg.process.config.engine != "hugo":
            return

        logger.info('[{}] Starting OLIP visualize.'.format(self.name))

        self.cont = DockerDAO(logger)

        logger.info('[{}] Recreating/Pushing OLIP-builder container.'.format(self.name))

        self.cont.build(
            dockerfile_path=os.path.abspath(os.getcwd()) + "/scripts/docker/olip/",
            tag=DOCKER_VARS['olip_image']
        )

        self.cont.push(DOCKER_VARS['olip_image'])

        msg.process.image_url = "http://{}".format(InitDockerVarsOlip(msg).hostname)
        MongoProcessLogger().update_message(msg)

        logger.info('[{}] Running Builder {}.'.format(self.name, DOCKER_VARS['olip_image']))

        msg.process.config.port_range, msg.process.config.port_range_id = self.get_free_port_range()
        olip_vars = InitDockerVarsOlip(msg)

        self.cont.run(olip_vars)

        msg.process.config.api_url = "http://api." + olip_vars.hostname + ":5002"

    def get_free_port_range(self):
        """
        Gets a free port range from DB.
        This value is then passed to olip-api container that will pop containers for each app and
        automatically associate a port to it from this range.
        """
        try:
            low_port, high_port = OLIP.get('port_range', '10000-20000').split('-')
        except Exception:
            low_port, high_port = 10000, 20000

        allocated_ports = OLIP.get('allocated_ports', 10)

        all_running_containers = self.cont.find_container_by_name(name='')

        # Make a list with all the port_range_id already in use found in olip-api containers labels.
        used_ranges = [
            int(a.labels.get('port_range_id')) for a in all_running_containers if a.labels.get('port_range_id')
        ]

        for i, start_port in enumerate(range(int(low_port), int(high_port), allocated_ports)):
            if i not in used_ranges:
                low_port = str(start_port)
                high_port = str(start_port + allocated_ports - 1)
                return low_port + '-' + high_port, i
