# -*- coding: utf-8 -*-

from models.core.producer.abstract_producer import AbstractProducer
from repository.omeka.MsgOmekaPackagesDAO import MsgOmekaPackagesDAO
from models.core.producer.producers.utils import MsgRelationItemsDAO, Repository, \
        FileDeviceListDAO, DeviceConfig, DevicePackages
from settings import GIT_CONFIG


class ProducerUpdateDeviceList(AbstractProducer):

    def execute(self, msg, logger):
        if msg.process.config.engine != "ideascube":
            return

        project = msg.process.project
        omeka_packages = MsgOmekaPackagesDAO.get_packages(msg)
        relation_items = MsgRelationItemsDAO.get_items(msg)
        logger.info("{} : Writing updated device list to disk".format(self.name))

        repo = Repository(GIT_CONFIG['ansiblecube_local_repo'], 'oneUpdateFile')    # pull

        path = GIT_CONFIG['ansiblecube_local_repo'] + GIT_CONFIG['device_list_relative_path']
        device_list = FileDeviceListDAO.get_device_list(path)       # get the Device list from file

        # config
        device_config = DeviceConfig()
        device_config.activate_kolibri_items(relation_items)         # activate kolibri items in device_conf

        # packages
        device_packages = DevicePackages()
        device_packages.add_omeka_packages(omeka_packages)          # add omeka packages
        device_packages.add_kiwix_packages(relation_items)          # add kiwix packages

        device_list.prepare_project(project.name)                   # clean data for specific project
        device_list.update_config(device_config, project.name)
        device_list.update_packages(device_packages, project.name)

        FileDeviceListDAO.update_device_list(path, device_list)     # update the Device list file

        repo.commit_push_device_list(project.name, logger)          # commit
