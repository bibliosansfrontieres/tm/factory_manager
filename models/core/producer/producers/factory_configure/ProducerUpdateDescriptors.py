# -*- coding: utf-8 -*-

import json

from models.core.producer.abstract_producer import AbstractProducer
from models.core.producer.producers.utils import utils
from settings import OLIP


class ProducerUpdateDescriptors(AbstractProducer):

    def execute(self, msg, logger):
        if msg.process.config.engine != "hugo":
            return

        catalogs = self.build_destinations_from_url(msg.process.catalog_path)

        utils.synchronize_descriptors(catalogs)

    def build_destinations_from_url(self, catalogs_path):
        """
        """
        with open(catalogs_path, 'r') as f:
            cat = json.load(f)

        for c in cat:  # given url is "http://drop.bsf-intranet.org/amd64/descriptor.json"
            host = c['url'].split('/')[3]  # host = "drop.bsf-intranet.org"
            path = c['url'].split(host)[-1]  # path = "/amd64/descriptor.json"
            c['rsync_dst'] = OLIP['catalog_storage'] + "/" + path

        return cat
