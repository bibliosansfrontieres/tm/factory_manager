# -*- coding: utf-8 -*-

from models.core.producer.abstract_producer import AbstractProducer
from models.core.producer.producers.utils import Repository, IdcCatalog, utils
from settings import GIT_CONFIG, BUILD_WORKSPACE


class ProducerUpdateCatalog(AbstractProducer):

    def execute(self, msg, logger):
        if msg.process.config.engine != "ideascube":
            return

        project = msg.process.project
        logger.info("{} : Updating catalog.".format(self.name))

        repo = Repository(GIT_CONFIG['catalog-io_local_repo'], 'master')

        yaml_path = BUILD_WORKSPACE + project.name + "/yaml/"

        catalog = IdcCatalog(msg.process.catalog_path)
        catalog.update_catalog(yaml_path, project.name, logger)

        catalog.store_catalog(msg.process.catalog_path, project.name, logger)  # Or race condition highly likely
        catalog.store_catalog(GIT_CONFIG['omeka_catalog_relative_path'], project.name, logger)

        repo.commit_push_catalog(project.name, logger)
        utils.push_to_bubble(logger)
