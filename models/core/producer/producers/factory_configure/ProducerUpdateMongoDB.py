# -*- coding: utf-8 -*-

from models.core.producer.abstract_producer import AbstractProducer
from repository.message.BuildCAPConfDAO import BuildCAPConfDAO
from repository.mongo.MongoDAO import MongoIO
from settings import PREPROD_MONGODB_CONNECTION_HOST, PREPROD_MONGODB_CONNECTION_PORT, \
        PREPROD_MONGODB_CONNECTION_USERNAME, PREPROD_MONGODB_CONNECTION_PASSWORD


class ProducerUpdateMongoDB(AbstractProducer):

    def execute(self, msg, logger):
        data = BuildCAPConfDAO.make_config(msg).to_dict()
        logger.info('Writing configuration to Mongo for {}'.format(msg.process.project.name))
        cursor = MongoIO(collection="configuration",
                         host=PREPROD_MONGODB_CONNECTION_HOST,
                         port=PREPROD_MONGODB_CONNECTION_PORT,
                         username=PREPROD_MONGODB_CONNECTION_USERNAME,
                         password=PREPROD_MONGODB_CONNECTION_PASSWORD)
        cursor.write(data)
        cursor.close()

        # Adding CAP configuration to the message
        msg.process.deployment.cap_config = BuildCAPConfDAO.get_config(msg.process.id)
