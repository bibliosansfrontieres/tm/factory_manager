from models.core.AbstractDockerVars import AbstractDockerVars
from repository.message.BuildCAPConfDAO import BuildCAPConfDAO
from settings import DOCKER_VARS


class InitDockerVarsApproveIDC(AbstractDockerVars):
    def __init__(self, msg):
        if not msg.process.config.deployable:
            raise Exception("Bad deployable received : {}".format(msg.process.config.deployable))
        self._msg = msg
        self.approved_project_name = BuildCAPConfDAO.get_config(msg.process.approval.process_id).project_name

    @property
    def image(self):
        return "busybox:latest"

    @property
    def name(self):
        return self.approved_project_name + \
            "." + \
            self._msg.process.approval.process_id + \
            ".ideascube.bkp"

    @property
    def volumes(self):
        return {
            '{}/{}/ideascube/{}'.format(
                DOCKER_VARS['CONTAINER_ROOT_DIR'],
                self.approved_project_name,
                self._msg.process.approval.process_id): {
                    'bind': '/backup',
                    'mode': 'rw'
                }
        }

    @property
    def volumes_from(self):
        return self.approved_project_name + \
            "." + \
            self._msg.process.approval.process_id + \
            ".ideascube"

    @property
    def command(self):
        return ["sh", "-c", """tar -C /var/cache/ideascube -cf /backup/var_cache_idc.tar .;
                tar -C /var/ideascube -chf /backup/var_idc.tar ."""]

    @property
    def wait(self):
        return True
