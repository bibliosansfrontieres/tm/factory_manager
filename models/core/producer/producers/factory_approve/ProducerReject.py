# -*- coding: utf-8 -*-
from models.core.producer.abstract_producer import AbstractProducer
from repository.docker.DockerDAO import DockerDAO
from services.process.MongoProcessLogger import MongoProcessLogger
from services.vm.vm import VMService


class ProducerReject(AbstractProducer):

    def execute(self, msg, logger):
        """
        - Mark process as rejected.
        - Removes all related containers.
        - Mark containers as deleted.
        """

        if msg.process.approval.approved:
            return

        logger.info('Process has been rejected, trashing demo containers.')

        msg.process.approval.parent_process_id = msg.process.id
        MongoProcessLogger().update_approval(msg.process.approval.process_id, msg)

        cont = DockerDAO(logger)
        cont.container_cleanup(msg.process.approval.process_id)

        VMService().mark_deleted(msg.process.approval.process_id)

        MongoProcessLogger().update_approval(msg.process.approval.process_id, msg)
