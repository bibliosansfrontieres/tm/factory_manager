# -*- coding: utf-8 -*-
from models.core.producer.abstract_producer import AbstractProducer
from .InitDockerVarsRemoveData import InitDockerVarsRemoveData
from repository.docker.DockerDAO import DockerDAO


class ProducerRemoveContainerData(AbstractProducer):

    def execute(self, msg, logger):

        #  TODO: Only handling longterm storage. Shorterm is not implemented yet.

        logger.info(
            "Trying to remove container data for process_id {}".format(msg.process.approval.process_id)
        )

        cont = DockerDAO(logger)
        cont.run(InitDockerVarsRemoveData(msg))
