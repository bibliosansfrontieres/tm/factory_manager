from models.core.AbstractDockerVars import AbstractDockerVars
from settings import DOCKER_VARS


class InitDockerVarsRemoveData(AbstractDockerVars):
    def __init__(self, msg):
        self._msg = msg

    @property
    def image(self):
        return "busybox:latest"

    @property
    def name(self):
        return self._msg.process.approval.process_id + \
            ".destroyer"

    @property
    def volumes(self):
        return {
                DOCKER_VARS['LONGTERM_VOLUME_STORAGE']: {
                    'bind': '/storage',
                    'mode': 'rw'
                }
        }

    @property
    def command(self):
        return ["sh", "-c", "rm -rf /storage/{}*".format(
                self._msg.process.approval.process_id)]

    @property
    def wait(self):
        return True
