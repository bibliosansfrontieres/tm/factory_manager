from models.core.AbstractDockerVars import AbstractDockerVars
from repository.message.BuildCAPConfDAO import BuildCAPConfDAO
from settings import DOCKER_VARS


class InitDockerVarsApproveOLIP(AbstractDockerVars):
    def __init__(self, msg):
        if not msg.process.config.deployable:
            raise Exception("Bad deployable received : {}".format(msg.process.config.deployable))
        self._msg = msg
        self.approved_project_name = BuildCAPConfDAO.get_config(msg.process.approval.process_id).project_name

    @property
    def image(self):
        return "alpine:latest"

    @property
    def name(self):
        return self.approved_project_name + \
            "." + \
            self._msg.process.approval.process_id + \
            ".olip.bkp"

    @property
    def volumes(self):
        return {
            '{}/{}/olip/{}'.format(
                DOCKER_VARS['CONTAINER_ROOT_DIR'],
                self.approved_project_name,
                self._msg.process.approval.process_id
            ):
            {
                'bind': '/backup',
                'mode': 'rw'
            },
            DOCKER_VARS["LONGTERM_VOLUME_STORAGE"] + self._msg.process.approval.process_id:  # source of data.
            {
                'bind': '/srv',
                'mode': 'rw'
            },
        }

    @property
    def command(self):
        return [
            "sh", "-c",
            "apk --no-cache add sqlite && \
            echo DELETE FROM containers \
                | sqlite3 /srv/app.db && \
            echo DELETE FROM installed_containers \
                | sqlite3 /srv/app.db && \
            echo UPDATE installed_applications SET current_state=\\'uninstalled\\' \
                | sqlite3 /srv/app.db && \
            echo UPDATE installed_contents SET current_state=\\'uninstalled\\' WHERE current_state=\\'installed\\' \
                | sqlite3 /srv/app.db && \
            tar -C /srv -cf /backup/srv.tar app.db"
        ]

    @property
    def wait(self):
        return True
