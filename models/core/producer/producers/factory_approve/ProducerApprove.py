# -*- coding: utf-8 -*-
from models.core.producer.abstract_producer import AbstractProducer
from models.core.process_status import ProcessStatus
from repository.docker.DockerDAO import DockerDAO
from services.process.MongoProcessLogger import MongoProcessLogger
from repository.process.MongoProcessDAO import MongoProcessDAO
from services.vm.vm import VMService
from .InitDockerVarsApproveIDC import InitDockerVarsApproveIDC
from .InitDockerVarsApproveOLIP import InitDockerVarsApproveOLIP
from models.core.producer.producers.utils import utils
from settings import IDC_DEPLOY_CONFIG, OLIP, BUILD_WORKSPACE


class ProducerApprove(AbstractProducer):

    def execute(self, msg, logger):

        if not msg.process.approval.approved:
            return

        msg.process.approval.parent_process_id = msg.process.id
        MongoProcessLogger().update_approval(msg.process.approval.process_id, msg)
        status = MongoProcessDAO().get_process_status(msg.process.approval.process_id)  # status of process to approve.

        if status != ProcessStatus.success:
            logger.error(
                "Process {} can't be approved, status is {}".format(
                    msg.process.approval.process_id,
                    status
                )
            )
            return

        logger.info(
            "Moving container {} for project {} to fileserver. Let's make it ready to deploy!"
            .format(msg.process.approval.process_id, msg.process.project.name)
        )

        # Make packages available on wasabi storage
        if msg.process.config.engine == "ideascube":
            _destination_folder = IDC_DEPLOY_CONFIG['package_folder']
        else:
            _destination_folder = OLIP['package_folder_wasabi']

        project_name = msg.process.project.name
        logger.info("{} : Starting copy packages to Wasabi storage".format(self.name))

        source_folder = BUILD_WORKSPACE + project_name + "/"

        utils.synchronize_zip(source_folder, _destination_folder)

        cont = DockerDAO(logger)

        # Freeze container such as nothing is modified in the db.
        cont.freeze(msg.process.approval.process_id)

        # runs a temporary container to move volumes.
        if msg.process.config.engine != "hugo":
            cont.run(InitDockerVarsApproveIDC(msg))
        else:
            cont.run(InitDockerVarsApproveOLIP(msg))

        cont.container_cleanup(msg.process.approval.process_id)

        VMService().mark_deleted(msg.process.approval.process_id)

        logger.info("Container successfully backuped for process {} !".format(msg.process.approval.process_id))
