# -*- coding: utf-8 -*-

from models.core.producer.abstract_producer import AbstractProducer
from models.core.producer.producers.utils import utils
from settings import IDC_DEPLOY_CONFIG, OLIP, BUILD_WORKSPACE


class ProducerCopyTo(AbstractProducer):
    """Now only copying to package storage."""

    def execute(self, msg, logger):

        if msg.process.config.engine == "ideascube":
            _destination_folder = IDC_DEPLOY_CONFIG['package_folder']
        else:
            _destination_folder = OLIP['package_folder_aws']

        project_name = msg.process.project.name
        logger.info("{} : Starting copy packages to {}".format(self.name, _destination_folder))

        source_folder = BUILD_WORKSPACE + project_name + "/"

        utils.synchronize_zip(source_folder, _destination_folder)
