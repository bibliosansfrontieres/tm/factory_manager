# -*- coding: utf-8 -*-

import json
import yaml
import subprocess
import tempfile

from models.core.producer.abstract_producer import AbstractProducer
from models.core.producer.producers.utils import IdcCatalog
from repository.omeka.MsgOmekaPackagesDAO import MsgOmekaPackagesDAO

from settings import IDC_DEPLOY_CONFIG


class ProducerMakeIdeascubePackages(AbstractProducer):
    """
    Building Ideascube packages that needs building.
    """

    def execute(self, msg, logger):
        if msg.process.config.engine != "ideascube":
            return

        project_name = msg.process.project.name
        omeka_packages = MsgOmekaPackagesDAO.get_packages(msg)

        logger.info("{} : Starting to create Ideascube-format Omeka packages.".format(self.name))

        msg.process.catalog_path = tempfile.NamedTemporaryFile(dir=msg.process.work_dir).name

        catalog = IdcCatalog(msg.process.catalog_path, url=IDC_DEPLOY_CONFIG["catalogs"][1]["url"])

        try:
            for pkg in omeka_packages:

                if pkg.update_needed(catalog):
                    logger.info("""{} : Catalog version of package {} is too old, updating..."""
                                .format(project_name, pkg.slug))

                    compile_errors = make_package(pkg, IDC_DEPLOY_CONFIG["omeka_package_url"], msg.process.work_dir)
                    if compile_errors:
                        raise Exception(compile_errors)

                    logger.debug("{} : Package {} built.".format(project_name, pkg.id))
                else:
                    logger.debug("{} : Package {} is up-to-date, not updating.".format(project_name, pkg.id))

        except Exception as e:
            raise Exception("Unable to make package : {}".format(e))


def create_json(pkg):

    full_json = []
    for item in pkg.items:
        full_json.append(item.to_dict())

    try:
        with open(pkg.json_path, "w") as file:
            json.dump(full_json, file, sort_keys=True, indent=2)
    except IOError as e:
        raise Exception("Cannot open file {} to create json; errno {}.".format(pkg.json_path, e.errno))


def make_package(pkg, package_url, package_dest):
    command = ['omeka-to-pkg', '--package-id', pkg.slug, '--name', pkg.ideascube_name, '--description',
               pkg.description, '--language', pkg.language, '--url', package_url + pkg.slug + ".zip",
               pkg.json_path, package_dest + "/" + pkg.slug + ".zip"]

    create_json(pkg)

    p = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()

    yaml_start_index = stdout.find(b'all:')
    yaml_content = stdout[yaml_start_index:]

    try:
        with open(pkg.txt_path, "w") as file:
            file.write(stderr.decode('utf-8'))
            file.write(stdout.decode('utf-8'))
    except IOError as e:
        raise Exception("Cannot open the file {} to write; errno {}.".format(pkg.txt_path, e.errno))

    path = package_dest + "/yaml/" + pkg.slug
    try:
        with open(path, "w") as file:
            yaml.safe_dump(yaml_content, file, default_flow_style=False, encoding='utf-8')
    except IOError as e:
        raise Exception("Cannot open the file {} to write; errno {}".format(path, e.errno))

    return stderr
