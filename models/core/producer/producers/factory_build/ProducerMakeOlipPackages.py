from models.core.producer.abstract_producer import AbstractProducer
from repository.omeka.MsgOmekaPackagesDAO import MsgOmekaPackagesDAO
from models.core.producer.producers.utils import OlipCatalog
from .hugo_package import MakeHugoPackage
from settings import OLIP
from os import path
from pathlib import Path

class ProducerMakeOlipPackages(AbstractProducer):
    """
    Building Hugo mediacenter packages that needs building.
    """

    def execute(self, msg, logger):

        if msg.process.config.engine != "hugo":
            return

        project_name = msg.process.project.name
        omeka_packages = MsgOmekaPackagesDAO.get_packages(msg)

        logger.info("{} : Starting to check for Omeka packages needing a build.".format(self.name))

        catalogs = OlipCatalog(urls=OLIP["catalogs"])

        for pkg in omeka_packages:

            need_build = False
            if not Path(path.join(OLIP["package_folder_aws"], pkg.slug) + ".zip").is_file():
                need_build = True
                logger.info("{} : Package {} (id {}) doesn't exist on the fileserver, creating..."
                            .format(project_name, pkg.slug, pkg.id))
            elif pkg.update_needed(catalogs):
                need_build = True
                logger.info("{} : Package {} (id {}) was edited in Omeka, updating..."
                            .format(project_name, pkg.slug, pkg.id))

            if need_build:

                go = MakeHugoPackage(pkg=pkg, work_dir=msg.process.work_dir, logger=logger)
                go.build()
                zip_path = go.zippit()

                catalogs.add(pkg, zip_path, logger)

                logger.debug("{} : Package {} (id {}) built.".format(project_name, pkg.slug, pkg.id))

        msg.process.catalog_path = catalogs.store(msg.process.work_dir)
