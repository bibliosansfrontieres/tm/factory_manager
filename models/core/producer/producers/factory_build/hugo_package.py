import os
import toml
import json
import zipfile
from datetime import datetime
from dateutil.tz import tzoffset
from slugify import slugify
from urllib.request import urlretrieve


class MakeHugoPackage:
    """
    Object responsible of creating packages for the OLIP mediacenter.
    """
    def __init__(self, pkg, work_dir, logger):
        self.pkg = pkg
        self.work_dir = work_dir
        self.logger = logger
        self.package_dir = self.work_dir + "/" + pkg.slug

        try:
            os.makedirs(self.package_dir)
        except FileExistsError:
            pass

    def build(self):

        pkg_cat = self.create_package_index_markdown()
        self.create_items(pkg_cat)
        self.create_index_json()

    def create_package_index_markdown(self):
        """
        Creates _index.md that describes package.
        :return: list. The package category (a string bundled into a list, our template constraint)
        that will be applied to all items.
        """
        self.logger.debug('Building _index.md for package.')

        filename = self.package_dir + "/_index.md"
        metadata = dict()
        metadata['title'] = self.pkg.ideascube_name
        metadata['description'] = self.pkg.description
        metadata['categories'] = ["No category"] if self.pkg.subject is None else [self.pkg.subject]

        with open(filename, 'w') as f:
            f.write('+++\n')
            toml.dump(metadata, f)
            f.write('+++\n')

        return metadata['categories']

    def create_index_json(self):
        """
        Build index.json file to be used as a database for search engine.
        """
        self.logger.debug('Building index.json (search db) for package.')

        result = list()

        for item in self.pkg.items:
            i = dict()
            i['id'] = item.id  # Using Omeka item ID.
            i['title'] = item.title
            i['description'] = item.summary
            i['tags'] = item.tags.split(',')
            i['href'] = "content/" + self.pkg.slug + "/" + item.filename_slug + "." + item.file_extension
            result.append(i)

        with open(self.package_dir + "/index.json", 'w') as f:
            json.dump(result, f, indent=4, ensure_ascii=False)

    @staticmethod
    def format_filename(filename):
        """
        This function is to make sure filename gets slugified.
        It's useless most of the time. As Omeka takes care of this, still some old filename aren't _clean_.

        :params path: filename of the item
        :type path: `str`
        """

        file_extension = filename.split(".")[-1:][0].lower()
        filename_slug = slugify(filename)

        return filename_slug, file_extension

    def create_items(self, package_category):
        """
        Main loop to create all items in a package.
        """
        now = datetime.now(tzoffset('EDT', +4 * 60 * 60)).replace(microsecond=0).isoformat()

        for item in self.pkg.items:
            item.filename_slug, item.file_extension = self.format_filename(item.filename)

            self.logger.debug('Creating markdown and downloading media for Omeka item #{}.'.format(item.id))

            self.create_item_markdown(item, now, package_category)
            self.pull_thumbnail(item)
            self.pull_media(item)

    def create_item_markdown(self, item, now, package_category):
        """
        Creates `filename_slug`.md that describes an item.

        :params item: item metadatas
        :params now: precompiled datetime
        :type item: `OmekaPackageItem`
        :type now: `str`
        """
        filename = self.package_dir + "/" + item.filename_slug + ".md"

        metadata = dict()
        metadata['date'] = now
        metadata['title'] = item.title
        metadata['lang'] = item.language
        metadata['mime_type'] = item.mime_type
        metadata['categories'] = package_category
        metadata['image'] = str() if not item.thumbnail_url else \
            "content/" + self.pkg.slug + "/" + item.filename_slug + "." + item.thumbnail_url

        with open(filename, 'w') as f:
            f.write('+++\n')
            toml.dump(metadata, f)
            f.write('+++\n')
            f.write(item.summary)

    def pull_thumbnail(self, item):
        """
        Downloads the media's thumbnail locally.

        :params item: item metadata
        :type item: `OmekaPackageItem`
        """
        dst = self.package_dir + "/" + item.filename_slug + ".jpg"
        try:
            if item.thumbnail_url:
                urlretrieve(item.thumbnail_url.replace(" ", "%20"), dst)
        except Exception as e:
            raise Exception('Failed downloading {} : {}'.format(item.thumbnail_url, e))

    def pull_media(self, item):
        """
        Downloads the media locally.

        :params item: item metadata
        :type item: `OmekaPackageItem`
        """
        dst = self.package_dir + "/" + item.filename_slug + "." + item.file_extension
        try:
            urlretrieve(item.file_url.replace(" ", "%20"), dst)
        except Exception as e:
            raise Exception('Failed downloading {} : {}'.format(item.file_url, e))

    def zippit(self):
        """
        Zip the whole folder, this will be the actual package.
        :return: `str`, Absolute path of the zip file.
        """
        zip_filename = self.work_dir + "/" + self.pkg.slug + ".zip"

        with zipfile.ZipFile(zip_filename, 'w') as zipf:
            for root, dirs, files in os.walk(self.package_dir):
                for filename in files:
                    zipf.write(os.path.join(root, filename), arcname=filename)

        return zip_filename
