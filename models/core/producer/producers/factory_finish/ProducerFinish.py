# -*- coding: utf-8 -*-

from models.core.producer.abstract_producer import AbstractProducer
from services.process.MongoProcessLogger import MongoProcessLogger
from models.core.process_status import ProcessStatus


class ProducerFinish(AbstractProducer):

    def execute(self, msg, logger):

        msg.process.status = ProcessStatus.success
        MongoProcessLogger().update_message(msg)
