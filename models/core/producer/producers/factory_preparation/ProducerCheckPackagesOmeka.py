# -*- coding: utf-8 -*-

from models.core.producer.exceptions import exceptions
from repository.omeka.MsgOmekaPackagesDAO import MsgOmekaPackagesDAO
from models.core.producer.abstract_producer import AbstractProducer


class ProducerCheckPackagesOmeka(AbstractProducer):
    """
    Checking if all packages metadata are correct/present.
    """

    def execute(self, msg, logger):

        project = msg.process.project
        omeka_packages = MsgOmekaPackagesDAO.get_packages(msg)
        logger.info("{} : Checking Omeka packages metadata for project {}.".format(self.name, project.name))

        raise_warning = False
        for pkg in omeka_packages:
            logger.debug("{} : Checking metadata for all items in package {}.".format(project.name, pkg.id))

            for item in pkg.items:
                for error in item.get_errors():
                    logger.warn("{} : Package {} / Item {} / Error [{} : {}]"
                                .format(project.name, pkg.id, item.id, error.status_code, error.description))
                    raise_warning = True

        if raise_warning:
            raise exceptions.ProducerWarning("Error found in project package.")
        logger.info("{} : No package's errors found in project {}.".format(self.name, project.name))
