# -*- coding: utf-8 -*-

import os

from models.core.producer.abstract_producer import AbstractProducer
from settings import BUILD_WORKSPACE


class ProducerInitWorkspace(AbstractProducer):
    """
    Preparing workspace, creating various folders, cleaning up dirty things.
    """

    def execute(self, msg, logger):

        project_name = msg.process.project.name
        logger.info("{} : Initializing workspace for project {}.".format(self.name, project_name))

        msg.process.work_dir = BUILD_WORKSPACE + project_name

        yml_folder = msg.process.work_dir + "/yaml"  # For IDC packages
        json_folder = msg.process.work_dir + "/json"  # For OLIP packages

        #  Removing any trace of previous build.
        if os.path.exists(yml_folder):
            for root, dirs, files in os.walk(msg.process.work_dir, topdown=False):
                for name in files:
                    os.remove(os.path.join(root, name))
        else:
            os.makedirs(yml_folder)

        if os.path.exists(json_folder):
            for root, dirs, files in os.walk(msg.process.work_dir, topdown=False):
                for name in files:
                    os.remove(os.path.join(root, name))
        else:
            os.makedirs(json_folder)
