# -*- coding: utf-8 -*-

from models.core.producer.abstract_producer import AbstractProducer
from models.core.producer.producers.utils import OmekaApi


class ProducerGetProject(AbstractProducer):
    """
    Update details of project.
    """
    def execute(self, msg, logger):

        project = msg.process.project
        logger.info("{} : Checking project {}".format(self.name, project.id))

        OmekaApi.update_project(project)
