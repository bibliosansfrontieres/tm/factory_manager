# -*- coding: utf-8 -*-

from models.core.producer.abstract_producer import AbstractProducer
from models.core.producer.producers.utils import OmekaApi, MsgRelationItemsDAO


class ProducerListItemsRelations(AbstractProducer):
    """
    Updates message with item_relations, aka kiwix/kolibri content.
    """

    def execute(self, msg, logger):

        project = msg.process.project
        logger.info("{} : Listing item relations for project {}.".format(self.name, project.id))

        try:
            relation_items = OmekaApi.get_relations_items_by_project_id(project.id)
            if relation_items.count() != 0:
                for item in relation_items:
                    logger.debug("{} : Item {} added to data.".format(project.name, item.id))
            else:
                logger.debug("{} : No items found for project {}.".format(project.name, project.id))

            # update relation_items in message
            MsgRelationItemsDAO.update_items(msg, relation_items)
        except Exception as e:
            raise Exception("Unable to list items relations : {}".format(e))
