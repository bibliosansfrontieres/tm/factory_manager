# -*- coding: utf-8 -*-

import yaml
import requests

from models.core.producer.exceptions import exceptions
from models.core.producer.abstract_producer import AbstractProducer
from models.core.producer.producers.utils import MsgRelationItemsDAO
from settings import IDC_DEPLOY_CONFIG, EXTERNAL_PACKAGE_TYPE


class ProducerCheckUrlAvailability(AbstractProducer):
    """
    Checks if all item relations URL are accessible.
    TODO: check for download_path in descriptor too.
    """

    def execute(self, msg, logger):

        project = msg.process.project
        items = MsgRelationItemsDAO.get_items(msg)
        logger.info("{} : Checking url availability for items in project {}.".format(self.name, project.name))

        full_catalog = {}
        for elem in IDC_DEPLOY_CONFIG['catalogs']:
            try:
                r = requests.get(elem['url'])
                catalog = yaml.load(r.text.encode('utf-8'))['all']

                full_catalog.update(catalog)
            except Exception:
                raise Exception("Unable to update the catalog.".format(self.name))

        raise_warning = False
        for item in items:
            if item.type == EXTERNAL_PACKAGE_TYPE['kiwix_packages']:
                logger.debug("{} : Checking url availability for item {}.".format(project.name, item.id))

                for error in item.get_errors(full_catalog):
                    logger.warn("{} : {}".format(project.name, error))
                    raise_warning = True

        if raise_warning:
            raise exceptions.ProducerWarning("Error found in project static sites.")
        logger.info("{} : No url errors found in project {}.".format(self.name, project.name))
