# -*- coding: utf-8 -*-

from models.core.producer.abstract_producer import AbstractProducer
from models.core.producer.producers.utils import OmekaApi
from repository.omeka.MsgOmekaPackagesDAO import MsgOmekaPackagesDAO


class ProducerListPackagesOmeka(AbstractProducer):
    """
    Updates message with package content/items informations.
    """

    def execute(self, msg, logger):

        project = msg.process.project
        logger.info("{} : Listing omeka packages for project {}.".format(self.name, project.id))

        try:
            omeka_packages = OmekaApi.get_packages_by_project_id(project.id)
            if omeka_packages.count() != 0:
                for pkg in omeka_packages:
                    logger.debug("{} : Package {} added to data.".format(project.name, pkg.id))
            else:
                logger.debug("{} : No Omeka packages found for project {}.".format(project.name, project.id))

            # update omeka_packages in message
            MsgOmekaPackagesDAO.update_packages(msg, omeka_packages)
        except Exception as e:
            raise Exception("Unable to list Omeka packages : {}".format(e))
