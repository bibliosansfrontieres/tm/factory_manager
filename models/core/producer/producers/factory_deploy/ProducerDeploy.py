# -*- coding: utf-8 -*-

from models.core.producer.abstract_producer import AbstractProducer
from models.core.process_status import ProcessStatus
from repository.process.MongoProcessDAO import MongoProcessDAO
from repository.message.BuildCAPConfDAO import BuildCAPConfDAO
from services.process.MongoProcessLogger import MongoProcessLogger


class ProducerDeploy(AbstractProducer):

    def execute(self, msg, logger):
        """
        Will check if process_id for deployment has been approved and that approval process is not running.
        Write new deployment message to DB.
        Update Message with configuration content for given process_id.
        """

        approved = MongoProcessDAO().get_approval(msg.process.deployment.process_id)
        status = MongoProcessDAO().get_process_status(msg.process.approval.parent_process_id)

        if approved and (status == ProcessStatus.success):

            logger.debug('process_id approved, proceeding with deployment.')
            msg.process.deployment.cap_config = BuildCAPConfDAO.get_config(msg.process.deployment.process_id)
            msg.process.deployment.parent_process_id = msg.process.id

            MongoProcessLogger().update_deploy(msg.process.deployment.process_id, msg)

            for device in msg.process.deployment.devices.devices:
                logger.info("{} : wrote build_id {} for device(s) {}".format(
                    self.name,
                    msg.process.deployment.process_id,
                    device.mac_address
                    ))

        else:
            logger.error('process_id {} has not been approved, \
                         or is rejected, or approved process is still running. \
                         Please check what could be wrong.'
                         .format(msg.process.approval.process_id))
