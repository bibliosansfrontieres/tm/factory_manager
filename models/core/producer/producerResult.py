class ProducerResult:
    """Result of the execution of a producer.

    The ProducerResult should be used to control whether the factory should continue a process after
    the execution of one producer, or stop the factory after the execution of all its producers.\n
    * In the event that the process should be stopped due to a serious error, an exception should be raised.

    Parameters
    ----------
    result_type : `ResultType`
        Type of the producer result.

    See Also
    --------
    factory_manager.models.result.ResultType
    """

    def __init__(self, result_type):
        self._type = result_type

    @property
    def type(self):
        """`ResultType`: Type of the result.
        """
        return self._type
