from models.result import ResultType
from .producerResult import ProducerResult


class ProducerResults:
    """Collection of `ProducerResult` instances.

    See Also
    --------
    factory_manager.models.result.ProducerResult
    """

    def __init__(self):
        self._results_list = []

    def add_result(self, result):
        """Add a `ProducerResult` at the end of the collection.

        Parameters
        ----------
        result : `ProducerResult`
            ProducerResult to add to the collection.

        Raises
        ------
        TypeError
            Raised if ``result`` is not an instance of `ProducerResult`.
        """
        if not isinstance(result, ProducerResult):
            raise TypeError("Invalid arguments type to add.")

        self._results_list.append(result)

    def has_warnings(self):
        """Check if the collection contains any ProducerResult with a warning.

        Returns
        -------
        has_warnings : `bool`
            True if any result type inside the collection is a warning. False in any other case.
        """
        return any(result.type == ResultType.WARNING for result in self._results_list)
