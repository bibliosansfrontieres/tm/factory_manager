# -*- coding: utf-8 -*-


class VirtualMachineModel:
    """Define log message format.
    """

    def __init__(self,
                 container_id=str(),
                 name=str(),
                 project_name=str(),
                 process_id=str(),
                 mediacenter=str(),
                 url=str(),
                 created_at=str(),
                 owner=str()):

        self._container_id = container_id
        self._name = name
        self._project_name = project_name
        self._process_id = process_id
        self._mediacenter = mediacenter
        self._url = url
        self._created_at = created_at
        self._owner = owner

    def to_dict(self):
        """Get the `VirtualMachine` as a dictionary.

        Returns
        -------
        dict : `dict`
            The `VirtualMachine` as dictionary.
        """
        return {
                    "container_id": self._container_id,
                    "name": self._name,
                    "project_name": self._project_name,
                    "process_id": self._process_id,
                    "mediacenter": self._mediacenter,
                    "url": self._url,
                    "created_at": self._created_at,
                    "owner": self._owner
                }
