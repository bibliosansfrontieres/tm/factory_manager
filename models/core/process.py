# -*- coding: utf-8 -*-
from models.core.omeka_project import OmekaProject
from models.core.omeka_project import KEYWORD_PROJECT
from models.core.mc_config import MediaCenterConfig
from models.core.mc_config import KEYWORD_CONFIG
from models.core.recipe import Recipe
from models.core.recipe import KEYWORD_RECIPE
from models.core.approval import Approval
from models.core.approval import KEYWORD_APPROVAL
from models.core.deploy_config import Deploy
from models.core.deploy_config import KEYWORD_DEPLOY
from models.core.user import User
from models.core.process_status import ProcessStatus


KEYWORD_PROCESS = "process"
"""`str` : Keyword used in dictionary to index a `Process`."""


class Process:
    """Process for the execution.

    Parameters
    ----------
    process_id : `str`
        Id of the `Process`.
    recipe : `Recipe`
        Current recipe in execution.
    project : `OmekaProject`
        Omeka Project to build.
    config : `MediaCenterConfig`
        Configuration for the creation of the Media Center.
    approval: `Approval`
        Deployment approval details.
    deploy: `Deploy`
        Deployment details.
    image_url: `str`
        Empty at first, will be defined when preprod is deployed.

    Raises
    ------
    TypeError
        Raised if any arguments for the `Process` is not the expected type.

    See Also
    --------
    factory_manager.models.recipe.Recipe
    factory_manager.models.omeka_project.OmekaProject
    factory_manager.models.mc_config.MediaCenterConfig
    factory_manager.models.approval.Approval
    """

    def __init__(self, process_id, recipe, project, user, config,
                 description, timestamp, approval, deploy, image_url=None, status=ProcessStatus.start,
                 work_dir=None, catalog_path=None):
        if not isinstance(recipe, Recipe) or not isinstance(project, OmekaProject) or \
            not isinstance(config, MediaCenterConfig) or \
            not isinstance(approval, Approval) or \
            not isinstance(deploy, Deploy) or \
                not isinstance(user, User):

            raise TypeError("Invalid arguments type for Process.")

        self._id = process_id
        self._description = description
        self._timestamp = timestamp
        self._recipe = recipe
        self._project = project
        self._user = user
        self._config = config
        self._approval = approval
        self._deploy = deploy
        self._image_url = image_url
        self._status = status
        self._catalog_path = catalog_path
        self._work_dir = work_dir

    @property
    def id(self):
        """`str`: Id of the process.
        """
        return self._id

    @property
    def description(self):
        """`str`: description of the process.
        """
        return self._description

    @property
    def timestamp(self):
        """
        `str`: timestamp of the process.
        """
        return self._timestamp

    @property
    def recipe(self):
        """`Recipe`: Current `Recipe` of the process.
        """
        return self._recipe

    @recipe.setter
    def recipe(self, value):
        self._recipe = value

    @property
    def project(self):
        """`OmekaProject`: Current `OmekaProject` of the process.
        """
        return self._project

    @property
    def user(self):
        """`OmekaProject`: Current `OmekaProject` of the process.
        """
        return self._user

    @property
    def config(self):
        """`MediaCenterConfig`: Current `MediaCenterConfig` of the process.
        """
        return self._config

    @property
    def approval(self):
        """`Approval`: Current `Approval` of the process.
        """
        return self._approval

    @property
    def deployment(self):
        """`Approval`: Current `Approval` of the process.
        """
        return self._deploy

    @property
    def image_url(self):
        """`image_url`: Current `image_url` of the process.
        """
        return self._image_url

    @image_url.setter
    def image_url(self, value):
        self._image_url = value

    @property
    def status(self):
        """`status`: Current `status` of the process.
        """
        return self._status

    @status.setter
    def status(self, value):
        self._status = value

    @property
    def work_dir(self):
        """`work_dir`: Current `work_dir` of the process.
        """
        return self._work_dir

    @work_dir.setter
    def work_dir(self, value):
        self._work_dir = value

    @property
    def catalog_path(self):
        """`catalog_path`: Current `catalog_path` of the process.
        """
        return self._catalog_path

    @catalog_path.setter
    def catalog_path(self, value):
        self._catalog_path = value

    def next_factory(self, factory_name):
        """Get the name of the next factory in the `Recipe`.

        Parameters
        ----------
        factory_name : `str`
            Name of the factory in execution.

        Returns
        -------
        next_factory : `str`
            Name of the next factory to execute.

        Notes
        -----
        Check in the process recipe for the ``factory_name`` position and returns the factory name
        placed next.
        Do not use when the `Recipe` includes multiple times the same factory name.
        """
        current = self._recipe.factories.index(factory_name)
        return self._recipe.get_factory_by_index(current + 1)

    def to_dict(self):
        """Get the `Process` as dictionary.

        Returns
        -------
        dict : `dict`
            The `Process` as dictionary with the following format:

            - ``'id'``: id of the `Process`.
            - ``KEYWORD_RECIPE``: dictionary of current `Recipe` of the process.
            - ``KEYWORD_PROJECT``: dictionary of current `OmekaProject` of the process.
            - ``KEYWORD_CONFIG``: dictionary of current `MediaCenterConfig` of the process.

        See Also
        --------
        factory_manager.models.recipe.KEYWORD_RECIPE
        factory_manager.models.omeka_project.KEYWORD_PROJECT
        factory_manager.models.mc_config.KEYWORD_CONFIG
        """
        return {
            'id': self._id,
            'description': self._description,
            'timestamp': self._timestamp,
            'image_url': self._image_url,
            'user': self._user.to_dict(),
            'status': self._status,
            'work_dir': self._work_dir,
            'catalog_path': self._catalog_path,
            KEYWORD_RECIPE: self._recipe.to_dict(),
            KEYWORD_PROJECT: self._project.to_dict(),
            KEYWORD_CONFIG: self._config.to_dict(),
            KEYWORD_APPROVAL: self._approval.to_dict(),
            KEYWORD_DEPLOY: self._deploy.to_dict()
        }
