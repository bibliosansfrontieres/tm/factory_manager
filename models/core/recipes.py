from models.core.recipe import Recipe


class Recipes:
    """Collection of `Recipe` instances.

    Parameters
    ----------
    recipes : `list` of `Recipe`, optional
        Content of the `Recipes` (empty list, by default).

    Raises
    ------
    TypeError
        Raised if any arguments for the `Recipes` is not the expected type.

    See Also
    --------
    factory_manager.models.recipe.Recipe
    """

    def __init__(self, recipes=None):
        if recipes and not all(isinstance(r, Recipe) for r in recipes):
            raise TypeError("Invalid arguments type for Recipes.")

        self._recipes_list = [] if recipes is None else recipes

    def add_recipe(self, recipe):
        """Add a `Recipe` at the end of the collection.

        Parameters
        ----------
        recipe : `Recipe`
            Recipe to add to the collection.

        Raises
        ------
        TypeError
            Raised if ``recipe`` is not an instance of `Recipe`.
        Exception
            Raised if in the collection exist already a `Recipe` with the same id as ``recipe``.
        """
        if not isinstance(recipe, Recipe):
            raise TypeError("Invalid arguments type to add.")
        if any(recipe.id == r.id for r in self._recipes_list):
            raise Exception("Existing conflict between recipes with same id.")

        self._recipes_list.append(recipe)

    def get_recipe_by_id(self, recipe_id):
        """Get the `Recipe` inside the collection by id.

        Parameters
        ----------
        recipe_id : `int`
            Id of the recipe to search.

        Returns
        -------
        recipe : `Recipe`
            Recipe with the corresponding ``recipe_id`` if it is in the collection. ``None`` if it is not.
        """
        for recipe in self._recipes_list:
            if recipe.id == recipe_id:
                return recipe
        return None
