# -*- coding: utf-8 -*-

from datetime import datetime


class Logs:
    """Define log message format.
    """

    def __init__(self, data):

        self._user = str() if data.get('user') is None else data.get('user')
        self._level = "ERROR" if data.get('level') is None else data.get('level')
        self._date = datetime.now() if data.get('date') is None else data.get('date')
        self._log = "No log content" if data.get('log') is None else data.get('log')
        self._process_id = str() if data.get('process_id') is None else data.get('process_id')

    @property
    def process_id(self):
        return self._process_id

    @property
    def user(self):
        return self._user

    @property
    def date(self):
        return self._date

    @property
    def log(self):
        return self._log

    @property
    def level(self):
        return self._level

    def to_dict(self):
        """Get the `Log` as dictionary.

        Returns
        -------
        dict : `dict`
            The `Log` as dictionary.
        """
        return {
                    "user": self._user,
                    "date": str(self._date),
                    "log": self._log,
                    "level": self._level,
                    "process_id": self._process_id
                }

    @staticmethod
    def error_lvl(process_id, level):
        return {"process_id": process_id,
                "error_lvl": level}
