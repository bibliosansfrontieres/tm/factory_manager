from marshmallow import Schema, fields, post_load, post_dump


# ##################### Configuration schema definition
from models.core.olip_descriptor import Descriptor, Application, Container, Content, Search, Endpoint, Authentication


class EndpointSchema(Schema):
    url = fields.Str()
    container = fields.Str()
    name = fields.Str()

    @post_load
    def make_endpoint_config(self, data, **kwargs):
        return Endpoint(**data)


class ContentSchema(Schema):
    content_id = fields.Str()
    description = fields.Str()
    name = fields.Str()
    language = fields.Str(missing="en")
    subject = fields.Str(missing=None)
    size = fields.Str(missing="0")
    version = fields.Str()
    endpoint = fields.Nested(EndpointSchema)
    download_path = fields.Str()
    destination_path = fields.Str()

    @post_load
    def make_content_config(self, data, **kwargs):
        return Content(**data)


class SearchSchema(Schema):
    opensearch_url = fields.Str()
    container = fields.Str()

    @post_load
    def make_search_config(self, data, **kwargs):
        return Search(**data)


class ContainerSchema(Schema):
    image = fields.Str()
    name = fields.Str()
    expose = fields.Integer(required=False, allow_none=True)

    @post_load
    def make_container_config(self, data, **kwargs):
        return Container(**data)


class AuthenticationSchema(Schema):
    grant_types = fields.List(fields.Str())
    response_types = fields.List(fields.Str())
    token_endpoint_auth_method = fields.Str()

    @post_load
    def make_auth_config(self, data, **kwargs):
        return Authentication(**data)


class ApplicationsSchema(Schema):
    name = fields.Str()
    description = fields.Str()
    version = fields.Str()
    picture = fields.Str()
    containers = fields.List(fields.Nested(ContainerSchema))
    bundle = fields.Str()
    search = fields.Nested(SearchSchema, required=False)
    contents = fields.List(fields.Nested(ContentSchema))
    authentication = fields.Nested(AuthenticationSchema, required=False)

    @post_load
    def make_application_config(self, data, **kwargs):
        return Application(**data)

    @post_dump
    def remove_skip_values(self, data, **kwargs):
        return {
            key: value for key, value in data.items()
            if value is not None
        }


class DescriptorRootSchema(Schema):
    applications = fields.List(fields.Nested(ApplicationsSchema))
    terms = fields.List(fields.Str())

    @post_load
    def make_descriptor_config(self, data, **kwargs):
        return Descriptor(**data)
