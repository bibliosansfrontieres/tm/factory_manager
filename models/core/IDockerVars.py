from abc import ABCMeta, abstractmethod


class IDockerVars(metaclass=ABCMeta):
    def __init__(self):
        pass

    @property
    @abstractmethod
    def image(self):
        pass

    @property
    @abstractmethod
    def volumes(self):
        pass

    @property
    @abstractmethod
    def volumes_from(self):
        pass

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    @abstractmethod
    def detach(self):
        pass

    @property
    @abstractmethod
    def remove(self):
        pass

    @property
    @abstractmethod
    def environment(self):
        pass

    @property
    @abstractmethod
    def announce_container(self):
        pass

    @property
    @abstractmethod
    def privileged(self):
        pass

    @property
    @abstractmethod
    def labels(self):
        pass

    @property
    @abstractmethod
    def network(self):
        pass

    @property
    @abstractmethod
    def command(self):
        pass

    @property
    @abstractmethod
    def wait(self):
        pass
