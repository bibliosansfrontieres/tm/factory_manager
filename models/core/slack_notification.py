import json
import requests
from settings import SLACK_NOTIF, ENVIRON


class Slack:
    def __init__(self, msg):
        self._msg = msg
        self.ENVIRON = ENVIRON

    def forge_msg(self, channel=None, icon_emoji=None, username=None, text=None):
        """Forge a generic message if no arguments are given.
        :param channel: The Slack channel.person to send the message to.
        :param icon_emoji: The icon representing the sender.
        :param username: The username of the sender.
        :param text: Content of message.
        :type channel: str. Must have the form "#channel" for a channel or "@username" for a direct msg to a user.
        :type icon_emoji: str. Must have the form ":my_icon:"
        :type username: str.
        :type text: str. If you want to mention a user in the message, use "<@username>".
        """

        self.payload = dict()
        self.payload['channel'] = SLACK_NOTIF['channel'] if channel is None else channel
        self.payload['icon_emoji'] = SLACK_NOTIF['icon_emoji'] if icon_emoji is None else icon_emoji
        self.payload['username'] = SLACK_NOTIF['username'] if username is None else username
        self.payload['text'] = """<@{}> *New job started*
‣ Recipe name : {}
‣ Project name : {}
‣ Description : {}
‣ Process_id: {}""".format(
                self._msg.process.user.username,
                self._msg.process.recipe.name,
                self._msg.process.project.name,
                self._msg.process.description,
                self._msg.process.id
                ) if text is None else text

    def send(self):

        headers = {'Content-Type': 'application/json'}
        try:
            if self.ENVIRON == "PROD":
                requests.post(
                    SLACK_NOTIF['url'],
                    data=json.dumps(self.payload),
                    headers=headers
                )
        except Exception:
            pass
