# -*- coding: utf-8 -*-

from pymongo import MongoClient
from settings import PREPROD_MONGODB_CONNECTION_HOST, PREPROD_MONGODB_CONNECTION_PORT, \
    PREPROD_MONGODB_CONNECTION_USERNAME, PREPROD_MONGODB_CONNECTION_PASSWORD, PREPROD_MONGODB_NAME


class MongoDBConnection:

    def __init__(self, host=PREPROD_MONGODB_CONNECTION_HOST, port=PREPROD_MONGODB_CONNECTION_PORT,
                 username=PREPROD_MONGODB_CONNECTION_USERNAME, password=PREPROD_MONGODB_CONNECTION_PASSWORD,
                 db=PREPROD_MONGODB_NAME):

        self._connection = MongoClient(host, port)
        self._db = db
        if username and password:
            self._connection[db].authenticate(username, password)

    def get_collection(self, collection):
        return self._connection[self._db][collection]

    def close(self):
        self._connection.close()
