# -*- coding: utf-8 -*-

from models.core.rabbit_mq_connection import RabbitMQConnection
from services.logger.RabbitMQLogger import RabbitMQLogger
from services.logger.MongoLogger import MongoLogger
from services.notifications.slack import SlackNotification

APP_PREFIX = "tmp."
START_EXCHANGE = APP_PREFIX + "start"
LOGGER_QUEUE = APP_PREFIX + "log"


class Gateway:

    def __init__(self, host, port, vhost, username, password, start_exchange=START_EXCHANGE,
                 log_queue=LOGGER_QUEUE):

        self.start_exchange = start_exchange

        # Use only one logger please, comment the other.
        self._logger = RabbitMQLogger(__name__, host, port, vhost, username, password, log_queue)
        # Is using the same host, port, username, password than RabbitMQ connection (for the moment)
        # self._logger = MongoLogger(__name__, host, port, username, password)

        self._logger.info("[{}] Initializing. (host: {}, port: {}, vhost: {})"
                          .format(__name__, host, port, vhost))

        self._host = host
        self._port = port
        self._vhost = vhost
        self._username = username
        self._password = password

    def publish(self, msg):

        body = msg.get_body()

        if isinstance(self._logger, MongoLogger):
            self._logger.update_msg(msg)
        self._logger.info("[Gateway] Publishing message to {}".format(self.start_exchange))

        try:
            connection = RabbitMQConnection(self._host, self._port, self._vhost, self._username,
                                            self._password)
            # publishing to fanout exchange
            connection.publish(
                body=body,
                exchange=self.start_exchange,
                routing_key=''
            )
            connection.close()

        except Exception as e:
            return e.args, 500

        SlackNotification().send(msg)
        return msg.process.to_dict(), 200
