# -*- coding: utf-8 -*-

from enum import Enum


class ResultType(Enum):
    """`Enum`.\n
    Type of ProducerResult.
    """
    SUCCESS = 1
    """Case in which the process was successfully executed."""
    WARNING = 2
    """Case in which the process was executed with warning."""
