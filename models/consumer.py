# -*- coding: utf-8 -*-

from threading import Thread

from models.core.rabbit_mq_connection import RabbitMQConnection


class ConsumerThread(Thread):
    """Independent thread to consume a RabbitMQ queue.

    Establish in a new thread a connection with a RabbitMQ server and consumes.

    Parameters
    ----------
    callback : `callable`
        Callback method to execute when consumes.
    queue : `str`
        Queue name to consume.
    host : `str`
        Connection host.
    port : `int`
        Connection port.
    vhost : `str`
        Connection virtual host.
    username : `str`
        Connection username.
    password : `str`
        Connection password.

    See Also
    --------
    factory_manager.models.rabbit_mq_connection.RabbitMQConnection
    """

    def __init__(self, callback, queue, host, port, vhost, username, password):
        super(ConsumerThread, self).__init__()
        self.setDaemon(True)

        self._callback = callback
        self._queue = queue

        self._connection = RabbitMQConnection(host, port, vhost, username, password)

    def run(self):
        """Starts consuming the queue when the thread starts to run.

        When the thread starts to run starts to consume the specific queue and execute a callback
        in case a message is consumed.
        """
        self._connection.start_consume(self._callback, self._queue)

    def __del__(self):
        """When the thread is deleted close the opened connections.
        """
        self._connection.close()
