#!/bin/bash

# Maximum time (in seconds) to wait for SSH daemon to load on CAP. Avoids neverending script.
MAX_WAIT=600
LOG_FILE="/tmp/debug.log"


function logthis()
{
  now=$(date "+%F %H:%M:%S")
  echo "["$now"] > "$1 >> $LOG_FILE
}

function use_sshpass_with_sudo()
{
  sshpass -p "123lkj" ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -q cap@$1 'echo "123lkj" | sudo -S '$2
}

function use_sshpass_with_scp()
{
  sshpass -p "123lkj" scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null $1 cap@$2:
}

# function install_deps()
# {
#   logthis "install_deps"
#   use_sshpass_with_sudo $1 "DEBIAN_FRONTEND=noninteractive apt-get -qq install python3-crontab"
#   logthis "install_deps exit"
# }

function push_n_move()
{
  # push cron files and scripts.
  logthis "push_n_move"

  # doesn't work -- but why?
  # install_deps $1

  use_sshpass_with_scp /usr/local/src/factory_manager/scripts/kea/cap.cron $1
  use_sshpass_with_scp /usr/local/src/factory_manager/scripts/kea/deploy.py $1

  # move
  use_sshpass_with_sudo $1 'crontab cap.cron'
  use_sshpass_with_sudo $1 'mv deploy.py /usr/local/bin/'

  # set permissions
  use_sshpass_with_sudo $1 'chmod +x /usr/local/bin/deploy.py'

  # Install dependency
  use_sshpass_with_sudo $1 'apt update'
  use_sshpass_with_sudo $1 'apt install python3-pip -y'
  use_sshpass_with_sudo $1 'pip3 install pid'

  logthis "push_n_move exit"
}


function wait_for_sshd()
{
  logthis "wait_for_sshd()"
  wait=0
  while [ $wait -le $MAX_WAIT ]; do
    logthis "Connecting to $1"
    nc -z $1 22
    (($?)) || return 0
    logthis "Retrying..."
    sleep 1
    wait=$((wait+1))
  done
  logthis "wait_for_sshd() exit"
}

function push_cronjob()
{
  # Check if cronjob already exists
  logthis "push_cronjob()"
  cronjob_exists=$(use_sshpass_with_sudo $1 'test -f /var/spool/cron/crontabs/root; echo $?')

  (($cronjob_exists)) || return
  (($cronjob_exists)) && push_n_move $1
  logthis "push_cronjob() exit"

}

# Don't push on other DHCP operations,
# even a lease4_renew (which means the script has already been pushed)

if [[ "$1" == "lease4_select" ]]

then
    # Only actually push the script if FAKE_ALLOCATION is set to 0
    [ "${KEA_FAKE_ALLOCATION}" = "0" ] && \
      wait_for_sshd $KEA_LEASE4_ADDRESS && \
        push_cronjob $KEA_LEASE4_ADDRESS
fi
