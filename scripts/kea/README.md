# Tools needed to deploy Ideascubes

This folder started as a KEA setup for automatic Ideascubes deployment.

Nowadays, only the `deploy.py` script is still in use.

## deploy.py

### Usage

```text
usage: deploy.py [-h] [-p PROCESS_ID] [-n PROJECT_NAME] [-d]

Deploying OLIP with optional project name and process id.

options:
  -h, --help            show this help message and exit
  -p PROCESS_ID, --process_id PROCESS_ID
                        Optional process id to override TM.
  -n PROJECT_NAME, --project_name PROJECT_NAME
                        Optional project name to override TM.
  -d, --dry_run         Does not run the actual deploy. Useful for tests.
```

#### Environment variables

For each playbook the branch to pull can be specified from environment variables:

- `OLIP_PRE_INSTALL_BRANCH`
- `OLIP_DEPLOY_BRANCH`
- `OLIP_POST_INSTALL_BRANCH`

Example:

```shell
OLIP_DEPLOY_BRANCH=123-some-related-issue python3 deploy.py
```

## KEA

A KEA setup that uses hooks to send needed files on devices.

### Installation

This repository is cloned at `deploygw:/usr/local/src/factory_manager`.

The `kea-dhcp4.conf` file is then copied to `/usr/local/etc/kea/`.

### Updates

Whenever updating this directory, you have to `git pull` the changes right
from the checkout on `deploygw`.

