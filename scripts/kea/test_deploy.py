import pytest

from deploy import main
from deploy import initIdeascube


@pytest.mark.parametrize("dry_run", ("-d", "--dry_run"))
def test_argv_dry_run(capsys, dry_run):
    try:
        main([dry_run])
    except SystemExit:
        pass
    output = capsys.readouterr()
    assert "" in output


def test_argv_missing_project_name(capsys):
    dry_run = "-d"
    process_id = "-p 5ff4bdf16cb14369aa6c4ddaf15e9b20"
    try:
        main([dry_run, process_id])
    except SystemExit:
        pass
    output = capsys.readouterr()
    assert "Please provide both process_id and project_name." in output.err


def test_argv_missing_process_id(capsys):
    dry_run = "-d"
    project_name = "-n idc-bsf-olip-fresh"
    try:
        main([dry_run, project_name])
    except SystemExit:
        pass
    output = capsys.readouterr()
    assert "Please provide both process_id and project_name." in output.err


def test_argv_with_project_name_and_process_id(capsys):
    dry_run = "-d"
    project_name = "-n idc-bsf-olip-fresh"
    process_id = "-p 5ff4bdf16cb14369aa6c4ddaf15e9b20"
    try:
        main([dry_run, project_name, process_id])
    except SystemExit:
        pass
    output = capsys.readouterr()
    assert "Please provide both process_id and project_name." not in output.err


def test_initideascube_with_project_name_and_process_id_arguments():
    process_id = '5ff4bdf16cb14369aa6c4ddaf15e9b20'
    project_name = 'idc-bsf-olip-fresh'
    ideascube = initIdeascube(process_id=process_id,
                              project_name=project_name)
    assert ideascube.process_id == process_id
    assert ideascube.project_name == project_name


def test_initideascube_missing_project_name():
    with pytest.raises(ValueError, match='Please provide both process_id and project_name.'):
        process_id = '5ff4bdf16cb14369aa6c4ddaf15e9b20'
        ideascube = initIdeascube(process_id=process_id)


def test_initideascube_missing_process_id():
    with pytest.raises(ValueError, match='Please provide both process_id and project_name.'):
        project_name = 'idc-bsf-olip-fresh'
        ideascube = initIdeascube(project_name=project_name)


def test_initideascube_without_argument():
    ideascube = initIdeascube()
    assert ideascube.process_id == None
    assert ideascube.project_name == None
