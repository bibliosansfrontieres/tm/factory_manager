#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import urllib3
import json
import fcntl
import socket
import struct
import subprocess
import platform
import sys
import os
import requests
import hashlib
from pathlib import Path
from pid import PidFile, PidFileAlreadyLockedError
from shutil import rmtree

urllib3.disable_warnings()

MISSING_ARG_ERROR = "Please provide both process_id and project_name."


class initIdeascube:

    def __init__(self, process_id=None, project_name=None):
        self.tm_api = 'https://front.tm.bsf-intranet.org/goto/temps-modernes/api/deploy/job/'
        self.tm_log = 'https://front.tm.bsf-intranet.org/goto/temps-modernes/api/logs/'
        self.olip_pre_install = "https://gitlab.com/bibliosansfrontieres/olip/olip-pre-install.git"
        self.olip_post_install = "https://gitlab.com/bibliosansfrontieres/olip/olip-post-install.git"
        self.olip_deploy = "https://gitlab.com/bibliosansfrontieres/olip/olip-deploy.git"
        self.olip_descriptor = "http://s3.eu-central-1.wasabisys.com/olip-catalog-descriptor/prod/amd64/"
        self.deploy_script_url = 'https://gitlab.com/bibliosansfrontieres/tm/factory_manager/-/raw/master/scripts/kea/deploy.py'
        self.deploy_script_path = '/usr/local/bin/deploy.py'
        self.olip_data = {}
        self.deploy_state_path = '/opt/'
        self.olip_data_from_api = set()
        self.remote_server = "google.com"
        self.process_id = process_id
        self.project_name = project_name
        self.olip_configfile = "/etc/default/olip"
        self.olip_pre_install_branch = os.environ.get('OLIP_PRE_INSTALL_BRANCH', 'master')
        self.olip_deploy_branch = os.environ.get('OLIP_DEPLOY_BRANCH', 'master')
        self.olip_post_install_branch = os.environ.get('OLIP_POST_INSTALL_BRANCH', 'master')

        if bool(self.process_id) ^ bool(self.project_name):
            raise ValueError(MISSING_ARG_ERROR)

    def logger(self, level, message):

        print('[+] {}'.format(message))

        try:
            data = {
                "log": self.get_mac()[-5:] + " - " + message,
                "level": level,
                "process_id": self.olip_data_from_api[2],
                "user": self.get_mac(),
            }
            encoded_data = json.dumps(data).encode('utf-8')

            with urllib3.PoolManager(cert_reqs='CERT_NONE') as http:
                http.request(
                    'POST',
                    self.tm_log,
                    body=encoded_data,
                    headers={'Content-Type': 'application/json'}
                )
        except TypeError:
            pass

    def auto_update_deploy_script(self):

        r = requests.get(self.deploy_script_url)

        file_to_verify = r.content
        remote_hash= hashlib.sha256(file_to_verify).hexdigest();

        with open(self.deploy_script_path, 'rb') as f:
            local_hash = hashlib.sha256(f.read()).hexdigest()

        if remote_hash != local_hash:
            self.logger("WARNING", "Script differ, updating...")

            r = requests.get(self.deploy_script_url)

            with open(self.deploy_script_path, 'wb') as f:
                f.write(r.content)

            self.logger("INFO", "Update complete, exit")
            sys.exit(0)
        else:
            self.logger("INFO", "Deploy script is up to date")

    def get_mac(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        ifname = socket.if_indextoname(2)
        info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', bytes(ifname, 'utf-8')[:15]))
        return ':'.join('%02x' % b for b in info[18:24])

    def _playbook_need_to_run(self, playbook):

        if self.olip_data_from_api:
            playbook_has_run = Path(self.deploy_state_path + str(playbook))

            if self.olip_data_from_api[0]:
                if playbook_has_run.is_file():
                    return False
                self.logger("INFO", "{} needs to run".format(playbook))
                return True
            else:
                self.logger("ERROR", "Project must be approved in TM in order to be deployed")
                sys.exit(0)
        return False

    def _is_install_done(self, deploy_command):

        playbook_set = set()

        for playbook in deploy_command:
            playbook_set.add(self._playbook_need_to_run(playbook[0]))

        return playbook_set

    def _read_default_olip_data(self):

        olip_data_file = Path('/etc/default/olip')

        if olip_data_file.is_file():
            with open(str(olip_data_file)) as f:
                for line in f:
                    name, var = line.partition("=")[::2]
                    self.olip_data[name.strip()] = var.strip()

    def internet_is_up(self):
        try:
            host = socket.gethostbyname(self.remote_server)
            s = socket.create_connection((host, 80), 2)
            s.close()
            print('[+] Internet is up')
            return True
        except:
            pass
        return False

    def connect_to_api(self):

        with urllib3.PoolManager(cert_reqs='CERT_NONE') as http:
            r = http.request('GET', self.tm_api + self.get_mac())
            data = json.loads(r.data.decode('utf-8'))

        print('[+] Use {} mac address'.format(self.get_mac()))

        if data:
            print('[+] Connected to TM API')
            return data

        elif self.olip_data.get("master_mac_address"):
            with urllib3.PoolManager(cert_reqs='CERT_NONE') as http:
                r = http.request('GET', self.tm_api + self.olip_data["master_mac_address"])
                data = json.loads(r.data.decode('utf-8'))

            return data
        else:
            print('[+] ERROR : This device is not registered on TM')
            sys.exit(0)

    def retrieve_olip_data(self):

        data = self.connect_to_api()

        if data:
            print('[+] Looking for {} process id and {} project name'.format(data["approval"]["process_id"], data["project"]["name"]))
            return(data["approval"]["approved"], data["project"]["name"], data["approval"]["process_id"])
        else:
            print('[+] ERROR : No information about this device on TM')
            sys.exit(0)

    def _write_deploy_state(self, playbook_name):

        with open(self.deploy_state_path + playbook_name, 'w') as f:
            f.write('')

    def install_ansible(self):

        ansible_pull_path = Path('/usr/bin/ansible-pull')
        ansible_pull_local_path = Path('/usr/local/bin/ansible-pull')

        if not ansible_pull_path.is_file() and not ansible_pull_local_path.is_file():

            print("[+] Installing Ansible...")

            distribution_name = platform.platform()

            list_command = [
                "apt-get update",
                "apt-get install software-properties-common ca-certificates --upgrade -y",
                "apt-get install -o Dpkg::Options::=\"--force-confdef\" -o Dpkg::Options::=\"--force-confold\" --quiet --quiet -y ansible=2.9* git"
            ]

            if "Ubuntu" in distribution_name:
                list_command.insert(2,
                                    "apt-add-repository --yes --update ppa:ansible/ansible")
            elif "debian" in distribution_name:
                list_command.insert(2,
                                    '''echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" > /etc/apt/sources.list.d/ansible.list''')
                list_command.insert(3,
                                    "apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367")
                list_command.insert(4,
                                    "apt-get update")
            else:
                print("[+] ERROR: Can not detect your Linux distribution")

            for command in list_command:
                result = subprocess.run(command, shell=True, universal_newlines=True, check=True)

                if result.returncode != 0:
                    print("[+] ERROR {}.".format(result.stderr))
                    sys.exit(0)

            print("[+] Ansible install process has ended successfully.")

    def run_deploy(self):
        """ Find a process_id and deploy it.

        Process_id and project_name are loaded in this order :
            - command line arguments
            - olip data (on disk)
            - TM (Retrieve data from API)
        In every case they get written to disk.

        It means TM is only used as a failover in case no
        process id were given in the command line nor
        were found on disk.

        Ansible playbooks are run if the trigger
        files say they need to run.
        """
        # load olip_data from disk
        self._read_default_olip_data()
        # load from command line arguments
        if self.project_name and self.process_id:
            self.olip_data_from_api = (
                    'True',
                    self.project_name,
                    self.process_id)
            self.olip_data.update({'target_project_name': self.project_name})
            self.olip_data.update({'target_process_id':self.process_id})
        # load from olip data (on disk)
        elif (self.olip_data.get('target_project_name') and
        self.olip_data.get('target_process_id')):
            self.olip_data_from_api = (
                    'True',
                    self.olip_data.get('target_project_name'),
                    self.olip_data.get('target_process_id'))
        # load from  TM - retrieve data from API
        else:
            self.olip_data_from_api = self.retrieve_olip_data()
            self.olip_data.update({'target_project_name': self.olip_data_from_api[1]})
            self.olip_data.update({'target_process_id':self.olip_data_from_api[2]})
        # write to olip data (on disk)
        filedata = str()
        for key in self.olip_data:
            filedata += "{}={}\n".format(key, self.olip_data[key])
        with open(self.olip_configfile, 'w') as f:
            f.write(filedata)


        ideascube_rebooted = Path(self.deploy_state_path + 'rebooted')

        if os.getenv('olip_descriptor'):
            self.olip_descriptor = os.getenv('olip_descriptor')
        elif "olip_file_descriptor" in self.olip_data:
            self.olip_descriptor = self.olip_data["olip_file_descriptor"]

        deploy_command = (
            ("olip-pre-install", [
                    "ansible-pull",
                    "--purge",
                    "-i", "localhost,", "-l", "localhost",
                    "-C", self.olip_pre_install_branch,
                    "-U", self.olip_pre_install,
                    "main.yml",
                    "--extra-vars",
                    "playbook_run_from=deploy.py \
                    process_id={} \
                    project_name={} \
                    end_user_olip_file_descriptor={}".format(
                        self.olip_data_from_api[2], self.olip_data_from_api[1], self.olip_descriptor
                        )]),
            ("olip-deploy", [
                    "ansible-pull",
                    "--purge",
                    "-i", "localhost,", "-l", "localhost",
                    "-C", self.olip_deploy_branch,
                    "-U", self.olip_deploy,
                    "main.yml",
                    "--extra-vars",
                    "playbook_run_from=deploy.py \
                    process_id={} \
                    end_user_server_name={} \
                    end_user_domain_name=ideascube.io \
                    proxy_enable=true \
                    end_user_olip_file_descriptor={}".format(
                        self.olip_data_from_api[2], self.olip_data_from_api[1], self.olip_descriptor
                        )]),
            ("olip-post-install", [
                    "ansible-pull",
                    "--purge",
                    "-i", "localhost,", "-l", "localhost",
                    "-C", self.olip_post_install_branch,
                    "-U", self.olip_post_install,
                    "main.yml",
                    "--extra-vars",
                    "playbook_run_from=deploy.py"])
        )

        for playbook in deploy_command:
            if self._playbook_need_to_run(playbook[0]):
                rmtree("/root/.ansible/", ignore_errors=True)
                p = subprocess.Popen(
                        playbook[1],
                        stdin=subprocess.PIPE,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE
                        )
                stderr = p.communicate()
                if p.returncode != 0:
                    self.logger(
                            "ERROR",
                            "{} process has ended with errors : {}.".format(playbook[0],stderr)
                            )
                    sys.exit(0)
                else:
                    self.logger("INFO", "{} process has ended successfully.".format(playbook[0]))
                    self._write_deploy_state(playbook[0])

        if self._is_install_done(deploy_command) and not ideascube_rebooted.is_file():
            self.logger("INFO", "Hip Hip Hip ! Deploy is over, content schedule to be installed !")
            self._write_deploy_state("rebooted")
            subprocess.run("reboot")
        elif self._is_install_done(deploy_command) and ideascube_rebooted.is_file():
            print('[+] Ideascube is properly configured, nothing else to do !')
        else:
            print('[+] An error occured, please checkout logs at /var/log/ansible-pull.log')
            sys.exit(0)


def main(argv=None):
    parser = argparse.ArgumentParser(
            prog='deploy.py',
            description='Deploying OLIP with optional project name and process id.',
            epilog='In case of trouble call Paul Nhumes.')
    parser.add_argument(
            '-p',
            '--process_id',
            type=str,
            help="Optional process id to override TM."
            )
    parser.add_argument(
            '-n',
            '--project_name',
            type=str,
            help="Optional project name to override TM."
            )
    parser.add_argument(
            '-d',
            '--dry_run',
            action='store_true',
            help="Does not run the actual deploy. Useful for tests."
            )
    args = parser.parse_args(argv)

    process_id = args.process_id
    project_name = args.project_name
    dry_run = args.dry_run

    if bool(process_id) ^ bool(project_name):
        parser.error(MISSING_ARG_ERROR)

    if not dry_run:
        try:
            with PidFile('deploy.pid', piddir="/var/run/") as p:

                ideascube = initIdeascube(process_id=process_id,
                                          project_name=project_name
                                        )

                if ideascube.internet_is_up():
                    ideascube.auto_update_deploy_script()
                    ideascube.install_ansible()
                    ideascube.run_deploy()
                else:
                    print("Cannot connect to Internet")

        except PidFileAlreadyLockedError:
            print ("Another instance of deploy is already running")
            sys.exit(0)


if __name__ == '__main__':
    sys.exit(main())
