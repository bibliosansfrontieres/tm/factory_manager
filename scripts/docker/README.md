# Docker images

## bibliosansfrontieres/temps-modernes-api

* Dockerfile: [`./scripts/docker/Dockerfile-api`](./Dockerfile-api)
* Image: <https://hub.docker.com/r/bibliosansfrontieres/temps-modernes-api>

Update process:

```shell
docker build --push -t bibliosansfrontieres/temps-modernes-api . -f scripts/docker/Dockerfile-api
ssh madrid.prod.tm.aws docker-compose -f /mnt/s3_conf/docker-compose.yml up --detach --pull always
```

## bibliosansfrontieres/factory_manager

* Dockerfile: [`./scripts/docker/Dockerfile-fm`](./Dockerfile-fm)
* Image: <https://hub.docker.com/r/bibliosansfrontieres/factory_manager>

Update process:

```shell
docker build --push -t bibliosansfrontieres/factory_manager . -f scripts/docker/Dockerfile-fm
ssh madrid.prod.tm.aws docker-compose -f /mnt/s3_conf/docker-compose.yml up --detach --pull always
```

## bibliosansfrontieres/olippp

This Dockerfile is sent by `tm` to `madrid`.

It fires `olip-deploy` with all the `extra-args` it requires
to run in this context (ports range etc),
generated from the `tmfront` form user input:

```shell
/usr/local/bin/ansible-pull always -d /var/lib/ansible/local \
    -C master \
    -i localhost, --limit localhost, \
    -U https://gitlab.com/bibliosansfrontieres/olip/olip-deploy main.yml \
    -e end_user_server_name=idc-bsf-project.22649bf99a1b41c2bc431cff7b72a32e.olip \
    end_user_domain_name=idc-bsf-project.22649bf99a1b41c2bc431cff7b72a32e.olip.projects.ideascube.io \
    end_user_olip_file_descriptor=http://s3.eu-central-1.wasabisys.com/olip-catalog-descriptor/prod/amd64/ \
    owner=thomas.faucher \
    olip_storage=/mnt/demostorage/22649bf99a1b41c2bc431cff7b72a32e \
    unique_container_id=22649bf99a1b41c2bc431cff7b72a32e \
    containers_port_range=10160-10169 \
    port_range_id=16 \
    olip_api_base_url=api.idc-bsf-project.22649bf99a1b41c2bc431cff7b72a32e.olip.projects.ideascube.io \
    -e {'extra_hosts':{'packages.ideascube.org': '173.17.0.1', 'download.kiwix.org': '173.17.0.1', 'kiwix.ideascube.org': '173.17.0.1', 's3.eu-central-1.wasabisys.com': '173.17.0.1'}} \
     --tags olip,develop
```

`tm` will also push the resulting image to
[Docker Hub](https://hub.docker.com/r/bibliosansfrontieres/olippp),
in order to serve as docker build --push cache.

* Dockerfile: [`./scripts/docker/olip/Dockerfile`](./olip/Dockerfile)
* Image: <https://hub.docker.com/r/bibliosansfrontieres/olippp>

Update process: rebuild the `bibliosansfrontieres/factory_manager` image.
