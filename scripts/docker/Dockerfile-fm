FROM python:3.7
LABEL maintainer "steven.wallimann@bibliosansfrontieres.org"
ENV recipe_id=7

RUN apt-get update && apt-get upgrade --yes \
    && apt-get install --yes \
        curl \
        gcc \
        git \
        libc-dev \
        libffi-dev \
        libldap2-dev \
        libsasl2-dev \
        openssh-client \
        rsync

COPY requirements.txt /TM/requirements.txt
RUN pip install -r /TM/requirements.txt


WORKDIR /TM

RUN mkdir /root/.ssh/ 2>/dev/null || true \
    && echo "StrictHostKeyChecking no" > /root/.ssh/config \
    && git config --global user.email "it+bot-tempsmodernes@bibliosansfrontieres.org" \
    && git config --global user.name "bot-TempsModernes"

# clone via http, then set remote to use git+ssh
# hadolint ignore=DL3003
RUN git clone https://github.com/ideascube/catalog-i-o /TM/catalog-i-o \
    && cd /TM/catalog-i-o \
    && git remote set-url origin git@github.com:ideascube/catalog-i-o.git \
    \
    && git clone https://github.com/ideascube/ansiblecube /TM/ansiblecube \
    && cd /TM/ansiblecube \
    && git remote set-url origin git@github.com:ideascube/ansiblecube.git


COPY main/factory_manager/ main/factory_manager/
COPY fm.py .
COPY data data
COPY models models
COPY services services
COPY scripts scripts
COPY repository repository

ENTRYPOINT python3 fm.py $recipe_id
