# factory_manager

[![pipeline status](https://gitlab.com/bibliosansfrontieres/tm/factory_manager/badges/develop/pipeline.svg)](https://gitlab.com/bibliosansfrontieres/tm/factory_manager/commits/develop)
[![coverage report](https://gitlab.com/bibliosansfrontieres/tm/factory_manager/badges/develop/coverage.svg)](https://gitlab.com/bibliosansfrontieres/tm/factory_manager/commits/develop)
[![Documentation Status](https://readthedocs.org/projects/factory-manager/badge/?version=latest)](https://factory-manager.readthedocs.io/en/latest/?badge=latest)

![alt text](temps-modernes-behaviour.png)

Docker images are documented at [`scripts/docker/README.md`](./scripts/docker/README.md).
