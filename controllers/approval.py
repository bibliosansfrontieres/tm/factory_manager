# -*- coding: utf-8 -*-
from flask import request
from flask_restplus import Resource, Namespace
from repository.approval.DictApprovalDAO import DictApprovalDAO
from services.approval.approval import ApprovalService
from .decorators.login_required import login_required


ns = Namespace('approval', description="Operations related to approval.")


@ns.route("/approve")
class ApprovalApprove(Resource):
    decorators = [login_required]

    def post(self):
        return ApprovalService().approve(DictApprovalDAO.get_approval(request.get_json()))


@ns.route("/reject")
class ApprovalReject(Resource):
    decorators = [login_required]

    def post(self):
        return ApprovalService().reject(DictApprovalDAO.get_approval(request.get_json()))
