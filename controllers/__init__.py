# -*- coding: utf-8 -*-

from .gateway import ns as ns_gateway
from .logs import ns as ns_logs
from .process import ns as ns_process
from .deploy import ns as ns_deploy
from .approval import ns as ns_approval
from .login import ns as ns_login
from .vm import ns as ns_vm


__all__ = ['ns_gateway', 'ns_logs', 'ns_process', 'ns_deploy', 'ns_approval', 'ns_login', 'ns_vm']
