from functools import wraps
from flask import request
from models.core.session import Session
from settings import ENVIRON


def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        # No login required in DEV env.
        if ENVIRON == "DEV":
            return f(*args, **kwargs)

        r = request.headers

        # if user is not logged in, ask for login.
        if r.get('X-Auth-Token') is None:
            return "You need to login to access this page", 403

        # get session state.
        session = Session(token=r.get('X-Auth-Token'))
        if not session.is_authenticated:
            session.flush_old_sessions()  # Using this perfect event for doing some housekeeping.
            return "Provided token has expired, please login again.", 403

        return f(*args, **kwargs)

    return wrap
