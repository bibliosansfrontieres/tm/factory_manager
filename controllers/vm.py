# -*- coding: utf-8 -*-
from flask_restplus import Resource, Namespace
from flask import request
from .decorators.login_required import login_required
from services.vm.vm import VMService

ns = Namespace('vm', description="Operations related to Virtual Machines.")


@ns.route("/list")
class VMList(Resource):
    decorators = [login_required]

    def get(self):
        return VMService().list()


@ns.route("/delete")
class VMDelete(Resource):
    decorators = [login_required]

    def post(self):

        data = request.get_json()
        return VMService().delete(data)
