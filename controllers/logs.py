# -*- coding: utf-8 -*-

from datetime import datetime
from flask_restplus import Resource, Namespace, reqparse
from flask import request
from services.logger.logs import get_logs, post_log

ns = Namespace('logs', description="Logs controller for MongoLogger.")


@ns.route("/")
class GatewayLogs(Resource):

    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument(
            'process_id',
            help="process id of the logs to show."
        )
        parser.add_argument(
            'level',
            default='INFO',
            choices=('HIGHEST', 'DEBUG', 'INFO', 'WARNING', 'ERROR'),
            help="level of the logs."
        )
        parser.add_argument(
            'limit',
            default=50,
            type=int,
            help="amount of elements to show by the request."
        )
        parser.add_argument(
            'min_date',
            type=datetime,
            help="inferior date of the search (included)."
        )
        parser.add_argument(
            'max_date',
            type=datetime,
            help="superior date of the search (excluded)."
        )

        args = parser.parse_args()
        return get_logs(**args)

    def post(self):

        data = request.get_json()
        return post_log(data)
