# -*- coding: utf-8 -*-

from flask_restplus import Resource, Namespace, reqparse
from services.process.process import ProcessID

ns = Namespace('process', description="Process controller for MongoLogger.")


@ns.route("/")
class ProcessById(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument(
            'id',
            required=False,  # if not provided, will return all results.
            help="process id of the logs to show."
        )
        args = parser.parse_args()
        return ProcessID.get_process_by_id(**args)


@ns.route("/recipe/")
class ProcessesByRecipeId(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument(
            'id',
            required=True,
            type=int,
            location='args',
            action='append',
            help="Provide a recipe id to get all process_id corresponding."
        )
        args = parser.parse_args()
        return ProcessID.get_processes_by_recipe(**args)
