# -*- coding: utf-8 -*-

from flask_restplus import Resource, Namespace

from services.deploy.deploy import DeployService
"""
get_lease, get_all_leases, get_device_job
"""


ns = Namespace('deploy', description="Operations related with deployment.")


@ns.route("/lease/<string:mac_address>")
class GatewayLease(Resource):
    """
    List DHCP lease informations for a given MAC address.
    """

    def get(self, mac_address):

        return DeployService.get_lease(mac_address)


@ns.route("/lease")
class GatewayAllLeases(Resource):
    """
    List all DHCP leases.
    """

    def get(self):

        return DeployService.get_all_leases()


@ns.route("/lease-with-content")
class GatewayAllLeasesWithContent(Resource):
    """
    List all DHCP leases.
    """

    def get(self):

        return DeployService.get_all_leases_with_content()


@ns.route("/job/<string:mac_address>")
class GatewayDeploy(Resource):
    """
    Get from database deploy orders given a MAC address
    """

    def get(self, mac_address):

        return DeployService.get_device_job(mac_address)
