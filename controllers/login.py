# -*- coding: utf-8 -*-

from flask import request
from flask_restplus import Resource, Namespace
from services.login.login import Login, Sessions
from .decorators.login_required import login_required


ns = Namespace('login', description="Login Service.")


@ns.route("/")
class LoginValidation(Resource):

    def post(self):
        return Login.auth(request.get_json()['username'], request.get_json()['password'])


@ns.route("/session/<string:token>")
class SessionCheck(Resource):
    decorators = [login_required]

    def get(self, token):
        return Sessions.validate(token)
