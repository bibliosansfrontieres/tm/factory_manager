# -*- coding: utf-8 -*-

from flask import request
from flask_restplus import Resource, Namespace
from .decorators.login_required import login_required
from services.gateway import gateway_start

ns = Namespace('gateway', description="Operations related with gateway.")


@ns.route("/prepare")
class GatewayPrepare(Resource):
    decorators = [login_required]

    def post(self):

        data = request.get_json()
        return gateway_start(1, data)


@ns.route("/visualize")
class GatewayVisualize(Resource):
    decorators = [login_required]

    def post(self):

        data = request.get_json()
        return gateway_start(4, data)


@ns.route("/configure")
class GatewayConfigure(Resource):
    decorators = [login_required]

    def post(self):

        data = request.get_json()
        return gateway_start(12, data)


@ns.route("/approve")
class GatewayApprove(Resource):
    decorators = [login_required]

    def post(self):

        data = request.get_json()
        return gateway_start(9, data)


@ns.route("/deploy")
class GatewayDeploy(Resource):
    decorators = [login_required]

    def post(self):

        data = request.get_json()
        return gateway_start(11, data)


@ns.route("/cleanup")
class GatewayCleanUp(Resource):
    decorators = [login_required]

    def post(self):

        data = request.get_json()
        return gateway_start(13, data)
