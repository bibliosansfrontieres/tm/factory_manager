#  Deployer un environnement de dev local (ou pas).
## 1) Puller les repo
`git clone git@gitlab.com:bibliosansfrontieres/tm/factory_manager.git`

## 2) Builder les containers
Version de docker-compose __1.13.1+__ attendue
```
~ $ cd factory_manager/
factory_manager $ docker-compose -f dev/docker/docker-compose.yml up -d --build
```
Le premier build est long...

## 3) Exploiter cet environnement
| Service | URL | Login | Password |
| ------ | ------ | ------ | ------ |
| Omeka | [http://localhost:8081](http://localhost:8081) | dev | devuser |
| RabbitMQ  | [http://localhost:15762](http://localhost:15762) | dev | devuser |
| API REST (logs) | `curl "http://localhost:5000/temps-modernes/api/logs/?level=INFO&limit=5"` |
| API REST (Start build) | `curl --header "Content-Type: application/json" -d '{"media_center_configuration":{"engine": "ideascube", "languages": ["fr"], "template": "no_template"}, "project": { "name": "kb-bsf-test", "id": "35909","description":"__description"}}' http://localhost:5000/temps-modernes/api/gateway/prepare`. (Sont disponible : prepare, visualize, approve, reject, configure. Voir [controllers/gateway.py](https://gitlab.com/bibliosansfrontieres/tm/factory_manager/blob/master/controllers/gateway.py)) | _NB_: project_id 35909 existe bel et bien et est le seul disponible dans cet environnement. | |
| MySQL | Inaccessible, port non forwardé. | | |
| MongoDB | Inaccessible, port non forwardé. | | |
| factory_manager | Inaccessible, port non forwardé. Si votre build réussit, vous aurez alors votre .zip dans /tmp (le fichier source dans /tmp/non-du-projet)| | |
| fileserver | Non accessible. Sert uniquement à pousser les catalogues (mock bubble) | Accessible en SSH/Rsync depuis le VLAN docker. | |

## 4) Schéma de principe

![](dev-environment-schema.png)
