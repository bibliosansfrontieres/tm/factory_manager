Authlib==0.13
aiohttp==3.6.2
asyncio==3.4.3
requests==2.19.1
PyYAML==3.13
pika==0.12.0
toml==0.9.4
docker==4.1.0
python-dateutil==2.7.3
pymongo==3.7.1
gitdb2==2.0.6
GitPython==2.1.10
python-slugify==2.0.1
pytest==3.8.1
pytest-cov==2.8.1
pytest-mock==1.10.4
pycodestyle==2.5.0
flake8==3.7.8
git+https://framagit.org/ideascube/omeka-to-pkg.git
marshmallow==3.3.0


# Issue with Rust dependency see https://github.com/pyca/cryptography/issues/5806
cryptography==3.3
