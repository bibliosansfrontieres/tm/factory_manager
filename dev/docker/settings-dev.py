# -*- coding: utf-8 -*-

FACTORIES_MODULES_PATH = "models.core.factory.factories"

ENVIRON = "DEV"

########################
#  PRODUCERS SETTINGS  #
########################

IDEASCUBE_ALLOWED_LANGUAGES = ['fr', 'am', 'apa', 'ar', 'bm', 'bn', 'crg', 'ctg', 'el', 'en',
                               'es', 'fa', 'fa-ir', 'ff', 'hat', 'hi', 'it', 'kmr',
                               'ku', 'ln', 'lsf', 'mm', 'mos', 'my-mm', 'oym', 'pl', 'por', 'prs',
                               'ps', 'rhg', 'rn', 'rw','sg', 'so', 'srn',
                               'srr', 'sw', 'ti', 'ur', 'vi', 'vo', 'way', '411', 'multilang']

OMEKA_API_KEY = "d0123f2a123b84ee2507c60d513f3e2e99a608eb"
OMEKA_API_URL = "http://omeka/api/"

DUBLIN_CORE_ELEMENTS = {
    "title": 50,
    "summary": 41,
    "language": 44,
    "credits": 39,
    "path": 256,
    "format": 42,
    "device": 262
}

OMEKA_BSF_PROJECT_TYPE_ID = 19
EXTERNAL_PACKAGE_TYPE = {
    "website": 7,
    "kiwix_packages": 22,
    "khan_academy": 23
}

# CheckPackagesOmeka
OMEKA_CONFIG = {
    'media_master_folder': "/mnt/alexandrie/"
}

# CheckUrlAvailability
IDC_DEPLOY_CONFIG = {
    "catalogs": [
        {
            "id": "Kiwix",
            "name": "Kiwix ZIM content",
            "url": "https://raw.githubusercontent.com/bot-TempsModernes/catalog-i-o/master/kiwix.yml"
        },
        {
            "id": "Omeka",
            "name": "Omeka packages",
            "url": "https://raw.githubusercontent.com/bot-TempsModernes/catalog-i-o/master/omeka.yml"
        },
        {
            "id": "StaticSites",
            "name": "Static sites",
            "url": "https://raw.githubusercontent.com/bot-TempsModernes/catalog-i-o/master/static-sites.yml"
        }
    ],
    "omeka_package_url": "http://packages.ideascube.org/omeka/",
    "omeka_json_destination": "/mnt/alexandrie/",
    "package_folder": "/tmp/",  # Where to copy builded packages to.
}

# User to rsync catalog with
REMOTE_USER = "root"

CATALOGS = ["kiwix.yml", "static-sites.yml", "bibliotecamovil.yml", "bukavu.yml", "omeka.yml",
            "osmaps.yml", "kb-demo-usa.yml"]

# CopyToBubble
BUBBLE_DEST_SERVER = "fileserver"
BUBBLE_CATALOG_FOLDER = "/"
BUBBLE_OMEKA_PACKAGES_FOLDER = "/"

# MakePackageOmeka & UpdateCatalog
CATALOG_PATH = "/tmp/catalog.yml"

# Where to store locally build packages
BUILD_WORKSPACE = "/tmp"

# UpdateCatalog & UpdateDeviceList
GIT_CONFIG = {
    "ansiblecube_local_repo": "/TM/ansiblecube/",
    "catalog-io_local_repo": "/TM/catalog-i-o",
    "device_list_relative_path": "roles/set_custom_fact/files/device_list.fact",
    "omeka_catalog_relative_path": "omeka.yml"
}

DOCKER_VARS = {
    "kiwix_image": "bibliosansfrontieres/kiwix",
    "kiwix_prod_image": "bibliosansfrontieres/kiwix-i386:latest",
    "kolibri_image": "bibliosansfrontieres/kolibripp:latest",
    "kolibri_prod_image": "bibliosansfrontieres/kolibripp:latest",
    "ideascube_image": "bibliosansfrontieres/ideascubepp:0.37.7",
    "ideascube_prod_image": "bibliosansfrontieres/ideascubepp:0.37.7",
    "olip_image": "bibliosansfrontieres/olippp:latest",
    "network": "docker_default",
    "DOCKER_HOST": "http://127.0.0.1:2376",
    "DOCKER_DOMAIN": "docker.local.lan",
    "KIWIX_SOURCE": "/mnt/kiwix",
    "KOLIBRI_SOURCE": "/mnt/kolibri",
    "LONGTERM_VOLUME_STORAGE": "/srv/",
    "SHORTERM_VOLUME_STORAGE": "/srv/",
    "CONTAINER_ROOT_DIR": "/srv",
    "DEFAULT_VALIDITY": 0
}

OLIP = {
    "catalogs": [
        "http://olip.ideascube.org/amd64/descriptor.json",
        "http://olip.ideascube.org/i386/descriptor.json",
        "http://olip.ideascube.org/arm32v7/descriptor.json"
    ],
    "mediacenter": {
        "name": "Mediacenter"
    },
    "package_folder_aws": "/tmp/",  # Where to copy builded packages to.
    "package_folder_wasabi": "/tmp/",  # Where to copy builded packages to.
    'base_package_url': 'http://packages.ideascube.org/hugo/',
    # New catalogs will copied to Wasabi storage
    'catalog_storage': '/mnt/s3_olip_descriptor',
    'extra_hosts': {  # add host entries to /etc/hosts of OLIP-API container
        "download.kiwix.org": "10.10.10.5",
        "packages.ideascube.org": "10.10.10.5"
    },
    # Port range to be used for apps, must be of form "lowport-highport"
    # Defaults to 10000-20000
    'port_range': '10000-11000',
    # How many port do we allocate per process? Or how many app at most will be needed for a deployment?
    # WARNING, if you change this, then you MUST remove all running VM, using frontend.
    # Default: 10
    'allocated_ports': 10
}

#################
#  DB settings  #
#################

KEA_API = "http://10.10.30.3:8080/"

RABBITMQ_CONNECTION_HOST = "broker"
RABBITMQ_CONNECTION_PORT = 5672
RABBITMQ_CONNECTION_VHOST = "temps_modernes"
RABBITMQ_CONNECTION_USERNAME = "dev"
RABBITMQ_CONNECTION_PASSWORD = "devuser"

PROD_MONGODB_CONNECTION_HOST = 'mongo'
PROD_MONGODB_NAME = 'devices'
PROD_MONGODB_CONNECTION_PORT = 27017
PROD_MONGODB_CONNECTION_USERNAME = None
PROD_MONGODB_CONNECTION_PASSWORD = None

PREPROD_MONGODB_CONNECTION_HOST = 'mongo'
PREPROD_MONGODB_NAME = 'temps-modernes'
PREPROD_MONGODB_CONNECTION_PORT = 27017
PREPROD_MONGODB_CONNECTION_USERNAME = None
PREPROD_MONGODB_CONNECTION_PASSWORD = None

LDAP_INFO = {
    "BASE_DN": "cn=users,dc=bsf-montreuil,dc=local",
    "USER_DN": "uid=",
    "LDAP_HOST": "ldap://10.10.9.10"
}

FLASK_SECRET = 'Vp4AOP5@Ncp6eyTn9-Yr3J4Q/#0DhY'

RECIPES_PATH = "data/recipes_db.json"

SLACK_NOTIF = {
    "channel": "#temps-modernes",
    "icon_emoji": ":temps-modernes:",
    "username": "root",
    "url": "https://hooks.slack.com/services/XXXXXXXX/XXXXXXX/xxxxxxxxxxxxxxxxxxxxxxx"
}
