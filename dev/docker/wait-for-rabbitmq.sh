#!/bin/sh
# wait-for-rabbitmq.sh

set -e

host="$1"
shift
cmd="$@"

echo "Starting Script"
until curl -s -f -u dev:devuser http://$host/api/exchanges/temps_modernes/tmp.log; do
  >&2 echo "RabbitMQ is unavailable - sleeping"
  sleep 1
done


>&2 echo "RabbitMQ is up - executing command"
exec $cmd
